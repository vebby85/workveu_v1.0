/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Aggregates.unwind;
import static com.mongodb.client.model.Projections.include;
import interfaces.BaseModal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import models.helper.CommonModel;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import redis.clients.jedis.Jedis;

/**
 *
 * @author abc
 */
public class Api_links extends BaseModal {

        public String filter = "";
   

    /*public List<String> listOfProducts()
        {
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
       
        List<String> list=cm.getAllProduct();
        
        return list;    
        }
*/
  /*  public List<String> listOfCategories()
        {
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
       
        List<String> list=cm.getAllCategories();
        
        return list;    
        }
    
       public List<String> listOfSuppliers()
        {
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
      
        List<String> list=cm.getAllSupplier();
        
        return list;    
            
        }
        */
        public String listofSaleProducts()
        {
         CommonModel cm = new CommonModel();
            cm.con = this.supplyMySql();
            cm.jedis = this.supplyJedis();
            JSONObject jin = new JSONObject();
            String deal = cm.getFormIdBySlug(this.app_code, "deals");
            MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                   .getCollection(deal);
            //System.out.print(deal);
          FindIterable<Document> find = col.find().projection(new Document("_id",0).append("product.product",1).append("product.category",1));
           

            JSONArray j = new JSONArray();

            for (Document s : find) {
                
                j.add(s);
           
            }
           
        return j.toJSONString();  
        }
}
