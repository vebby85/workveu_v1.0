/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.client.MongoCollection;
import interfaces.BaseModal;
import org.bson.Document;

/**
 *
 * @author viru
 */
public class Logs extends BaseModal {
    public String data = "";
    
    public void saveAccountTimeline()
    {
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection("timeline"); 
        Document parse = Document.parse(this.data);
        col.insertOne(parse); 
    }
}
