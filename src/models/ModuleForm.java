/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBObject;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import helpers.Helper;
import interfaces.BaseModal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import models.helper.CommonModel;
import models.helper.MongoModel;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author viru
 */
public class ModuleForm extends BaseModal {

    public void saveForm(String modid) {
        try {
            String form = "";
            PreparedStatement ps = this.supplyMySql().prepareStatement("SELECT CONVERT(app_module_forms.module_form USING utf8) as form FROM app_module_forms"
                    + " WHERE org_id=? AND module_id=?");
            ps.setString(1, this.app_code);
            ps.setString(2, modid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                form = rs.getString("form");
            }
            rs.close();
            ps.close();
            if (!form.isEmpty()) {
                MongoModel model = new MongoModel();
                model.mc = this.supplyMongo();
                model.db = "module_forms";
                model.collection = this.app_code;
                model.id = modid;
                JSONArray blocks = (JSONArray) JSONValue.parse(form);
                JSONArray jarr = new JSONArray();
                JSONArray formIDS = new JSONArray();
                for (Object s : blocks) {
                    JSONObject fields = (JSONObject) s;

                    if (null != fields.get("showInSingle") && (boolean) fields.get("showInSingle")) {
                        Helper.printConsole(fields);
                        if (null != fields) {
                            JSONArray infields = (JSONArray) JSONValue.parse(fields.get("fields").toString());
                            for (Object f : infields) {
                                Document parse = Document.parse(f.toString());
                                parse.remove("showEdit");
                                parse.remove("class");
                                parse.remove("showEdit");
                                parse.remove("showDetail");
                                parse.remove("placeholder");
                                parse.remove("fieldEditable");
                                parse.remove("fieldshowinApp");
                                parse.remove("fieldshowinApp");
                                jarr.add(parse);
                                if (parse.getBoolean("baseField")) {
                                    formIDS.add(parse.getString("id"));
                                }

                            }

                        }
                    }

                }
                Document fields = new Document("fields", jarr);
                model.UpdateOrInsertDocument(fields);
                if (!formIDS.isEmpty()) {
                    CommonModel cm = new CommonModel();
                    cm.con = this.supplyMySql();
                    List<String> users = cm.getUserListByORG(app_code);

                    User user = new User();
                    user.app_code = this.app_code;
                    for (String u : users) {
                        user.uid = u;
                        user.updateColumnSettings(formIDS.toString(), modid);
                    }
                }

            }

        } catch (SQLException ex) {
        }
    }

    public void saveAllForm() {
        try {
            String form = "";
            PreparedStatement ps = this.supplyMySql().prepareStatement("SELECT CONVERT(app_module_forms.module_form USING utf8) as form,module_id FROM app_module_forms"
                    + " WHERE org_id=? ");
            ps.setString(1, this.app_code);
            ResultSet rs = ps.executeQuery();
            MongoModel model = new MongoModel();
            model.mc = this.supplyMongo();
            model.db = "module_forms";
            model.collection = this.app_code;
            CommonModel cm = new CommonModel();
            cm.con = this.supplyMySql();
            List<String> users = cm.getUserListByORG(app_code);

            User user = new User();
            user.app_code = this.app_code;
            while (rs.next()) {
                form = rs.getString("form");
                if (!form.isEmpty()) {

                    model.id = rs.getString("module_id");
                    JSONArray blocks = (JSONArray) JSONValue.parse(form);
                    JSONArray jarr = new JSONArray();
                    JSONArray formIDS = new JSONArray();
                    for (Object s : blocks) {
                        JSONObject fields = (JSONObject) s;

                        if (null != fields.get("showInSingle") && (boolean) fields.get("showInSingle")) {
                            Helper.printConsole(fields);
                            if (null != fields) {
                                JSONArray infields = (JSONArray) JSONValue.parse(fields.get("fields").toString());
                                for (Object f : infields) {
                                    Document parse = Document.parse(f.toString());
                                    parse.remove("showEdit");
                                    parse.remove("class");
                                    parse.remove("showEdit");
                                    parse.remove("showDetail");
                                    parse.remove("placeholder");
                                    parse.remove("fieldEditable");
                                    parse.remove("fieldshowinApp");
                                    parse.remove("fieldshowinApp");
                                    jarr.add(parse);
                                    if (parse.getBoolean("baseField")) {
                                        formIDS.add(parse.getString("id"));
                                    }
                                }

                            }
                        }

                    }
                    Document fields = new Document("fields", jarr);
                    model.UpdateOrInsertDocument(fields);
                    if (!formIDS.isEmpty()) {
                        for (String u : users) {
                            user.uid = u;
                            user.updateColumnSettings(formIDS.toString(), rs.getString("module_id"));
                        }
                    }
                }
            }
            rs.close();
            ps.close();
        } catch (SQLException ex) {
        }
    }

    public String getFormFields(String module_id) {
        JSONObject json = new JSONObject();
        MongoCollection<Document> collection = this.supplyMongo().getDatabase("module_forms")
                .getCollection(this.app_code);
        Document first = collection.find(new BasicDBObject("_id", module_id))
                .projection(new BasicDBObject("fields.label", 1)
                        .append("fields.id", 1)
                        .append("fields.type", 1)
                        .append("fields.options", 1)
                        .append("fields.autocompleteTrigger", 1)
                        .append("fields.required", 1)
                        .append("fields.baseField", 1))
                .first();

        JSONArray jarr = new JSONArray();
        JSONArray jarr1 = new JSONArray();
        ArrayList<Document> get = first.get("fields", new ArrayList<>());
        get.forEach((d) -> {
            JSONObject fields = new JSONObject();
            fields.put("label", d.getString("label"));
            fields.put("id", d.getString("id"));
            fields.put("type", d.getString("type"));
            fields.put("required", d.getBoolean("required"));
            fields.put("base", d.getBoolean("baseField"));
            jarr.add(fields);
            jarr1.add(d);
        });
        json.put("fields", jarr);
        json.put("filter", jarr1);
        
        return json.toJSONString();

    }

    public String getFormFieldsAll() {
        JSONObject json = new JSONObject();
        MongoCollection<Document> collection = this.supplyMongo().getDatabase("module_forms")
                .getCollection(this.app_code);
        PreparedStatement ps;
        try {

            ps = this.supplyMySql().prepareStatement("SELECT module_id FROM app_modules WHERE app_code=? AND module_type='form'");
            ps.setString(1, this.app_code);
            ResultSet rs = ps.executeQuery();
            JSONArray fieldsArray = new JSONArray();
            JSONArray filterArray = new JSONArray();
            List<String> in = new ArrayList<>();
            in.add("autocomplete");
            in.add("select");
            in.add("date");
            while (rs.next()) {
                Document first = collection.find(new BasicDBObject("_id", rs.getString("module_id")))
                        .projection(new BasicDBObject("fields.label", 1)
                                .append("fields.id", 1)
                                .append("fields.type", 1)
                                .append("fields.required", 1))
                        .first();
                Object fields = null == first ? new Document() : first.get("fields");
                JSONObject fieldJson = new JSONObject();
                fieldJson.put(rs.getString("module_id"), fields);
                fieldsArray.add(fieldJson);
                Document first1 = collection.aggregate(Arrays.asList(
                        new BasicDBObject("$unwind", "$fields"),
                        new BasicDBObject("$match", new BasicDBObject(
                                "_id", rs.getString("module_id")
                        ).append("fields.type", new BasicDBObject("$in", in))),
                        new BasicDBObject("$project", new BasicDBObject("fields.label", 1)
                                .append("fields.id", 1)
                                .append("fields.options", 1)
                                .append("fields.autocompleteTrigger", 1)
                                .append("fields.type", 1))
                )).first();
                Object fields1 = null == first1 ? new Document() : first1.get("fields");
                JSONObject filterJson = new JSONObject();
                filterJson.put(rs.getString("module_id"), fields1);
                filterArray.add(filterJson);
            }
            ps.close();
            json.put("filter", filterArray);
            json.put("fields", fieldsArray);
            rs.close();
            ps.close();
        } catch (SQLException ex) {
        }
        return json.toJSONString();
    }

    public String getTextFieldsAutocomplete(String module_id) {
        MongoCollection<Document> collection = this.supplyMongo().getDatabase("module_forms")
                .getCollection(this.app_code);
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        String inslug = cm.getFormIdBySlug(this.app_code, "inquiries");
        List<String> nin = new ArrayList<>();
        List<String> in = new ArrayList<>();
        nin.add(module_id);
        nin.add(inslug);
        in.add("text");
        in.add("phone");
        in.add("select");
        AggregateIterable<Document> fields = collection.aggregate(Arrays.asList(
                new BasicDBObject("$unwind", "$fields"),
                new BasicDBObject("$match", new BasicDBObject(
                        "_id", new BasicDBObject("$nin", nin)
                ).append("fields.type", new BasicDBObject("$in", in))),
                new BasicDBObject("$project", new BasicDBObject("fields.label", 1).append("fields.id", 1)
                        .append("fields.type", 1))
        ));
        JSONArray jarr = new JSONArray();
        for (Document d : fields) {
            Document din = new Document();
            if (d.containsKey("fields")) {
                din = d.get("fields", din);
                din.put("field_id", d.getString("_id"));
            }
            jarr.add(din);
        }
        fields.iterator().close();
        return jarr.toJSONString();
    }
    public String checkUniqueInDb(String formid,String fieldid,String value)
    {
        CommonModel cm = new CommonModel();
        JSONObject rs = new JSONObject();
        cm.mongo = this.supplyMongo();
        if (cm.isRecordExistInMongo(app_code, formid, fieldid, value)) {
                rs.put("res", true);
                return rs.toJSONString();
        }
        
        rs.put("res", false);
        return rs.toJSONString();
    }
    public void closeClass() {
        this.closeMySql();
    }
}
