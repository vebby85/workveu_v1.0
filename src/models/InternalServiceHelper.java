/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import interfaces.BaseModal;
import org.bson.Document;
import org.json.simple.JSONObject;
import templates.Invoices;

/**
 *
 * @author viru
 */
public class InternalServiceHelper extends BaseModal {

    public String moduleid = "";

    public String getProductDetailForInvoice(String recordid) {
        MongoCollection<Document> collection = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(this.moduleid);
        
        //Helper.printConsole(moduleid);
        BasicDBObject find = new BasicDBObject("_id", recordid);
        Document find1 = collection.find(find)
                .projection(new BasicDBObject("product", 1).append("_id", 0))
                .first();
        JSONObject json = new JSONObject();
        if (null != find1) {
            find1.entrySet().forEach((entry) -> {
                json.put(entry.getKey(), entry.getValue());
            });
        }
        find1.clear();
        return json.toJSONString();
    }
    public String generateSOPdf(String html,String path)
    {
        Invoices in = new Invoices();
        in.returnSoInvoice(html, path);
        return null;
    }
}
