/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import helpers.Helper;
import interfaces.BaseModal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import models.helper.CommonModel;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class AutocompleteResource extends BaseModal {

    public String module_id = "";
    public String databindwith = "";
    public String uid = "";

    public String getUserResourceWithField(String fieldid, String search, String pid, String pname) {
        JSONObject json = new JSONObject();
        CommonModel cm = new CommonModel();
        cm.jedis = this.supplyJedis();
//        if (!cm.isFormFieldExist(app_code, module_id, fieldid)) {
//            json.put("error", true);
//            json.put("message", "No Field Found");
//            return json.toJSONString();
//        }
        if (fieldid.equals("manager_user")) {
            JSONArray arr = cm.getMyTeamAsResourceMap(uid, app_code);

            json.put("data", arr);
            json.put("error", false);
            return json.toJSONString();

        }
        if (fieldid.equals("territory_master")) {
            return this.getTerritoriesAsResource();
        }
        JSONArray jarr = new JSONArray();
        if (fieldid.equals("assigned_to")) {
            cm.con = this.supplyMySql();
            cm.getMyUpperTeam(app_code, uid);
            JSONObject user = new JSONObject();
            user.put("name", "Me");
            user.put("id", uid);
            cm.maplist.add(user);
            json.put("data", cm.maplist);
            json.put("error", false);
            return json.toJSONString();
        }
        MongoCollection<Document> collection = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(this.module_id);
        BasicDBObject find = new BasicDBObject("isActive", true);
        if (!search.isEmpty() && this.databindwith.equals("typeahead")) {
            find.append(fieldid, new BasicDBObject("$regex", search).append("$options", "$i"));
        }
        if (!pid.isEmpty()) {
            find.append(pname + "_key", pid);
        }
        // BasicDBObject project = new BasicDBObject("_id", 1).append(fieldid, 1);

        if (fieldid.equals("field_1579092826995")) {
            cm.con = this.supplyMySql();
            BasicDBObject cl1 = new BasicDBObject("manager_user_key", new BasicDBObject("$in", cm.getMyTeamAsList(this.uid, app_code)));
            BasicDBObject cl2 = new BasicDBObject("inquery_manager", this.uid);
            BasicDBList d = new BasicDBList();
            d.add(cl1);
            d.add(cl2);
            find.append("$or", d);
            // project.append("deal_status_key", 1).append("deal_status", 1).append("ex_close_date", 1).append("product", 1);
        }

        FindIterable<Document> data = collection.find(find)
                .projection(new BasicDBObject(fieldid, 1).append("_id", 1));

        for (Document s : data) {
            JSONObject j = new JSONObject();
            j.put("name", s.get(fieldid) == null ? "" : s.getString(fieldid));
            j.put("id", s.getString("_id"));
            jarr.add(j);
        }

        // Helper.printConsole(jarr);
        json.put("error", false);
        json.put("data", jarr);

        //System.out.println(json);
        return json.toJSONString();
    }

    public String getUsersAsResource(String s, String role) {
        JSONObject json = new JSONObject();
        JSONArray jarr = new JSONArray();
        try {
            String query = "SELECT org_portal_users.uid,org_portal_users.name "
                    + "FROM org_portal_users "
                    + "  WHERE org_portal_users.org_id=? ";
            if (!role.equals("all")) {
                query += " AND org_portal_users.role_id= ?";
            }
            if (this.databindwith.equals("typeahead")) {
                query += " AND users.name LIKE ?";
            }
            PreparedStatement ps = this.supplyMySql().prepareStatement(query);
            ps.setString(1, this.app_code);
            if (!role.equals("all")) {
                ps.setString(2, role);
            }
            if (this.databindwith.equals("typeahead")) {
                ps.setString(3, "%" + s + "%");
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                JSONObject j = new JSONObject();
                j.put("name", rs.getString("name"));
                j.put("id", rs.getString("uid"));
                jarr.add(j);
            }
            ps.close();
        } catch (SQLException ex) {
        }
        json.put("error", false);
        json.put("data", jarr);
        // Helper.printConsole(jarr);
        return json.toJSONString();
    }

    public String getTerritoriesAsResource() {
        JSONObject json = new JSONObject();
        JSONArray jarr = new JSONArray();
        try {
            String query = "SELECT org_portal_users.assign_branch "
                    + "FROM org_portal_users "
                    + "  WHERE org_portal_users.uid=? AND org_portal_users.org_id=?";
            PreparedStatement ps = this.supplyMySql().prepareStatement(query);
            ps.setString(1, this.uid);
            ps.setString(2, this.app_code);
            Helper.printConsole(ps.toString());
            ResultSet rs = ps.executeQuery();
            String in = "";
            while (rs.next()) {
                String[] brancehs = rs.getString("assign_branch").split(",");
                if (brancehs.length > 0) {
                    for (String t : brancehs) {
                        if (!t.isEmpty()) {
                            in += "'" + t + "',";
                        }
                    }
                    in = in.substring(0, (in.length() - 1));
                }

            }
            ps.clearParameters();
            if (!in.isEmpty()) {
                ps = this.supplyMySql().prepareStatement("SELECT id,territory_name as name FROM master_territory \n"
                        + "  WHERE id IN(" + in + ") OR parent_id IN(" + in + ") \n"
                        + "  UNION \n"
                        + "    SELECT id,territory_name as name \n"
                        + "  FROM master_territory \n"
                        + "  WHERE parent_id IN(SELECT id FROM master_territory WHERE parent_id IN(" + in + "))");
                ResultSet rsin = ps.executeQuery();
                while (rsin.next()) {
                    JSONObject jsonin = new JSONObject();
                    jsonin.put("id", rsin.getString("id"));
                    jsonin.put("name", rsin.getString("name"));
                    jarr.add(jsonin);
                }
            }
            ps.close();
        } catch (SQLException ex) {
        }
        json.put("error", false);
        json.put("data", jarr);
        return json.toJSONString();
    }

    public String getMasterResoursce(String field) {
        JSONObject json = new JSONObject();
        JSONArray jarr = new JSONArray();
        String q = "";
        switch (field) {
            case "deal_status":
                q = "SELECT deal_status,deal_status_key FROM master_deal_status WHERE org_id='" + this.app_code + "' AND isActive='1' order by orderBy ASC";
                break;
        }
        try {

            PreparedStatement ps = this.supplyMySql().prepareStatement(q);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                JSONObject j = new JSONObject();
                j.put("name", rs.getString("deal_status"));
                j.put("id", rs.getString("deal_status_key"));
                jarr.add(j);
            }
            rs.close();
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        json.put("error", false);
        json.put("data", jarr);
        // Helper.printConsole(jarr);
        return json.toJSONString();
    }

    public String getDealDataById(String rid, String filter) {
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(cm.getFormIdBySlug(this.app_code, "deals"));
        Document pro = new Document();
        pro.append("deal_status_key", 1);
        pro.append("deal_status", 1);
        pro.append("ex_close_date", 1);
        pro.append("product", 1);
        Document first = col.find(new BasicDBObject("_id", rid))
                .projection(pro)
                .first();
        JSONObject inj = new JSONObject();
        if (first != null) {
            inj.put("est_close_date", first.get("ex_close_date"));
            inj.put("deal_status_key", first.get("deal_status_key"));
            inj.put("deal_status", first.get("deal_status"));
            inj.put("ex_close_date", first.get("ex_close_date"));
            inj.put("product", first.get("product"));
        }

        return inj.toJSONString();
    }

    public String getEmailsByAccount(String rid) {
        JSONArray jarr = new JSONArray();
        FindIterable<Document> find = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(this.module_id)
                .find(new BasicDBObject("acc_name_key", rid)).projection(new BasicDBObject("con_email", 1));
        // Helper.printConsole(find);
        for (Document a : find) {
            jarr.add(null != a.get("con_email") ? a.getString("con_email") : "");
        }
        find.iterator().close();

        return jarr.toJSONString();
    }

    public void closeClass() {
        this.closeMySql();
    }

}
