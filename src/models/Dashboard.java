/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBObject;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import helpers.DateParser;
import helpers.Helper;
import interfaces.BaseModal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import models.helper.CommonModel;
import org.bson.Document;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Months;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class Dashboard extends BaseModal {

    public String uid = "";
    public String getTargetVsAchiveWidget() {
        JSONArray jarr = new JSONArray();
        JSONArray jarr1 = new JSONArray();
        JSONArray jarr2 = new JSONArray();
        DateTime today = new DateTime();

        try {
            PreparedStatement ps = this.supplyMySql().prepareStatement("SELECT target_start_from,target"
                    + " from user_targets WHERE uid=? AND org_id=? AND isActive='1'");
            ps.setString(1, this.uid);
            ps.setString(2, this.app_code);
            ResultSet rs = ps.executeQuery();
            long target = 0;
            String target_start = DateTime.now().toString();

            while (rs.next()) {
                target = rs.getLong("target");
                target_start = rs.getString("target_start_from");
            }

            ps.clearParameters();

            CommonModel cm = new CommonModel();
            cm.con = this.supplyMySql();
            cm.jedis = this.supplyJedis();
            List<String> myTeamAsList = cm.getMyTeamAsList(this.uid, app_code);
            String lead = cm.getFormIdBySlug(app_code, "deals");
            MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                    .getCollection(lead);
            BasicDBObject group = new BasicDBObject();
            group.put("$group", new BasicDBObject("_id", "deal_status")
                    .append("total", new BasicDBObject("$sum", new BasicDBObject("$sum", "$product.total"))));
            BasicDBObject match = new BasicDBObject("deal_status_key", "won")
                    .append("deal_convert_by", new BasicDBObject("$in", myTeamAsList));

            AggregateIterable<Document> total = col.aggregate(Arrays.asList(
                    new BasicDBObject("$match", match.append("deal_convert_on_date_filter", new BasicDBObject(
                            "$gte", DateParser.getFirstDateOfMonth(today.monthOfYear().get(), today.year().get()))
                            .append("$lte", DateParser.getLastDateOfMonth(today.monthOfYear().get(), today.year().get())))),
                    // .append("", col)),
                    group));
            jarr.add(0, target);
            jarr2.add(0, today.monthOfYear().getAsShortText());
            jarr1.add(0, total.first() == null ? 0 : total.first().get("total"));

            total = col.aggregate(Arrays.asList(
                    new BasicDBObject("$match", match.append("deal_convert_on_date_filter", new BasicDBObject(
                            "$gte", DateParser.getFirstDateOfMonth(today.minusMonths(1).monthOfYear().get(), today.minusMonths(1).year().get()))
                            .append("$lte", DateParser.getLastDateOfMonth(today.minusMonths(1).monthOfYear().get(), today.minusMonths(1).year().get())))),
                    // .append("", col)),
                    group));

            jarr1.add(1, total.first() == null ? 0 : total.first().get("total"));
            jarr2.add(1, today.minusMonths(1).monthOfYear().getAsShortText());

            total = col.aggregate(Arrays.asList(
                    new BasicDBObject("$match", match.append("deal_convert_on_date_filter", new BasicDBObject(
                            "$gte", DateParser.getFirstDateOfMonth(today.minusMonths(2).monthOfYear().get(), today.minusMonths(2).year().get()))
                            .append("$lte", DateParser.getLastDateOfMonth(today.minusMonths(2).monthOfYear().get(), today.minusMonths(2).year().get())))),
                    // .append("", col)),
                    group));

            jarr1.add(2, total.first() == null ? 0 : total.first().get("total"));
            jarr2.add(2, today.minusMonths(2).monthOfYear().getAsShortText());
            jarr.add(1, target);
            jarr.add(2, target);
            DateTime start = DateTime.parse(target_start).toDateTime();
            int duration = Months.monthsBetween(start, today).getMonths();
            Helper.printConsole(duration + "duration");
            if (duration == 1) {
                jarr.add(1, target);
                target = 0;
                ps = this.supplyMySql().prepareStatement("SELECT target_start_from,target"
                        + " from user_targets WHERE uid=? AND org_id=? AND isActive='0' ORDER BY id DESC LIMIT 0,1");
                ps.setString(1, this.uid);
                ps.setString(2, this.app_code);
                ResultSet rs1 = ps.executeQuery();
                while (rs1.next()) {
                    target = rs1.getLong("target");
                    target_start = rs1.getString("target_start_from");
                }
                ps.clearParameters();
                jarr.add(2, target);
            }

            if (duration == 0) {
                target = 0;
                ps = this.supplyMySql().prepareStatement("SELECT target_start_from,target"
                        + " from user_targets WHERE uid=? AND org_id=? AND isActive='0' ORDER BY id DESC LIMIT 0,1");
                ps.setString(1, this.uid);
                ps.setString(2, this.app_code);
                ResultSet rs1 = ps.executeQuery();
                while (rs1.next()) {
                    target = rs1.getLong("target");
                    target_start = rs1.getString("target_start_from");
                }
                ps.clearParameters();
                jarr.add(1, target);
                DateTime startin = DateTime.parse(target_start).toDateTime();
                duration = Months.monthsBetween(startin, start).getMonths();
                if (duration == 1) {
                    ps = this.supplyMySql().prepareStatement("SELECT target"
                            + " from user_targets WHERE uid=? AND org_id=? AND isActive='0' ORDER BY id  DESC LIMIT 1,1");
                    ps.setString(1, this.uid);
                    ps.setString(2, this.app_code);
                    ResultSet rs2 = ps.executeQuery();
                    while (rs2.next()) {
                        target = rs2.getLong("target");
                    }
                    ps.clearParameters();
                    jarr.add(2, target);
                } else {
                    jarr.add(2, target);
                }

            }
            ps.close();
            JSONObject json = new JSONObject();
            json.put("targets", jarr);
            json.put("achieves", jarr1);
            json.put("months", jarr2);
            return json.toJSONString();

        } catch (SQLException ex) {
        }
        return null;
    }

    public String getAnalyticsCount() {

        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.jedis = this.supplyJedis();
        List<String> tl = cm.getMyTeamAsList(this.uid, this.app_code);
        BasicDBObject fact = new BasicDBObject();
        BasicDBObject pro = new BasicDBObject();
        String dealForm = cm.getFormIdBySlug(app_code, "deals");
        BasicDBObject in = new BasicDBObject("$in", tl);
        MongoCollection<Document> dealCol = this.supplyMongo().getDatabase(app_code)
                .getCollection(dealForm);
        List<String> nin = new ArrayList<>();
        nin.add("won");
        nin.add("closed");
        fact.append("won", Arrays.asList(
                new BasicDBObject("$match", new BasicDBObject("deal_status_key", "won")
                        .append("deal_convert_by", in)),
                new BasicDBObject("$count", "won")
        ));
        pro.append("won", new BasicDBObject("$arrayElemAt", Arrays.asList("$won.won", 0)));

        fact.append("nowon", Arrays.asList(
                new BasicDBObject("$match", new BasicDBObject("deal_status_key", new BasicDBObject(
                        "$nin", nin))
                        .append("manager_user_key", in)),
                new BasicDBObject("$count", "nowon")
        ));
        pro.append("nowon", new BasicDBObject("$arrayElemAt", Arrays.asList("$nowon.nowon", 0)));
        fact.append("lost", Arrays.asList(
                new BasicDBObject("$match", new BasicDBObject("deal_status_key", "closed")
                        .append("manager_user_key", in)),
                new BasicDBObject("$count", "lost")
        ));
        pro.append("lost", new BasicDBObject("$arrayElemAt", Arrays.asList("$lost.lost", 0)));

        AggregateIterable<Document> aggregate = dealCol.aggregate(Arrays.asList(
                new BasicDBObject("$facet", fact),
                new BasicDBObject("$project", pro)
        ));

        JSONObject j = new JSONObject();
        j.put("closed_deals", aggregate.first() != null && aggregate.first().containsKey("won")
                ? aggregate.first().get("won") : 0);
        j.put("open_deals", aggregate.first() != null && aggregate.first().containsKey("nowon")
                ? aggregate.first().get("nowon") : 0);
        j.put("lost_deals", aggregate.first() != null && aggregate.first().containsKey("lost")
                ? aggregate.first().get("lost") : 0);
        j.put("total_deals", (aggregate.first() != null && aggregate.first().containsKey("won")
                ? aggregate.first().getInteger("won") : 0)
                + (aggregate.first() != null && aggregate.first().containsKey("nowon")
                ? aggregate.first().getInteger("nowon") : 0)
                + (aggregate.first() != null && aggregate.first().containsKey("lost")
                ? aggregate.first().getInteger("lost") : 0));
        j.put("tem_size", tl.size() > 1 ? tl.size() - 1 : 0);
        return j.toJSONString();

    }

    public String getAnalyticsComparision() {

        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.jedis = this.supplyJedis();
        List<String> tl = cm.getMyTeamAsList(this.uid, this.app_code);
        BasicDBObject fact = new BasicDBObject();
        BasicDBObject pro = new BasicDBObject();
        String dealForm = cm.getFormIdBySlug(app_code, "deals");
        BasicDBObject in = new BasicDBObject("$in", tl);
        MongoCollection<Document> dealCol = this.supplyMongo().getDatabase(app_code)
                .getCollection(dealForm);
        int year = DateTime.now().getYear();
        Date curMonStart = DateParser.getFirstDateOfMonth(DateTime.now().getMonthOfYear(), year);

        // dealCol.aggregate(Arrays.asList(
        //           new BasicDBObject("$match", new BasicDBObject("deal_status_key", "won")
        //           .append("$gte", curMonStart).append("$lte", DateTime.now().toDate())),
        //           new BasicDBObject("$project", new BasicDBObject("total", new BasicDBObject("$sum", "$product.total")))
        //   )).first();               
        fact.append("currMonth", Arrays.asList(
                new BasicDBObject("$match", new BasicDBObject("deal_status_key", "won")
                        .append("deal_convert_by", in)
                        .append("deal_convert_on_date_filter", new BasicDBObject("$gte", curMonStart).append("$lte", DateTime.now().toDate()))),
                new BasicDBObject("$unwind", "$product"),
                new BasicDBObject("$group", new BasicDBObject("_id", null)
                        .append("totalSale", new BasicDBObject("$sum", "$product.total"))
                        .append("count", new BasicDBObject("$sum", 1)))
        ));
        pro.append("currMonth", new BasicDBObject("$arrayElemAt", Arrays.asList("$currMonth", 0)));
        Date lastMonStart = DateParser.getFirstDateOfMonth(DateTime.now().minusMonths(1).getMonthOfYear(), DateTime.now().minusMonths(1).getYear());
        Date lastMonEnd = DateParser.getLastDateOfMonth(DateTime.now().minusMonths(1).getMonthOfYear(), DateTime.now().minusMonths(1).getYear());

        fact.append("lastMonth", Arrays.asList(
                new BasicDBObject("$match", new BasicDBObject("deal_status_key", "won")
                        .append("deal_convert_by", in)
                        .append("deal_convert_on_date_filter", new BasicDBObject("$gte", lastMonStart).append("$lte", lastMonEnd))),
                new BasicDBObject("$unwind", "$product"),
                new BasicDBObject("$group", new BasicDBObject("_id", null)
                        .append("totalSale", new BasicDBObject("$sum", "$product.total"))
                        .append("count", new BasicDBObject("$sum", 1)))
        ));
        pro.append("lastMonth", new BasicDBObject("$arrayElemAt", Arrays.asList("$lastMonth", 0)));
        AggregateIterable<Document> stat = dealCol.aggregate(Arrays.asList(
                new BasicDBObject("$facet", fact),
                new BasicDBObject("$project", pro)
        ));
        Document first = stat.first();
        JSONObject j = new JSONObject();
        j.put("totalSale", 0);
        j.put("count", 0);
        JSONObject j1 = new JSONObject();
        j1.put("totalSale", 0);
        j1.put("count", 0);
        if(null != first)
        {
            if(first.containsKey("currMonth"))
            {
                Document Inner = first.get("currMonth",new Document());
                j.put("totalSale", Inner.get("totalSale"));
                j.put("count", Inner.get("count"));
            }
            if(first.containsKey("lastMonth"))
            {
                Document Inner = first.get("lastMonth",new Document());
                j1.put("totalSale", Inner.get("totalSale"));
                j1.put("count", Inner.get("count"));
            }
        }
        JSONObject json = new JSONObject();
        json.put("lastMonth", j1);
        json.put("currMonth", j);
        return json.toJSONString();

    }

    public String getAvgMeetingStatics() {
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.jedis = this.supplyJedis();
        Map<String,String> juniors = cm.getMyDirectJuniorMap(uid, app_code);
        
        String dealForm = cm.getFormIdBySlug(app_code, "activity");
        String naction = cm.getFormIdBySlug(app_code, "next-action");
        MongoDatabase db = this.supplyMongo().getDatabase(app_code);
        
        Date start = DateParser.getFirstDateOfMonth(DateTime.now().getMonthOfYear(), DateTime.now().getYear());
        Date end = DateParser.getEndTimeOfToday(this.tz);
        int days = Days.daysBetween(new DateTime().dayOfMonth().withMinimumValue().withTimeAtStartOfDay(),
                DateTime.now().withTimeAtStartOfDay()).getDays();
        JSONArray jarr = new JSONArray();
        juniors.entrySet().stream().map(entry -> {
            JSONObject j = new JSONObject();
            Helper.printConsole(entry.getKey());
            long count = db.getCollection(dealForm).countDocuments(
                    new BasicDBObject("manager_user_key", entry.getKey())
                            .append("meeting_date_date_filter", new BasicDBObject("$gte", start).append("$lte", end))
            );
            long countIn = db.getCollection(naction).countDocuments(
                    new BasicDBObject("manager_user_key", entry.getKey())
                            .append("date_time_date_filter", new BasicDBObject("$gte", start).append("$lte", end))
            );
            j.put("total_meetings", count);
            j.put("total_followup", countIn);
            j.put("user", entry.getValue());
            return j;
        }).forEachOrdered(j -> {
            jarr.add(j);
        });
        JSONObject j = new JSONObject();
        j.put("totaldays", days);
        j.put("stats", jarr);
        //Helper.printConsole(j);
        return j.toJSONString();

    }

    public String getDealsByTerritory(String branches) {
        try {
            CommonModel cm = new CommonModel();
            cm.con = this.supplyMySql();
            cm.jedis = this.supplyJedis();
            Map<String, String> tMap = cm.getTerritoriesRecurcive(branches);
            String form = cm.getFormIdBySlug(this.app_code, "deals");
            MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                    .getCollection(form);
            BasicDBObject fact = new BasicDBObject();
            BasicDBObject pro = new BasicDBObject();
            List<String> l = new ArrayList<>();
            List<String> tl = cm.getMyTeamAsList(this.uid, this.app_code);
            BasicDBObject in = new BasicDBObject("$in", tl);
            tMap.forEach((key, value) -> {
                fact.append(value, Arrays.asList(
                        new BasicDBObject("$match", new BasicDBObject("territory_master_key", key)
                                .append("manager_user_key", in)),
                        new BasicDBObject("$count", value)
                ));
                pro.append(value, new BasicDBObject("$arrayElemAt", Arrays.asList("$" + value + "." + value, 0)));
                l.add(value);
            });

            AggregateIterable<Document> stat = col.aggregate(Arrays.asList(
                    new BasicDBObject("$facet", fact),
                    new BasicDBObject("$project", pro)
            ));
            JSONArray jarr1 = new JSONArray();
            for(Document d : stat)
            {
               jarr1.add(d);
            }
           
            JSONObject json = new JSONObject();
            json.put("counts", jarr1);
            json.put("teritories", l);
            stat.iterator().close();
            return json.toJSONString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
     public String getNewlyCreatedAccounts()
    {
        JSONArray jarr1 = new JSONArray();

        CommonModel cm = new CommonModel();
            cm.con = this.supplyMySql();
            cm.jedis = this.supplyJedis();
             String form1 = cm.getFormIdBySlug(app_code, "accounts");
        MongoCollection<Document> col1= this.supplyMongo().getDatabase(this.app_code)
               .getCollection(form1);
        //Helper.printConsole(form1);
                        
        Date today = DateParser.getStartTimeOfToday(this.tz);
        Date end = DateParser.getEndTimeOfToday(this.tz);
         
        //Map<String, String> inqUsers1 = cm.getInquiryUsersMap(app_code);
          
            BasicDBObject fact1 = new BasicDBObject();
            BasicDBObject pro1 = new BasicDBObject();
          //  Helper.printConsole(user.getKey());
                   
            fact1.append("account_created", Arrays.asList( 
                   new BasicDBObject("$match", new BasicDBObject("isActive", true)
                           .append("createon_date_filter", new BasicDBObject("$gte", today).append("$lte", end))),
                      new BasicDBObject("$count", "account_created")
     
            ));
            pro1.append("account_created", new BasicDBObject("$arrayElemAt", Arrays.asList("$account_created.account_created", 0)));
            
            AggregateIterable<Document> stat = col1.aggregate(Arrays.asList(
                    new BasicDBObject("$facet", fact1),
                    new BasicDBObject("$project", pro1)
            ));
            for(Document d : stat)
            {
               jarr1.add(d);
            }
           
            JSONObject json = new JSONObject();
            json.put("counts", jarr1);
            stat.iterator().close();
            return json.toJSONString();
    
         
    }

   
}