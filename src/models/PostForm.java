/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBObject;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import helpers.DateParser;
import helpers.Helper;
import interfaces.BaseModal;
import interfaces.Emails;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import models.helper.CommonModel;
import models.helper.Inventory;
import models.helper.MongoModel;
import org.bson.Document;
import org.joda.time.Instant;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author viru
 */
public class PostForm extends BaseModal {

    private boolean exist = false;
    private boolean updating = false;
    private MongoModel mongoModal = new MongoModel();
    //private MongoDatabase db;
    private String formName = "";
    public String dealName = "";
    public String save(String formid, String form, JSONObject sessionJson) {

        JSONObject formJson = (JSONObject) JSONValue.parse(form);

        Document doc = new Document();

        CommonModel cm = new CommonModel();
        this.mongoModal.mc = this.supplyMongo();
        //this.db = this.mongoModal.mc.getDatabase(this.app_code);
        cm.mongo = this.mongoModal.mc;
        cm.con = this.supplyMySql();
        this.logged = sessionJson.get("uid").toString();
        String docid = UUID.randomUUID().toString();
        this.formName = cm.getFormNameById(this.app_code, formid);

        if (null != formJson.get("record_id") && !formJson.get("record_id").toString().isEmpty()) {
            this.updating = true;
            docid = formJson.get("record_id").toString();
        }
        if (formJson.containsKey("deal_convert_on")
                && null != formJson.get("deal_convert_on")
                && !formJson.get("deal_convert_on").toString().isEmpty()) {
            formJson.put("deal_convert_on_date_filter", DateParser.returnDateFromZuluString(
                    formJson.get("deal_convert_on").toString()));
        }
        if (formJson.containsKey("product")) {
            JSONArray productArr = (JSONArray) JSONValue.parse(formJson.get("product").toString());
            if (!this.updating) {
                //this.setFreshProductDetail(productArr);
            } else if (this.updating) {
                //this.updateProductDetail(productArr);
            }
            formJson.put("product", productArr);
        }

        formJson.forEach((k, v) -> {
            if (this.updating && (k.toString().equals("manager_user_key")
                    || k.toString().equals("manager_user"))) {
                return;
            }
            String fftype = cm.getFormFieldType(this.app_code, formid, k.toString());
            switch (fftype) {
                case "date":
                case "date_time":
                    String date = "";
                    if (null != v && !v.equals("")) {
                        date = v.toString();
                    } else {
                        date = Instant.now().toString();
                    }
                    doc.append(k.toString() + "_date_filter", DateParser.returnDateFromZuluString(date));
                    doc.append(k.toString(), date);
                    break;
                case "map":
                    if (null != v && !v.toString().isEmpty()) {
                        Document parse = Document.parse(v.toString());
                        doc.append(k.toString(), parse);
                    } else {
                        doc.append(k.toString(), new Document("lat", "").append("lon", ""));
                    }
                    break;
                case "product":
                    doc.append(k.toString(), v);
                    break;
                default:
                    //  Helper.printConsole(k.toString());
//                        if (v instanceof String && !v.toString().isEmpty()) {
//                            if (!k.toString().matches("(.*)_key(.*)")) {
//                                sIndex.put(k, v);
//                                sArr.add(v);
//                            }
//                        }

                    doc.append(k.toString(), v);
            }
            if (this.updating) {
                if (!k.toString().contains("_key") && !k.toString().contains("_date_filter")
                        && !k.toString().equals("record_id")
                        && !k.toString().equals("product")) {
                    String val = doc.get(k.toString()) instanceof String ? doc.getString(k.toString()) : "";
                    if (!val.isEmpty()) {
                        this.setFormActivityLogs(cm, formid, k.toString(), null != formJson.get("record_id")
                                ? formJson.get("record_id").toString() : "",
                                val, null != formJson.get("acc_name_key")
                                ? formJson.get("acc_name_key").toString() : "", fftype);
                    }

                }

            }

        });

        if (!this.updating) {
            doc.append("createon_ms", System.currentTimeMillis());
            doc.append("createon_date", Instant.now().toString());
            doc.append("createon_date_filter", Instant.now().toDate());
            doc.append("createby", this.logged);
            doc.append("last_modify_ms", System.currentTimeMillis());
            doc.append("last_modify", Instant.now().toString());
            doc.append("last_modify_by", this.logged);
        } else {
            doc.append("last_modify_ms", System.currentTimeMillis());
            doc.append("last_modify", Instant.now().toString());
            doc.append("last_modify_date_filter", Instant.now().toDate());
            doc.append("last_modify_by", this.logged);
        }

        String slug = cm.getFormSlugById(this.app_code, formid);
        switch (slug) {
            case "next-action":
                String accId = cm.getFormIdBySlug(this.app_code, "accounts");
                Document accManager = cm.getAccountDetails(this.app_code, accId, doc.getString("acc_name_key"));
                doc.append("manager_user_key", null != accManager ? accManager.getString("manager_user_key") : "");
                this.makeLastActionDeactivate(formid, doc.getString("deal_key"));
                break;
            case "activity":
                String accId2 = cm.getFormIdBySlug(this.app_code, "accounts");
                Document accManager2 = cm.getAccountDetails(this.app_code, accId2, doc.getString("acc_name_key"));
                doc.append("manager_user_key", null != accManager2 ? accManager2.getString("manager_user_key") : "");
                break;
            case "deals":
                String accId1 = cm.getFormIdBySlug(this.app_code, "accounts");
                Document accManager1 = cm.getAccountDetails(this.app_code, accId1, doc.getString("acc_name_key"));
                if (doc.containsKey("deal_status_key") && doc.getString("deal_status_key").equals("won")) {
                    doc.append("deal_convert_by", null != accManager1 ? accManager1.getString("manager_user_key") : "");
                    this.makeAccountToCustomer(accId1, doc.getString("acc_name_key"));
                }
                doc.append("territory_master_key", null != accManager1 ? accManager1.getString("territory_master_key") : "");
                doc.append("manager_user_key", null != accManager1 ? accManager1.getString("manager_user_key") : "");
                this.dealName = doc.getString("field_1579092826995");
                break;
            case "inquiries":
                if (doc.containsKey("next_followup")) {
                    doc.append("inquiry_status_key", "1584258695791");

                    doc.append("next_followup_date_filter", DateParser.returnDateFromZuluString(doc.getString("next_followup")));
                }
                break;
        }
        doc.append("isActive", true);

        this.mongoModal.db = this.app_code;
        this.mongoModal.collection = formid;
        this.mongoModal.id = this.updating ? doc.getString("record_id") : docid;
        doc.append("primary_id",
                this.updating ? this.mongoModal.getOldPrimaryId(slug.substring(0, 2).toUpperCase())
                        : this.mongoModal.getNewPrimaryId(slug.substring(0, 2).toUpperCase()));

        this.mongoModal.UpdateOrInsertDocument(doc);
        if (!this.updating) {
            AggregateIterable<Document> fl = cm.getFormFieldAndLabels(app_code, formid);
            this.setNewFormActivityLogs(fl, doc, slug, docid,sessionJson);
        }
        //sIndex.put("keywords", sArr);

        if (!slug.isEmpty() && slug.equals("activity")) {
            //this.setActivityHistory(this.mongoModal.id, doc);
        }
        return this.mongoModal.id;

    }

    public String saveAsArray(String formid, String form, JSONObject sessionJson) {
        try {
            JSONArray formArray = (JSONArray) JSONValue.parse(form);
            CommonModel cm = new CommonModel();
            this.mongoModal.mc = this.supplyMongo();
            //this.db = this.mongoModal.mc.getDatabase(this.app_code);
            cm.mongo = this.mongoModal.mc;
            cm.con = this.supplyMySql();
            this.logged = sessionJson.get("uid").toString();
            this.formName = cm.getFormNameById(this.app_code, formid);
            String slug = cm.getFormSlugById(this.app_code, formid);
            this.mongoModal.db = this.app_code;
            this.mongoModal.collection = formid;
            List<Document> list = new ArrayList<>();
            long i = 1;
            for (Object f : formArray) {
                JSONObject formJson = (JSONObject) f;
                Document doc = new Document();

                if (formJson.containsKey("deal_convert_on")
                        && null != formJson.get("deal_convert_on")
                        && !formJson.get("deal_convert_on").toString().isEmpty()) {
                    formJson.put("deal_convert_on_date_filter", DateParser.returnDateFromZuluString(
                            formJson.get("deal_convert_on").toString()));
                }
                if (formJson.containsKey("product")) {
                    JSONArray productArr = (JSONArray) JSONValue.parse(formJson.get("product").toString());
                    if (!this.updating) {
                        this.setFreshProductDetail(productArr);
                    } else if (this.updating) {
                        this.updateProductDetail(productArr);
                    }
                    formJson.put("product", productArr);
                }

                formJson.forEach((k, v) -> {
                    if (this.updating && (k.toString().equals("manager_user_key")
                            || k.toString().equals("manager_user"))) {
                        return;
                    }
                    String fftype = cm.getFormFieldType(this.app_code, formid, k.toString());
                    switch (fftype) {
                        case "date":
                        case "date_time":
                            String date = "";
                            if (null != v && !v.equals("")) {
                                date = v.toString();
                            } else {
                                date = Instant.now().toString();
                            }
                            doc.append(k.toString() + "_date_filter", DateParser.returnDateFromZuluString(date));
                            doc.append(k.toString(), date);
                            break;
                        case "map":
                            if (null != v && !v.toString().isEmpty()) {
                                Document parse = Document.parse(v.toString());
                                doc.append(k.toString(), parse);
                            } else {
                                doc.append(k.toString(), new Document("lat", "").append("lon", ""));
                            }
                            break;
                        case "product":
                            doc.append(k.toString(), v);
                            break;
                        default:
                            //  Helper.printConsole(k.toString());
//                        if (v instanceof String && !v.toString().isEmpty()) {
//                            if (!k.toString().matches("(.*)_key(.*)")) {
//                                sIndex.put(k, v);
//                                sArr.add(v);
//                            }
//                        }

                            doc.append(k.toString(), v);
                    }

                });

                doc.append("createon_ms", System.currentTimeMillis());
                doc.append("createon_date", Instant.now().toString());
                doc.append("createby", this.logged);
                doc.append("last_modify_ms", System.currentTimeMillis());
                doc.append("last_modify", Instant.now().toString());
                doc.append("last_modify_by", this.logged);

                switch (slug) {
                    case "next-action":
                        String accId = cm.getFormIdBySlug(this.app_code, "accounts");
                        Document accManager = cm.getAccountDetails(this.app_code, accId, doc.getString("acc_name_key"));
                        doc.append("manager_user_key", null != accManager ? accManager.getString("manager_user_key") : "");
                        this.makeLastActionDeactivate(formid, doc.getString("deal_key"));
                        break;
                    case "activity":
                        String accId2 = cm.getFormIdBySlug(this.app_code, "accounts");
                        Document accManager2 = cm.getAccountDetails(this.app_code, accId2, doc.getString("acc_name_key"));
                        doc.append("manager_user_key", null != accManager2 ? accManager2.getString("manager_user_key") : "");
                        break;
                    case "deals":
                        String accId1 = cm.getFormIdBySlug(this.app_code, "accounts");
                        Document accManager1 = cm.getAccountDetails(this.app_code, accId1, doc.getString("acc_name_key"));
                        if (doc.containsKey("deal_status_key") && doc.getString("deal_status_key").equals("won")) {
                            doc.append("deal_convert_by", null != accManager1 ? accManager1.getString("manager_user_key") : "");
                            this.makeAccountToCustomer(accId1, doc.getString("acc_name_key"));
                        }
                        doc.append("territory_master_key", null != accManager1 ? accManager1.getString("territory_master_key") : "");
                        doc.append("manager_user_key", null != accManager1 ? accManager1.getString("manager_user_key") : "");
                        this.dealName = doc.getString("field_1579092826995");
                        break;
                    case "inquiries":
                        if (doc.containsKey("next_followup")) {
                            doc.append("inquiry_status_key", "1584258695791");
                            doc.append("next_followup_date_filter", DateParser.returnDateFromZuluString(doc.getString("next_followup")));
                        }
                        break;
                }
                doc.append("isActive", true);
                doc.append("primary_id", this.mongoModal.getNewPrimaryIdForArray(slug.substring(0, 2).toUpperCase(), i++));
                list.add(doc);
            }
            //Helper.printConsole(list);

            this.mongoModal.UpdateOrInsertList(list);
            return this.mongoModal.id;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String saveIntegratedInquery(String form, String lead_source) {

        JSONArray formJson = (JSONArray) JSONValue.parse(form);
        try {
            CommonModel cm = new CommonModel();
            this.mongoModal.mc = this.supplyMongo();
            //this.db = this.mongoModal.mc.getDatabase(formid);
            cm.mongo = this.mongoModal.mc;
            cm.con = this.supplyMySql();
            //this.logged = sessionJson.get("uid").toString();
            this.mongoModal.db = this.app_code;
            this.mongoModal.collection = cm.getFormIdBySlug(this.app_code, "inquiries");
            for (Object f : formJson) {
                JSONObject doc = (JSONObject) f;
                doc.put("createon_ms", System.currentTimeMillis());
                doc.put("createon_date", Instant.now().toString());
                doc.put("createby", "");
                doc.put("last_modify_ms", System.currentTimeMillis());
                doc.put("last_modify", Instant.now().toString());
                doc.put("last_modify_by", "");
                doc.put("isActive", true);
                doc.put("is_new", true);
                Helper.printConsole(doc);
                this.mongoModal.id = UUID.randomUUID().toString();
                this.mongoModal.UpdateOrInsertJSONObject(doc);
            }
            if (formJson.size() > 0) {
                Helper.printConsole("count" + formJson.size());
                Helper.printConsole(lead_source);

            }

        } catch (Exception e) {
            try {
                FileWriter myWriter = new FileWriter("/var/www/html/crm/uploads/filename.txt");
                myWriter.write(e.getMessage());
                myWriter.close();
                //   Helper.printConsole("Successfully wrote to the file.");

            } catch (IOException ep) {
                // Helper.printConsole("An error occurred.");
                // e.printStackTrace();
            }
            e.printStackTrace();
        }
        return null;
    }
//    public String updateForm()
//    {
//        
//    }

    public String setFreshProductDetail(JSONArray j) {
        Inventory in = new Inventory();
        in.con = this.supplyMySql();
        if (null != j && j.size() > 0) {
            for (Object a : j) {
                JSONObject pob = (JSONObject) a;
                int q = Integer.parseInt(pob.get("quantity").toString());
                String ts = null != pob.get("tax") && !pob.get("tax").toString().isEmpty()
                        ? pob.get("tax").toString() : "0";
                double tax = Double.parseDouble(ts);
                double total = Double.parseDouble(pob.get("sale_price").toString()) * q;
                if (tax > 0) {
                    total = total + ((tax * total) / 100);
                }
                pob.put("total", total);
                // Helper.printConsole(total + " Total Rebt");
                if (in.inStock(q, pob.get("product_id").toString())) {
                    in.updateStock(q, pob.get("product_id").toString());
                }
            }
        }

        return null;
    }

    public String updateProductDetail(JSONArray j) {
        Inventory in = new Inventory();
        in.con = this.supplyMySql();
        //Helper.printConsole("Test Check");
        if (j != null && j.size() > 0) {
            for (Object a : j) {
                JSONObject pob = (JSONObject) a;
                int q = Integer.parseInt(pob.get("quantity").toString());
                String ts = null != pob.get("tax") && !pob.get("tax").toString().isEmpty()
                        ? pob.get("tax").toString() : "0";
                double tax = Double.parseDouble(ts);
                double total = Double.parseDouble(pob.get("sale_price").toString()) * q;
                if (tax > 0) {
                    total = total + ((tax * total) / 100);
                }
                pob.put("total", total);
                // Helper.printConsole(pob);
                in.reverseStock(q, pob.get("product_id").toString());
                if (in.inStock(q, pob.get("product_id").toString())) {
                    in.updateStock(q, pob.get("product_id").toString());
                }
            }
        }
        return null;
    }

    public boolean trashRecord(String modid, String recordid) {
        this.mongoModal.mc = this.supplyMongo();
        this.mongoModal.db = this.app_code;
        this.mongoModal.collection = modid;
        this.mongoModal.id = recordid;
        long update = this.mongoModal.update(new Document("isActive", false));

        return update >= 1;
    }

    public boolean cancelOrder(String modid, String recordid) {
        this.mongoModal.mc = this.supplyMongo();
        MongoCollection<Document> data = this.mongoModal.returnCollection(this.app_code, modid);
        Document first = data.find(new BasicDBObject("_id", recordid))
                .projection(new BasicDBObject("product", 1)).first();
        if (null != first) {
            ArrayList< Document> product = (ArrayList<Document>) first.get("product");
            Inventory in = new Inventory();
            in.con = this.supplyMySql();
            product.forEach((d) -> {
                in.reverseStock(Integer.parseInt(d.get("quantity").toString()), d.getString("product_id"));
            });

        }
        this.mongoModal.db = this.app_code;
        this.mongoModal.collection = modid;
        this.mongoModal.id = recordid;
        long update = this.mongoModal.update(new Document("order_canceled", true));

        return update >= 1;
    }

    public String setAppointmentTiming(int timeLaps, String datetoadded, JSONObject formJson) {
        String dateTime = "";
        dateTime = DateParser.addMinutesToDate(timeLaps, datetoadded);
        int matchMinutes = DateParser.getTotalMinutesFromDate(dateTime);
        if ((matchMinutes >= 600 && matchMinutes < 720) || (matchMinutes >= 960 && matchMinutes < 1140)) {
            formJson.put("appointment_date", dateTime);
        } else if (matchMinutes > 720 && matchMinutes < 960) {
            dateTime = DateParser.addHourMinutesToDate(960, datetoadded);
            formJson.put("appointment_date", dateTime);
        } else if (matchMinutes > 960) {
            dateTime = DateParser.addOneDayAndMinutes(600, datetoadded);
            formJson.put("appointment_date", dateTime);
        } else if (matchMinutes < 600) {
            Helper.printConsole("iam  here now");
            dateTime = DateParser.addHourMinutesToDate(600, datetoadded);
            formJson.put("appointment_date", dateTime);
        }
        return dateTime;
    }

    public boolean isExistRecordInMongo(JSONObject sessionJson, String formid, String fieldid, String value) {
        CommonModel cm = new CommonModel();
        cm.mongo = this.supplyMongo();
        this.app_code = sessionJson.get("app_code").toString();
        if (cm.isUniqueInDB(this.app_code, formid, fieldid)) {
            if (cm.isRecordExistInMongo(app_code, formid, fieldid, value)) {
                return true;
            }
        }
        return false;
    }

    public void updateSingleField(String modid, String rid, String fieldid, String value, String value_key) {

        CommonModel cm = new CommonModel();
        cm.mongo = this.supplyMongo();
        BasicDBObject update = new BasicDBObject(fieldid, value);
        if (null != value_key && !value_key.isEmpty()) {
            update.append(fieldid + "_key", value_key);
        }
        if (fieldid.equals("inquiry_status")) {
            update.append("is_new", false);
        }
        cm.mongo.getDatabase(this.app_code)
                .getCollection(modid)
                .updateOne(new BasicDBObject("_id", rid), new BasicDBObject(
                        "$set", update
                ));

        //  Helper.printConsole(updateOne.getModifiedCount() + " Updated");
    }
    boolean conExist = false;
    boolean addExist = false;

    public void convertToAccount(JSONObject sess, String modid, String rid, String terr, String assign_to) throws Exception {

        MongoCollection<Document> collection = this.supplyMongo().getDatabase(app_code)
                .getCollection(modid);
        BasicDBObject find = new BasicDBObject("_id", rid);
        Document find1 = collection.find(find).first();
        // Helper.printConsole(find1);
        Document acc = new Document();
        if (find1 != null) {
            find1.entrySet().forEach((entry) -> {
                if (!entry.getKey().matches("(.*)_date_filter(.*)")) {
                    acc.put(entry.getKey(), entry.getValue());
                }
            });

            JSONObject _con = new JSONObject();
            JSONObject _add = new JSONObject();
            MongoCollection<Document> col = this.supplyMongo().getDatabase("module_forms")
                    .getCollection(this.app_code);
            CommonModel cm = new CommonModel();
            cm.con = this.supplyMySql();
            String conFid = cm.getFormIdBySlug(app_code, "contacts");
            Document first = col.find(new BasicDBObject("_id", conFid))
                    .projection(new BasicDBObject("fields.id", 1))
                    .first();
            ArrayList<Document> cons = first.get("fields", new ArrayList<>());
            if (find1.containsKey("con_decision_maker")) {
                find1.remove("con_decision_maker");
                find1.remove("con_decision_maker_key");
            }
            //  Helper.printConsole(rid + " -- " + modid);
            for (Document s : cons) {
                if (null != find1.get(s.getString("id"))
                        && !s.getString("id").equals("acc_name")) {

                    conExist = true;
                    _con.put(s.getString("id"), find1.get(s.getString("id")));
                    if (find1.containsKey(s.get("id") + "_key")) {
                        _con.put(s.get("id") + "_key", find1.get(s.get("id") + "_key"));
                    }
                }

                acc.remove(s.getString("id") + "_key");
                acc.remove(s.getString("id"));
            }

            first.clear();
            String addFid = cm.getFormIdBySlug(app_code, "address");
            first.putAll(col.find(new BasicDBObject("_id", addFid))
                    .projection(new BasicDBObject("fields.id", 1))
                    .first());
            ArrayList<Document> adds = first.get("fields", new ArrayList<>());

            adds.stream().map((s) -> {
                if (null != find1.get(s.getString("id"))) {

                    addExist = true;
                }
                _add.put(s.getString("id"), find1.get(s.getString("id")));
                acc.remove(s.getString("id"));
                return s;
            }).filter((s) -> (find1.containsKey(s.get("id") + "_key"))).forEachOrdered((s) -> {
                _add.put(s.get("id") + "_key", find1.get(s.get("id") + "_key"));
                acc.remove(s.get("id") + "_key");
            });

            acc.remove("_id");
            acc.remove("record_id");
            acc.remove("createon_ms");
            acc.remove("next_followup_date_filter");
            acc.remove("next_followup");

            String _accId = cm.getFormIdBySlug(app_code, "accounts");
            acc.put("acc_name", find1.getString("acc_name"));
            acc.put("manager_user_key", assign_to);
            acc.put("manager_user", cm.getUserNameByUID(assign_to, this.app_code));
            acc.put("territory_master_key", terr);
            acc.put("territory_master", cm.getTerritoryNameByID(terr));
            acc.put("Enquiry_converted",true);
            // Helper.printConsole(acc + "  Here is Mine Test");
            String accid = this.save(_accId, acc.toJson(), sess);
            if (this.conExist) {
                _con.put("acc_name_key", accid);
                this.save(conFid, _con.toJSONString(), sess);
            }
            if (this.addExist) {
                _add.put("acc_name_key", accid);
                this.save(addFid, _add.toJSONString(), sess);
            }

            collection.deleteOne(find);

        }

    }

    public void assignAccountToUser(JSONObject session, String modid, String rid, String assignTo, String assignFrom) {

        MongoDatabase db = this.supplyMongo().getDatabase(app_code);
        MongoCollection<Document> collection = db.getCollection(modid);
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        String assText = cm.getUserNameByUID(assignTo, this.app_code);
        String asFText = cm.getUserNameByUID(assignFrom, this.app_code);
        if (rid.equals("all")) {

        } else {
            String[] split = rid.split(",");
            List<String> in = Arrays.asList(split);
            collection.updateMany(new BasicDBObject("_id", new BasicDBObject("$in", in)), new BasicDBObject(
                    "$set", new BasicDBObject("manager_user_key", assignTo).append("manager_user", assText)
            ));
            //Helper.printConsole(updateMany.getModifiedCount() + " modified");
            String deal = cm.getFormIdBySlug(this.app_code, "deals");
            MongoCollection<Document> collection1 = db.getCollection(deal);
            collection1.updateMany(new BasicDBObject("acc_name_key", new BasicDBObject("$in", in)), new BasicDBObject(
                    "$set", new BasicDBObject("manager_user_key", assignTo).append("manager_user", assText)
            ));
            String naction = cm.getFormIdBySlug(this.app_code, "next-action");
            MongoCollection<Document> collection2 = db.getCollection(naction);
            collection2.updateMany(new BasicDBObject("acc_name_key", new BasicDBObject("$in", in)), new BasicDBObject(
                    "$set", new BasicDBObject("last_modify_by", assignTo).append("manager_user_key", assignTo)
                            .append("assigned_to_key", assignTo)
            ));
            MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code).
                    getCollection("timeline");
            List<Document> li = new LinkedList<>();
            for (String acc : in) {
                Document doc = new Document();
                doc.put("timeline", "Account has been transfer from <b>" + asFText + "</b> to <b>" + assText + "</b>");
                doc.put("tag_with", acc);
                doc.put("created_on", DateParser.getCurrentDateASZulu());
                doc.put("created_date", new Date());
                doc.put("created_by", this.logged);
                doc.put("meta", new Document());
                doc.put("ftype", "text");
                li.add(doc);

            }
            col.insertMany(li);
            //     Helper.printConsole(cm.getMyUpperTeam(app_code, assignTo));
        }

    }

    public void assignInqueryToUser(JSONObject session, String modid, String rid, String assignTo) {

        MongoDatabase db = this.supplyMongo().getDatabase(app_code);
        MongoCollection<Document> collection = db.getCollection(modid);
        int count = 0;
        if (rid.equals("all")) {

        } else {
            String[] split = rid.split(",");
            List<String> in = Arrays.asList(split);
            count = in.size();
            collection.updateMany(new BasicDBObject("_id", new BasicDBObject("$in", in)), new BasicDBObject(
                    "$set", new BasicDBObject("follow_up_by", assignTo).append("direct_assign_to", "")
            ));
             
        }
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        String receiver = cm.getEmail(this.app_code, assignTo);
        Emails email = new Emails();
        email.setConfig();
        email.subject = "Enquiry Assigned";
        email.message = "You have Assigned " +count+" new enquiries, Please check";
        email.SendEmail(receiver);
       

    }

    private void setActivityHistory(String actid, Document data) {
        String con = data.get("con_name") != null ? " with " + data.get("con_name") : "";
        String msg = "Meeting Updated on " + DateParser.returnFormattedDateFromZulu(data.get("meeting_date").toString(), "MMM/d/y h:mm a",this.tz)
                + " " + con + "<br/> Deal :- <b> " + data.get("field_1580055617309")
                + "<br/> Remarks : " + data.get("field_1580057636833");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(app_code)
                .getCollection("timeline");
        Document doc = new Document();
        doc.put("timeline", msg);
        doc.put("tag_with", data.get("acc_name_key"));
        doc.put("created_on", DateParser.getCurrentDateASZulu());
        doc.put("created_date", new Date());
        doc.put("created_by", this.logged);
        doc.put("meta", new Document());
        //.out.println(doc);
        col.insertOne(doc);

    }

    private void makeLastActionDeactivate(String mid, String dkey) {
        this.supplyMongo().getDatabase(app_code)
                .getCollection(mid)
                .updateMany(new BasicDBObject("deal_key", dkey), new BasicDBObject(
                        "$set", new BasicDBObject("isActive", false)
                ));
    }

    public void closeClass() {
        this.closeMySql();
       // this.closeMongo();
        
    }

    private void setFormActivityLogs(CommonModel cm, String mid, String fieldid,
            String rid, String newVal, String accid, String ftype) {
        Document first = this.supplyMongo().getDatabase(this.app_code).getCollection(mid).find(new BasicDBObject(
                "_id", rid)).projection(new Document(fieldid, 1)).first();
        if (null != first && null != first.get(fieldid) && !first.getString(fieldid).equals(newVal)) {
            String label = cm.getFormFieldLabel(app_code, mid, fieldid);
            MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code).
                    getCollection("timeline");
            String oldvalue = first.getString(fieldid);
            String msg = "";
            Helper.printConsole(ftype + " i am here");
            if (ftype.equals("date") || ftype.equals("date_time")) {
                msg = "<b>" + label + "</b> updated in <b>" + this.formName + "</b><br/> <b>"
                        + DateParser.returnFormattedDateFromZulu(oldvalue, "dd/MM/yyyy hh:mm a",this.tz)
                        + "</b> updated to <b>" + DateParser.returnFormattedDateFromZulu(newVal, "dd/MM/yyyy hh:mm a",this.tz) + "</b>";
            } else {
                msg = "<b>" + label + "</b> updated in <b>" + this.formName + "</b><br/> <b>"
                        + oldvalue + "</b> updated to <b>" + newVal + "</b>";
            }
            Document doc = new Document();
            doc.put("timeline", msg);
            doc.put("tag_with", accid);
            doc.put("created_on", DateParser.getCurrentDateASZulu());
            doc.put("created_date", new Date());
            doc.put("created_by", this.logged);
            doc.put("meta", new Document());
            doc.put("ftype", ftype);
            //Helper.printConsole(doc);
            col.insertOne(doc);
        }
    }

    private void setNewFormActivityLogs(AggregateIterable<Document> fl, Document d, String s, String accid, JSONObject sessionJson) {
        String time = "";
        String tag = d.getString("acc_name_key");
         CommonModel cm = new CommonModel();
        MongoCursor<Document> iterator = fl.iterator();
        switch (s) {
            case "accounts":
                time = " New Account <b> " + d.getString("acc_name") + " </b> has been created<br/>";
                tag = accid;
                if(d.containsKey("Enquiry_converted"))
                {
                    System.out.println(this.logged+""+this.app_code);
                    time+="AssignedTo:<b>"+d.get("manager_user")+"</b><br/>AssignedBy:<b>"+sessionJson.get("name").toString()+"</b></br/>";
                }
                break;

            case "contacts":
                time = " New Contact <b> " + d.getString("con_name") + " </b> has been created with account. <br/>";
                break;
            case "address":
                time = " New Address has been created with account. <br/>";
                break;
            case "deals":
                time = " New Deal has been created . <br/>";
                break;
            case "activity":
                time = " Meeting has been updated.<br/>";
                break;
            case "next-action":
                time = " New Next action has been created.<br/>";
                break;
        }
        //CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        while (iterator.hasNext()) {
            Document labels = iterator.next();

            Document getLabel = labels.get("fields", new Document());
            if (getLabel.get("type").equals("product")) {

                JSONArray doc = (JSONArray) d.get(getLabel.getString("id"));
                time += getLabel.getString("label") + " :" + "<table class=\"table table-bordered\"><tr><th>Product</th><th>Quantity</th><th>Sale Price</th></tr>";
                for (Object p : doc) {
                    JSONObject j = (JSONObject) p;
                    time += "<tr><td>" + j.get("product") + "</td>" + "<td>" + j.get("quantity") + "</td>"
                            + "<td>" + j.get("sale_price") + "</td></tr>";

                }
                time += "</table><br/>";
            } 
            else if (getLabel.get("type").equals("autocomplete")) {
                
                if(getLabel.getString("id").equals("assigned_to"))
                {
                    time += getLabel.getString("label") + ":<b>" + (null == d.get(getLabel.getString("id")) ? " - " :  cm.getUserNameByUID(d.getString(getLabel.getString("id") + "_key"), this.app_code)) + "</b><br/>";
                   
                }
            }
            else if (getLabel.get("type").equals("hidden") && getLabel.get("id").equals("deal_convert_on")) {
                time += getLabel.getString("label") + ":<b>" + (null == d.get(getLabel.getString("id")) ? " - " : DateParser.returnFormattedDateFromZulu(d.getString(getLabel.getString("id")), "MMM dd,YYYY hh.mm aa",this.tz)) + "</b><br/>";
            } else if (getLabel.get("type").equals("date")) {
                time += getLabel.getString("label") + " : <b>" + (null == d.get(getLabel.getString("id")) ? " - " : DateParser.returnFormattedDateFromZulu(d.getString(getLabel.getString("id")), "MMM dd,YYYY",this.tz)) + "</b><br/>";
            } else if (getLabel.get("type").equals("date_time")) {
                time += getLabel.getString("label") + " : <b>" + (null == d.get(getLabel.getString("id")) ? " - " : DateParser.returnFormattedDateFromZulu(d.getString(getLabel.getString("id")), "MMM dd,YYYY hh.mm aa",this.tz)) + "</b><br/>";
            } else {
                time += getLabel.getString("label") + " : <b>" + (null == d.get(getLabel.getString("id")) ? " - " : d.get(getLabel.getString("id"))) + "</b><br/>";
            }
            // time += getLabel.getString("label") + " : <b>" + (null == d.get(getLabel.getString("id")) ? " - " : d.get(getLabel.getString("id"))) + "</b><br/>";
        }

        iterator.close();
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code).
                getCollection("timeline");
        Document doc = new Document();
        doc.put("timeline", time);
        doc.put("tag_with", tag);
        doc.put("created_on", DateParser.getCurrentDateASZulu());
        doc.put("created_date", new Date());
        doc.put("created_by", this.logged);
        doc.put("meta", new Document());
        doc.put("ftype", "text");
        Helper.printConsole(doc);
        col.insertOne(doc);

    }

    public String saveFinalSaleProducts(String data) {
        if (null != data) {
            JSONArray doc = (JSONArray) JSONValue.parse(data);
            List<Document> d = new ArrayList<>();
            MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code).
                    getCollection("final_sale_products");
            String format = String.format("%06d", col.countDocuments() + 1);
            //String sale_date=DateParser.getCurrentDateASZulu();
            // doc.add(sale_date);
            for (Object p : doc) {
                JSONObject j = (JSONObject) p;
                j.put("soid", format);
                d.add(Document.parse(j.toJSONString()));
            }
            col.insertMany(d);
            return format;

        }
        return null;
    }
    private void makeAccountToCustomer(String formid, String accid) {
        //System.out.println(app_code + " == " + formid + " == " + accid);
        this.supplyMongo().getDatabase(app_code)
                .getCollection(formid)
                .updateOne(new BasicDBObject("_id", accid), new BasicDBObject(
                        "$set", new BasicDBObject("isCustomer", true)
                ));
    }
    public void closeActions(String modid, String record) {

        CommonModel cm = new CommonModel();
        cm.mongo = this.supplyMongo();
        MongoCollection<Document> col = cm.mongo.getDatabase(this.app_code)
                .getCollection(modid);

        col.updateOne(new BasicDBObject("_id", record),
                new BasicDBObject("$set", new BasicDBObject("is_closed", true)));
       
    }
    

}
