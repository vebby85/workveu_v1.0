/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import helpers.Helper;
import interfaces.BaseModal;
import java.util.Map;
import models.helper.CommonModel;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class GetMetaForm extends BaseModal {

    public String session;
    public String page = "0";

    public String getAttachments(String modid,String doctype) {

        MongoDatabase db = this.supplyMongo().getDatabase(app_code);
        MongoCollection<Document> collection = db.getCollection("attachments");
        BasicDBObject cond = new BasicDBObject("modid", modid);
        cond .append("publish", true);
        if(!doctype.isEmpty())
        {
            cond.append("doc_type", doctype);
        }
        MongoCursor<Document> iterator = collection.find(cond)
                .projection(new BasicDBObject("upload_by", 1)
                        .append("real_name", 1)
                        .append("upload_at", 1)
                        .append("upload_at_ms", 1)
                        .append("uploaded_name", 1)
                        .append("_id", 0)).iterator();
        JSONArray arr = new JSONArray();
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        Map<String, String> users = cm.getUserMapByORG(this.app_code);

        while (iterator.hasNext()) {
            Document doc = iterator.next();
            doc.put("upload_by_text", users.get(doc.getString("upload_by")));
            arr.add(doc);
        }
        iterator.close();
        users.clear();
        return arr.toJSONString();
    }

    public String getSingleAttachment(String modid, String fileid) {
       
        MongoDatabase db = this.supplyMongo().getDatabase(app_code);
        MongoCollection<Document> collection = db.getCollection("attachments");
       // Helper.printConsole(new BasicDBObject("modid", modid).append("uploaded_name", fileid));
        Document first = collection.find(new BasicDBObject("modid", modid).append("uploaded_name", fileid))
                .projection(new BasicDBObject("dst_folder", 1)
                        .append("real_name", 1)
                        .append("mime", 1)
                        .append("size", 1)).first();
      //  Helper.printConsole(first);
        return first == null ? new Document().toJson() : first.toJson();
    }

    public String getComments(String modid) {

        MongoDatabase db = this.supplyMongo().getDatabase(app_code);
        MongoCollection<Document> collection = db.getCollection("comments");
        MongoCursor<Document> iterator = collection.find(new BasicDBObject("modid", modid))
                .projection(new BasicDBObject("comment_by", 1)
                        .append("comment", 1)
                        .append("comment_at", 1)
                        .append("comment_at_ms", 1)
                        .append("_id", 0)).iterator();
        JSONArray arr = new JSONArray();
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        Map<String, String> users = cm.getUserMapByORG(this.app_code);
        while (iterator.hasNext()) {
            Document doc = iterator.next();
            doc.put("comment_by_text", users.get(doc.getString("comment_by")));
            arr.add(doc);
        }
        users.clear();
        iterator.close();
        return arr.toJSONString();
    }

    public String getFolloups(String modid) {
        
        //Helper.printConsole("modid="+modid);
        MongoDatabase db = this.supplyMongo().getDatabase(app_code);
        MongoCollection<Document> collection = db.getCollection("inquiry_followup");
        MongoCursor<Document> iterator = collection.find(new BasicDBObject("modid", modid))
                .projection(new BasicDBObject("followup_by", 1)
                        .append("remarks", 1)
                        .append("next_followup", 1)
                        .append("followup_at", 1)
                        .append("inquiry_status",1)
                        .append("_id", 0)).iterator();
        
        JSONArray arr = new JSONArray();
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        Map<String, String> users = cm.getUserMapByORG(this.app_code);
        while (iterator.hasNext()) {
            Document doc = iterator.next();
            doc.put("followup_by_text", users.get(doc.getString("followup_by")));
            arr.add(doc);
            Helper.printConsole(arr);
        }
       // Helper.printConsole("arr="+arr);
        users.clear();
        iterator.close();
        return arr.toJSONString();
    }

    public String getEmailsByMod(String modid) {
        MongoDatabase db = this.supplyMongo().getDatabase(app_code);
        MongoCursor<Document> iterator = db.getCollection("emails")
                .find(new BasicDBObject("modid", modid))
                .projection(new BasicDBObject("_id", 0))
                .iterator();
        JSONArray jarr = new JSONArray();
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        Map<String, String> users = cm.getUserMapByORG(this.app_code);
        while (iterator.hasNext()) {
            Document doc = iterator.next();
            doc.put("sent_by_text", users.get(doc.getString("sent_by")));
            jarr.add(doc);
        }
        users.clear();
        iterator.close();
        return jarr.toJSONString();
    }

    public String getUserMetaById(String id) {
        Document user = this.supplyMongo()
                .getDatabase(this.app_code)
                .getCollection("user_meta")
                .find(new BasicDBObject("uid", id))
                .first();
        if (user != null) {
            return user.toJson();
        }
        return null;
    }
    public String getApprovals(String uid,String listSort)
    {
        BasicDBList or = new BasicDBList();
        or.add(new BasicDBObject("sent_by",uid));
        or.add(new BasicDBObject("sent_to",uid));
        FindIterable<Document> find = this.supplyMongo()
                .getDatabase(this.app_code)
                .getCollection("approvals")
                .find(new BasicDBObject(listSort, uid).append("$or", or))
                .limit(10)
                .skip(0)
                .sort(new BasicDBObject("createon_ms", -1))
                .projection(new BasicDBObject("createon_ms",0));
        MongoCursor<Document> iterator = find.iterator();
        JSONArray jarrPen = new JSONArray();
        JSONArray jarrApp = new JSONArray();
        JSONArray jarrDec = new JSONArray();
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        Map<String, String> users = cm.getUserMapByORG(this.app_code);
        while (iterator.hasNext()) {
            Document next = iterator.next();
            if(!next.getString("sent_by").isEmpty())
            {
                next.put("sent_by_text", users.get(next.getString("sent_by")));
            }
            if(!next.getString("sent_to").isEmpty())
            {
                next.put("sent_to_text", users.get(next.getString("sent_to")));
            }
            switch(next.getString("status"))
            {
                case "pending" : 
                    jarrPen.add(next);
                    break;
                case "approved" : 
                    jarrApp.add(next);
                    break;
                default :
                    jarrDec.add(next);
            }
            

        }
        iterator.close();
        JSONArray jarr = new JSONArray();
        JSONObject json = new JSONObject();
        json.put("data", jarrPen);
        json.put("id", "pending");
        json.put("name", "Pending");
        jarr.add(json);
         JSONObject json1 = new JSONObject();
        json1.put("data", jarrApp);
        json1.put("id", "approved");
        json1.put("name", "Approved");
        jarr.add(json1);
         JSONObject json2 = new JSONObject();
        json2.put("data", jarrDec);
        json2.put("id", "decline");
        json2.put("name", "Decline");
        jarr.add(json2);
        return jarr.toJSONString();
    }
    public String getQuotationId() {
            long c = this.supplyMongo().getDatabase(this.app_code).
                    getCollection("attachments").countDocuments(new BasicDBObject("doc_type", "quotations"));
            return String.format("%06d", c + 1);
    }
}
