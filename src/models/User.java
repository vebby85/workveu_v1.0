/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import helpers.Helper;
import interfaces.BaseModal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import models.helper.MongoModel;
import org.bson.Document;
import org.json.simple.JSONArray;

/**
 *
 * @author viru
 */
public class User extends BaseModal {
    public String uid = "";
   
    public void setUserBasicSettings()
    {
        this.supplyJedis().select(1);
        this.setTableColumns();
    }
    private void setTableColumns()
    {
        try {
            MongoCollection<Document> collection = this.supplyMongo().getDatabase("module_forms")
                    .getCollection(this.app_code);
            PreparedStatement ps = this.supplyMySql().prepareStatement("SELECT module_id FROM app_modules WHERE org_id=? AND module_type='form'");
            ps.setString(1, this.app_code);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                String mid = rs.getString("module_id");                
                String hget = this.supplyJedis().hget("user:settings:" + this.uid, "columns_" + mid);
              
                if(hget == null || hget.isEmpty())
                {
                    Map <String,String> m = new HashMap<>();
                    Document first = collection.find(new BasicDBObject("_id",mid))
                    .projection(new BasicDBObject("fields.id",1)).first();
                    List <Document> l = new ArrayList<>();
                    JSONArray l1 = new JSONArray();
                    if(null != first )
                    {
                        l = first.get("fields", l);
                        for(Document a : l)
                        {
                            l1.add(a.getString("id"));
                        }
                    }
                    m.put("columns_" + mid, l1.toJSONString());
                    this.supplyJedis().hset("user:settings:" + this.uid, m);
                }
            }
            ps.close();
        } catch (SQLException ex) {
          
        }
    }
    public void updateColumnSettings(String columns,String modid)
    {
        Map <String,String> m = new HashMap<>();
        Helper.printConsole(columns);
        m.put("columns_" + modid, columns);
        this.supplyJedis().select(1);
        this.supplyJedis().hset("user:settings:" + this.uid, m);
    }
    
    public void closeClass()
    {
        this.closeMySql();
    }
    public void createNewDB()
    {
        MongoModel m = new MongoModel();
        m.mc = this.supplyMongo();
        m.createDatabase(this.app_code); 
        //this.supplyElastic();
       // CreateIndexRequest request = new CreateIndexRequest(this.app_code.toLowerCase());
        //try {
        //    this.client.indices().create(request, RequestOptions.DEFAULT);
        //} catch (IOException ex) {}
    }
    
}
