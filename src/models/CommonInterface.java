/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import helpers.Helper;
import interfaces.BaseModal;
import java.util.Map;
import models.helper.CommonModel;
import models.helper.MongoModel;
import org.bson.Document;
import org.json.simple.JSONArray;

/**
 *
 * @author viru
 */
public class CommonInterface extends BaseModal {

    // private final Connection con;
    private final MongoModel mongo = new MongoModel();

    public void saveComment(String comment) {
        Document commentDoc = Document.parse(comment);
        Helper.printConsole(commentDoc);
        this.mongo.mc = this.supplyMongo();
        this.mongo.db = this.app_code;
        this.mongo.collection = "comments";
        this.mongo.insertDocumentAutoId(commentDoc);
        //this.mongo.close();
    }

    public String getComments(String record_id) {
        JSONArray jarr = new JSONArray();

        CommonModel cm = new CommonModel();
        this.mongo.mc = this.supplyMongo();

        cm.con = this.supplyMySql();
        FindIterable<Document> comments = this.mongo
                .returnCollection(app_code, "comments")
                .find(new BasicDBObject("record_id", record_id))
                .projection(new BasicDBObject("_id", 0))
                .sort(new BasicDBObject("comment_time_ms", 1));
        Map<String, String> users = cm.getUserMapByORG(this.app_code);
        for (Document s : comments) {
            s.put("by_text", users.get(s.getString("by")));
            jarr.add(s);
        }
        users.clear();
        comments.iterator().close();
        this.closeMySql();
        return jarr.toJSONString();
    }
    
}
