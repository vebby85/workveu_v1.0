/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import helpers.DateParser;
import interfaces.BaseModal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import models.helper.CommonModel;
import models.helper.FilterHelper;
import org.bson.Document;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class AnalyticsModel extends BaseModal {

    public String getInqueryCount(String modid, String fby,String fwd, String filter) {
        try {
            CommonModel cm = new CommonModel();
            cm.con = this.supplyMySql();
            cm.mongo = this.supplyMongo();
            cm.jedis = this.supplyJedis();

            BasicDBObject all = new BasicDBObject("isActive", true);
            BasicDBObject newQ = new BasicDBObject("is_new", true).append("isActive", true);
            BasicDBObject pending = new BasicDBObject("isActive", true).append("inquiry_status_key", new BasicDBObject("$ne", "1584258740752"));
            BasicDBObject notint = new BasicDBObject("inquiry_status_key", "1584258740752");
            BasicDBObject andOb = new BasicDBObject();

            if (null == fby || fby.equals("all")) {

                List<String> myTeamAsList = cm.getMyTeamAsList(this.logged, app_code);
                BasicDBObject cl1 = new BasicDBObject("direct_assign_to", new BasicDBObject("$in", myTeamAsList));
                BasicDBObject cl2 = new BasicDBObject("follow_up_by", new BasicDBObject("$in", myTeamAsList));
                BasicDBList d = new BasicDBList();
                d.add(cl1);
                d.add(cl2); 
                if (null != filter && !filter.isEmpty()) {
                    FilterHelper fh = new FilterHelper();
                    fh.app_code = this.app_code;
                    fh.modid = modid;
                    fh.cm = cm;
                    BasicDBObject filterOut = new BasicDBObject();
                    fh.applyMonoFilter(filter, filterOut);
                    all.append("$and", filterOut.get("$and"));
                    newQ.append("$and", filterOut.get("$and"));
                    pending.append("$and", filterOut.get("$and"));
                    notint.append("$and", filterOut.get("$and"));
                    andOb.append("$and", filterOut.get("$and"));
                }
                all.append("$or", d);
                newQ.append("$or", d);
                pending.append("$or", d);
                andOb.append("$or", d);
                notint.append("$or", d);

            } else {
                BasicDBObject cl1 = new BasicDBObject("direct_assign_to", fby);
                BasicDBObject cl2 = new BasicDBObject("follow_up_by", fby);
                BasicDBList d = new BasicDBList();
                d.add(cl1);
                d.add(cl2);
                all.append("$or", d);
                newQ.append("$or", d);
                pending.append("$or", d);
                andOb.append("$or", d);
                notint.append("$or", d);

            }
            Date sdate = DateParser.getStartTimeOfToday(this.tz);
            Date edate = DateParser.getEndTimeOfToday(this.tz);
            if(null != fwd && !fwd.isEmpty())
            {
               sdate = DateParser.getStartTimeOfByDate(fwd,this.tz);
               edate = DateParser.getEndTimeOfByDate(fwd,this.tz );
            }
            System.out.println(sdate);
            System.err.println(edate);
            //Helper.printConsole(edate);
            BasicDBObject fact = new BasicDBObject();
            BasicDBObject pro = new BasicDBObject();
            MongoCollection<Document> dealCol = this.supplyMongo().getDatabase(app_code)
                    .getCollection(modid);
            fact.append("all", Arrays.asList(
                    new BasicDBObject("$match", all),
                    new BasicDBObject("$count", "all")
            ));
            pro.append("all", new BasicDBObject("$arrayElemAt", Arrays.asList("$all.all", 0)));

            fact.append("new", Arrays.asList(
                    new BasicDBObject("$match", newQ),
                    new BasicDBObject("$count", "new")
            ));
            pro.append("new", new BasicDBObject("$arrayElemAt", Arrays.asList("$new.new", 0)));
            fact.append("pending", Arrays.asList(
                    new BasicDBObject("$match", pending.append(
                            "next_followup_date_filter",
                            new BasicDBObject("$lt", sdate))),
                    new BasicDBObject("$count", "pending")
            ));
            pro.append("pending", new BasicDBObject("$arrayElemAt", Arrays.asList("$pending.pending", 0)));

            fact.append("not_interested", Arrays.asList(
                    new BasicDBObject("$match", notint),
                    new BasicDBObject("$count", "not_interested")
            ));
            pro.append("not_interested", new BasicDBObject("$arrayElemAt", Arrays.asList("$not_interested.not_interested", 0)));

            BasicDBList orDate = new BasicDBList();
            BasicDBObject obj = new BasicDBObject();
            obj.put("next_followup_date_filter",
                    new BasicDBObject("$gte", sdate).append("$lte", edate)
            );
            BasicDBObject obj1 = new BasicDBObject();
            obj1.put("last_modify_date_filter",
                    new BasicDBObject("$gte", sdate).append("$lte", edate)
            );
            orDate.add(obj);
            orDate.add(obj1);
            BasicDBList and = new BasicDBList();
            //and.add(new BasicDBObject("inquiry_status_key", new BasicDBObject("$in",in)));
            and.add(new BasicDBObject("$or", orDate));
            and.add(andOb);
            //Helper.printConsole(and);
            fact.append("today", Arrays.asList(
                    new BasicDBObject("$match", new BasicDBObject("isActive", true)
                            .append("$and", and)),
                    new BasicDBObject("$count", "today")
            ));
            pro.append("today", new BasicDBObject("$arrayElemAt", Arrays.asList("$today.today", 0)));

            AggregateIterable<Document> aggregate = dealCol.aggregate(Arrays.asList(
                    new BasicDBObject("$facet", fact),
                    new BasicDBObject("$project", pro)
            ));

            JSONObject j = new JSONObject();
            j.put("all", aggregate.first() != null && aggregate.first().containsKey("all")
                    ? aggregate.first().get("all") : 0);
            j.put("new", aggregate.first() != null && aggregate.first().containsKey("new")
                    ? aggregate.first().get("new") : 0);
            j.put("pending", aggregate.first() != null && aggregate.first().containsKey("pending")
                    ? aggregate.first().get("pending") : 0);
            j.put("today", aggregate.first() != null && aggregate.first().containsKey("today")
                    ? aggregate.first().get("today") : 0);
            j.put("not_interested", aggregate.first() != null && aggregate.first().containsKey("not_interested")
                    ? aggregate.first().get("not_interested") : 0);
            aggregate.iterator().close();
            return j.toJSONString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getAccountDetailStats(String rid) {
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        BasicDBObject fact = new BasicDBObject();
        BasicDBObject pro = new BasicDBObject();
        String dealForm = cm.getFormIdBySlug(app_code, "deals");

        MongoCollection<Document> dealCol = this.supplyMongo().getDatabase(app_code)
                .getCollection(dealForm);
        List<String> nin = new ArrayList<>();
        nin.add("won");
        nin.add("closed");
        fact.append("won", Arrays.asList(
                new BasicDBObject("$match", new BasicDBObject("deal_status_key", "won")
                        .append("acc_name_key", rid)),
                new BasicDBObject("$count", "won")
        ));
        pro.append("won", new BasicDBObject("$arrayElemAt", Arrays.asList("$won.won", 0)));

        fact.append("nowon", Arrays.asList(
                new BasicDBObject("$match", new BasicDBObject("deal_status_key", new BasicDBObject(
                        "$nin", nin))
                        .append("acc_name_key", rid)),
                new BasicDBObject("$count", "nowon")
        ));
        pro.append("nowon", new BasicDBObject("$arrayElemAt", Arrays.asList("$nowon.nowon", 0)));
        fact.append("lost", Arrays.asList(
                new BasicDBObject("$match", new BasicDBObject("deal_status_key", "closed")
                        .append("acc_name_key", rid)),
                new BasicDBObject("$count", "lost")
        ));
        pro.append("lost", new BasicDBObject("$arrayElemAt", Arrays.asList("$lost.lost", 0)));

        AggregateIterable<Document> aggregate = dealCol.aggregate(Arrays.asList(
                new BasicDBObject("$facet", fact),
                new BasicDBObject("$project", pro)
        ));
        JSONObject j = new JSONObject();
        j.put("closed_deals", aggregate.first() != null && aggregate.first().containsKey("won")
                ? aggregate.first().get("won") : 0);
        j.put("open_deals", aggregate.first() != null && aggregate.first().containsKey("nowon")
                ? aggregate.first().get("nowon") : 0);
        j.put("lost_deals", aggregate.first() != null && aggregate.first().containsKey("lost")
                ? aggregate.first().get("lost") : 0);
        return j.toJSONString();

    }

}
