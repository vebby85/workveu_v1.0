/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import interfaces.BaseModal;
import org.bson.Document;
import org.json.simple.JSONArray;

/**
 *
 * @author viru
 */
public class Reminders extends BaseModal {

    public void setReminder(String data) {
        Document datain = Document.parse(data);
        this.supplyMongo().getDatabase("collective")
                .getCollection("reminders")
                .insertOne(datain);
    }

    public String getReminder(String mod, String type) {
        JSONArray jarr = new JSONArray();
        FindIterable<Document> find = this.supplyMongo().getDatabase("collective")
                .getCollection("reminders")
                .find(new BasicDBObject("modid", mod).append("type", type).append("app_code", this.app_code))
                .projection(new BasicDBObject("_id",0));
         
        for (Document s : find) {
            jarr.add(s);
        }
        find.iterator().close();
        //Helper.printConsole(jarr + " Her ");
        
        return jarr.toJSONString();
    }
}
