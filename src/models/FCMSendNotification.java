/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import helpers.Helper;
import interfaces.BaseModal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//import org.apache.http.HttpResponse;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;

/**
 *
 * @author Varinder Singh
 */
public class FCMSendNotification extends BaseModal {
    public String title = "",message="";
    public JSONObject data = new JSONObject();
    public String sendNotifiactionAndriod(String to)
    {
        String key = "AAAAmh3cj_4:APA91bE-osyBHqN_PGwRYZHal7maiyGU6LuzURVX6Lc_RJ_EQNin3K3MHa-sWQiWua_x45PqEg-LE4f9Roy-J71LO61icpdWXY37pHEk1PQXkOJenW3kv0IIVVXL7ntseFz2bKZveB8cTCDy1J7JnqqpqsLLSJYvjw";
       
          //  HttpClient client = HttpClientBuilder.create().build();
           // HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
         // post.setHeader("Content-type", "application/json");
           // post.setHeader("Authorization", "key=" + key);
            
            JSONObject messageO = new JSONObject();
            messageO.put("to", to);
            messageO.put("priority", "high");
            
            JSONObject notification = new JSONObject();
            notification.put("title", this.title);
            notification.put("body",this.message);
            this.data.put("screen", "coach");
            this.data.put("screen_id", "1");
            messageO.put("notification", notification);
            messageO.put("data", this.data);
            Helper.printConsole(messageO);
           // post.setEntity(new StringEntity(messageO.toString(), ""));
           // post.getHeaders("Authorization");
          //  HttpResponse response = client.execute(post);
           // Helper.printConsole(response);
            return null;
        
    }
    public String[] getDeviceIdDeviceTypeByUser(String uid)
    {
        String s[] = new String[2];
        try {
            PreparedStatement ps = this.supplyMySql().prepareStatement("SELECT device_id,device_type FROM user_device_ids WHERE uid=?");
            ps.setString(1, uid);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
               s[0] = rs.getString("device_id");
               s[1] = rs.getString("device_type");
            }
            this.closeMySql();
            return s;
        } catch (SQLException ex) {}
        return s;
    }
}
