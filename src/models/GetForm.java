/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import helpers.DateParser;
import helpers.Helper;
import interfaces.BaseModal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import models.helper.CommonModel;
import models.helper.FilterHelper;
import org.bson.Document;
import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author viru
 */
public class GetForm extends BaseModal {

    public String session;
    public String page = "0";
    public String limit = "10";
    public String search = "";
    public String filter = "";
    public String slug = "";

    public String getList(String modid) {
        JSONObject json = new JSONObject();
        try {
            JSONArray arr = new JSONArray();

            CommonModel cm = new CommonModel();
            cm.con = this.supplyMySql();
            cm.mongo = this.supplyMongo();
            cm.jedis = this.supplyJedis();
            this.slug = cm.getFormSlugById(app_code, modid);

            BasicDBObject findQuery = new BasicDBObject("isActive", true);
            BasicDBObject project = this.getProjection(modid);

            MongoCollection<Document> collection = this.supplyMongo().getDatabase(app_code)
                    .getCollection(modid);
            BasicDBList sin = new BasicDBList();
            if (Helper.notNull(this.search)) {

                // findQuery.append("acc_name", new BasicDBObject("$regex", this.search).append("$options", "$i"));
                BasicDBObject s1 = new BasicDBObject("acc_name", new BasicDBObject("$regex", this.search).append("$options", "$i"));
                BasicDBObject s2 = new BasicDBObject("primary_id", new BasicDBObject("$regex", this.search).append("$options", "$i"));

                sin.add(s1);
                sin.add(s2);
            }
            FilterHelper fh = new FilterHelper();
            if (Helper.notNull(this.filter)) {
                fh.app_code = app_code;
                fh.modid = modid;
                fh.cm = cm;
                fh.applyMonoFilter(this.filter, findQuery);
            }

            if (!fh.is_manager_added) {
                List<String> myTeamAsList = cm.getMyTeamAsList(this.session, app_code);
                if (fh.is_account_key) {
                    Document accountManager = cm.getAccountDetails(app_code, cm.getFormIdBySlug(this.app_code, "accounts"), fh.acoount_key);
                    String am = null != accountManager ? accountManager.getString("manager_user_key") : "";
                    if (!myTeamAsList.contains(am)) {
                        myTeamAsList.add(am);
                    }
                }
                BasicDBObject cl1 = new BasicDBObject("manager_user_key", new BasicDBObject("$in", myTeamAsList));
                BasicDBObject cl2 = new BasicDBObject("inquery_manager", this.session);

                BasicDBList d = new BasicDBList();
                d.add(cl1);
                d.add(cl2);
                if (sin.size() > 0) {
                    BasicDBList inor = new BasicDBList();
                    BasicDBObject cl4 = new BasicDBObject("$or", d);
                    inor.add(cl4);
                    BasicDBObject cl3 = new BasicDBObject("$or", sin);
                    inor.add(cl3);

                    findQuery.append("$and", inor);
                } else {
                    //  System.out.print("am in else");
                    findQuery.append("$or", d);
                }

            }

            Helper.printConsole("======================= Filter ============================== \n" + findQuery);
            //Helper.printConsole(findQuery);

            FindIterable<Document> find = collection.find(findQuery);
            if (project.size() > 0) {
                find.projection(project);
            }
            // Helper.printConsole(findQuery);
            find.sort(new BasicDBObject("last_modify_ms", -1))
                    .skip(this.pageNumber())
                    .limit(Integer.parseInt(this.limit));

            long total = collection.countDocuments(findQuery);

            //Helper.printConsole(total);
            Map<String, String> users = cm.getUserMapByORG(this.app_code);
            for (Document s : find) {
                if (s.containsKey("manager_user_key")) {
                    s.put("manager_user", users.get(s.getString("manager_user_key")));
                }
//            if (s.containsKey("acc_name") && Helper.notNull(this.search)) {
//                s.put("acc_name", s.getString("acc_name").replaceAll("(?i)" + this.search, "<span class='text-search'>" + this.search + "</span>"));
//            }
                if (this.slug.equals("deals")) {
                    s.put("total_amount", cm.getProductTotalByDeal(s.getString("_id"), app_code, modid));
                    if (s.getString("deal_status_key").equals("won")) {
                        s.put("isCustomer", true);
                    }
                }
                arr.add(s);
                //Helper.printConsole(s);
            }
            find.iterator().close();
            json.put("data", arr);
            json.put("total", total);
            // mongo.close();
            //Helper.printConsole(arr);

            // } catch (Exception e) {
            //   
            //}
            return json.toJSONString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getListAPI(String modid) {
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();
        BasicDBObject findQuery = new BasicDBObject("isActive", true);
        MongoCollection<Document> collection = this.supplyMongo().getDatabase(app_code)
                .getCollection(modid);
        if (Helper.notNull(this.search)) {
            BasicDBObject s1 = new BasicDBObject("acc_name", new BasicDBObject("$regex", this.search).append("$options", "$i"));
            BasicDBObject s2 = new BasicDBObject("primary_id", new BasicDBObject("$regex", this.search).append("$options", "$i"));
            BasicDBList sin = new BasicDBList();
            sin.add(s1);
            sin.add(s2);
            findQuery.append("$or", sin);
        }

        FindIterable<Document> find = collection.find(findQuery);
        find.projection(new BasicDBObject("acc_name", 1).append("_id", 1));
        find.skip(this.pageNumber())
                .limit(Integer.parseInt(this.limit));

        for (Document s : find) {
            arr.add(s);
        }
        find.iterator().close();
        json.put("data", arr);

        return json.toJSONString();
    }

    public String getListForApp(String modid) {
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();

        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        BasicDBObject findQuery = new BasicDBObject("isActive", true);

        BasicDBObject project = new BasicDBObject("acc_name", 1);
        project.append("acc_type", 1)
                .append("territory_master", 1)
                .append("_id", 1);

        MongoCollection<Document> collection = this.supplyMongo().getDatabase(app_code)
                .getCollection(modid);
        if (Helper.notNull(this.search)) {
            // findQuery.append("$text", new BasicDBObject("$search", this.search));
            findQuery.append("acc_name", new BasicDBObject("$regex", this.search).append("$options", "$i"));
        }
        FilterHelper fh = new FilterHelper();

        if (Helper.notNull(this.filter)) {

            fh.app_code = app_code;
            fh.modid = modid;
            fh.cm = cm;
            fh.applyMonoFilter(this.filter, findQuery);
        }
        List<String> myTeamAsList = cm.getMyTeamAsList(this.session, app_code);
        Helper.printConsole(myTeamAsList);
        Helper.printConsole(this.session);
        BasicDBObject cl1 = new BasicDBObject("manager_user_key", new BasicDBObject("$in", myTeamAsList));

        BasicDBObject cl2 = new BasicDBObject("inquery_manager", this.session);
        BasicDBList d = new BasicDBList();
        d.add(cl1);
        d.add(cl2);
        findQuery.append("$or", d);

        //Helper.printConsole(findQuery);
        FindIterable<Document> find = collection.find(findQuery);
        if (project.size() > 0) {
            find.projection(project);
        }
        find.sort(new BasicDBObject("last_modify_ms", -1))
                .skip(this.pageNumber())
                .limit(Integer.parseInt(this.limit));

        long total = collection.countDocuments(findQuery);
        Helper.printConsole(findQuery);

        String dealForm = cm.getFormIdBySlug(app_code, "deals");
        MongoCollection<Document> dealCol = this.supplyMongo().getDatabase(app_code)
                .getCollection(dealForm);
        BasicDBObject fact = new BasicDBObject();
        BasicDBObject pro = new BasicDBObject();

        List<String> nin = new LinkedList<>();
        nin.add("won");
        nin.add("closed");
        for (Document s : find) {
            if (s.containsKey("_id")) {
                fact.append("won", Arrays.asList(
                        new BasicDBObject("$match", new BasicDBObject("deal_status_key", "won")
                                .append("acc_name_key", s.get("_id"))
                                .append("deal_convert_by", new BasicDBObject("$in", myTeamAsList))),
                        new BasicDBObject("$count", "won")
                ));
                pro.append("won", new BasicDBObject("$arrayElemAt", Arrays.asList("$won.won", 0)));
                fact.append("closed", Arrays.asList(
                        new BasicDBObject("$match", new BasicDBObject("deal_status_key", "closed")
                                .append("acc_name_key", s.get("_id"))
                                .append("manager_user_key", new BasicDBObject("$in", myTeamAsList))),
                        new BasicDBObject("$count", "closed")
                ));
                pro.append("closed", new BasicDBObject("$arrayElemAt", Arrays.asList("$closed.closed", 0)));

                fact.append("nowon", Arrays.asList(
                        new BasicDBObject("$match", new BasicDBObject("deal_status_key", new BasicDBObject(
                                "$nin", nin))
                                .append("acc_name_key", s.get("_id"))
                                .append("manager_user_key", new BasicDBObject("$in", myTeamAsList))),
                        new BasicDBObject("$count", "nowon")
                ));
                pro.append("nowon", new BasicDBObject("$arrayElemAt", Arrays.asList("$nowon.nowon", 0)));

                AggregateIterable<Document> aggregate = dealCol.aggregate(Arrays.asList(
                        new BasicDBObject("$facet", fact),
                        new BasicDBObject("$project", pro)
                ));
                long won = aggregate.first() != null && aggregate.first().containsKey("won")
                        ? aggregate.first().getInteger("won") : 0;
                long closed = aggregate.first() != null && aggregate.first().containsKey("closed")
                        ? aggregate.first().getInteger("closed") : 0;
                s.put("closed_deals", closed + won);
                s.put("open_deals", aggregate.first() != null && aggregate.first().containsKey("nowon")
                        ? aggregate.first().get("nowon") : 0);

            }
            arr.add(s);
        }
        find.iterator().close();
        json.put("data", arr);
        json.put("total", total);

        return json.toJSONString();
    }

    public String getListGlobalElastic(String modid, String with) {
//        JSONObject json = new JSONObject();
//        try {
//            ElasticModel es = new ElasticModel();
//            es.app_code = this.app_code;
//            this.supplyElastic();
//            es.client = this.client;
//            json.put("data", es.search(this.search));
//            json.put("total", es.total);
//            this.closeElastic();
//        } catch (Exception e) {
//
//        }
        return null;

    }

    public String getListGlobal(String modid, String with) {
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();
        try {
            BasicDBObject project = this.getProjection(modid);
            BasicDBObject findQuery = new BasicDBObject("isActive", true);
            MongoCollection<Document> collection = this.supplyMongo().getDatabase(app_code)
                    .getCollection(modid);
            if (null != this.search && !this.search.isEmpty()) {
                // findQuery.append("$text", new BasicDBObject("$search", this.search));
                // findQuery.append("acc_name", new BasicDBObject("$regex", this.search).append("$options", "$i"));
                //findQuery.append("acc_name", new BasicDBObject("$regex", this.search).append("$options", "$i"));
                BasicDBObject s1 = new BasicDBObject("acc_name", new BasicDBObject("$regex", this.search).append("$options", "$i"));
                BasicDBObject s2 = new BasicDBObject("primary_id", new BasicDBObject("$regex", this.search).append("$options", "$i"));
                BasicDBList sin = new BasicDBList();
                sin.add(s1);
                sin.add(s2);
                findQuery.append("$or", sin);
            }
            //  Helper.printConsole(findQuery);
            //Helper.printConsole(findQuery);
            FindIterable<Document> find = collection.find(findQuery)
                    .projection(project)
                    .sort(new BasicDBObject("last_modify_ms", -1))
                    .skip(this.pageNumber())
                    .limit(Integer.parseInt(this.limit));
            long total = collection.countDocuments(findQuery);
            CommonModel cm = new CommonModel();
            cm.con = this.supplyMySql();

            Map<String, String> users = cm.getUserMapByORG(this.app_code);
            for (Document s : find) {
                JSONObject j = new JSONObject();
                s.forEach((k, v) -> {
                    switch (k) {
                        case "manager_user_key":
                            j.put("manager_user", users.get(v.toString()));
                            j.put(k, v);
                            break;
                        default:
                            j.put(k, v);
                    }
                });
                arr.add(j);
            }
            find.iterator().close();
            json.put("data", arr);
            json.put("total", total);

        } catch (NumberFormatException e) {

        }
        return json.toJSONString();
    }

    public String getListGlobalForApp(String modid) {
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();
        try {
            BasicDBObject project = new BasicDBObject();
            project.append("_id", 1);
            project.append("acc_name", 1);
            project.append("acc_type", 1);
            project.append("territory_master", 1);
            BasicDBObject findQuery = new BasicDBObject("isActive", true);
            MongoCollection<Document> collection = this.supplyMongo().getDatabase(app_code)
                    .getCollection(modid);
            if (null != this.search && !this.search.isEmpty()) {
                findQuery.append("acc_name", new BasicDBObject("$regex", this.search).append("$options", "$i"));
            }

            FindIterable<Document> find = collection.find(findQuery)
                    .projection(project)
                    .sort(new BasicDBObject("last_modify_ms", -1))
                    .skip(this.pageNumber())
                    .limit(Integer.parseInt(this.limit));
            long total = collection.countDocuments(findQuery);
            CommonModel cm = new CommonModel();
            cm.con = this.supplyMySql();
            String dealForm = cm.getFormIdBySlug(app_code, "deals");
            MongoCollection<Document> dealCol = this.supplyMongo().getDatabase(app_code)
                    .getCollection(dealForm);
            BasicDBObject fact = new BasicDBObject();
            BasicDBObject pro = new BasicDBObject();
            JSONArray or = new JSONArray();
            or.add(new BasicDBObject("deal_status_key", "won"));
            or.add(new BasicDBObject("deal_status_key", "lost"));

            for (Document s : find) {
                if (s.containsKey("_id")) {
                    fact.append("won", Arrays.asList(
                            new BasicDBObject("$match", new BasicDBObject("$or", or)
                                    .append("acc_name_key", s.get("_id"))),
                            new BasicDBObject("$count", "won")
                    ));
                    pro.append("won", new BasicDBObject("$arrayElemAt", Arrays.asList("$won.won", 0)));

                    fact.append("nowon", Arrays.asList(
                            new BasicDBObject("$match", new BasicDBObject("deal_status_key", new BasicDBObject(
                                    "$ne", "won"))
                                    .append("acc_name_key", s.get("_id"))),
                            new BasicDBObject("$count", "nowon")
                    ));
                    pro.append("nowon", new BasicDBObject("$arrayElemAt", Arrays.asList("$nowon.nowon", 0)));
                    AggregateIterable<Document> aggregate = dealCol.aggregate(Arrays.asList(
                            new BasicDBObject("$facet", fact),
                            new BasicDBObject("$project", pro)
                    ));

                    s.put("closed_deals", aggregate.first() != null && aggregate.first().containsKey("won")
                            ? aggregate.first().get("won") : 0);
                    s.put("open_deals", aggregate.first() != null && aggregate.first().containsKey("nowon")
                            ? aggregate.first().get("nowon") : 0);

                }
                arr.add(s);
            }
            find.iterator().close();
            json.put("data", arr);
            json.put("total", total);
            // mongo.close();

        } catch (NumberFormatException e) {
            json.put("error", "Number Format Exception");
            return json.toJSONString();
        }
        return json.toJSONString();
    }

    public String getInqueryList(String modid, String fby) {
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();

        BasicDBObject findQuery = new BasicDBObject("isActive", true);
        BasicDBObject project = this.getProjection(modid);
        project.append("next_followup", 1);
        MongoCollection<Document> collection = this.supplyMongo().getDatabase(app_code)
                .getCollection(modid);
        long total = 0;
        FilterHelper fh = new FilterHelper();
         fh.tz = this.tz;
        if (null != this.filter && !this.filter.isEmpty()) {
            
            fh.app_code = app_code;
            fh.modid = modid;
            fh.cm = cm;
            fh.applyMonoFilter(this.filter, findQuery);

        }
        if (null == fby || fby.equals("all")) {
            List<String> myTeamAsList = cm.getMyTeamAsList(this.session, app_code);
            BasicDBObject cl1 = new BasicDBObject("direct_assign_to", new BasicDBObject("$in", myTeamAsList));
            BasicDBObject cl2 = new BasicDBObject("follow_up_by", new BasicDBObject("$in", myTeamAsList));
            BasicDBList d = new BasicDBList();
            d.add(cl1);
            d.add(cl2);
            findQuery.append("$or", d);

        } else {
            //Helper.printConsole("hello am in else"+fby);
            BasicDBObject cl1 = new BasicDBObject("direct_assign_to", fby);
            BasicDBObject cl2 = new BasicDBObject("follow_up_by", fby);
            //findQuery.append("follow_up_by", fby);
            BasicDBList d = new BasicDBList();
            d.add(cl1);
            d.add(cl2);
            findQuery.append("$or", d);
        }
        BasicDBList sin = new BasicDBList();
        if (Helper.notNull(this.search)) {

            // findQuery.append("acc_name", new BasicDBObject("$regex", this.search).append("$options", "$i"));
            BasicDBObject s1 = new BasicDBObject("con_name", new BasicDBObject("$regex", this.search).append("$options", "$i"));
            BasicDBObject s2 = new BasicDBObject("con_phone", new BasicDBObject("$regex", this.search).append("$options", "$i"));
            BasicDBObject s3 = new BasicDBObject("acc_name", new BasicDBObject("$regex", this.search).append("$options", "$i"));

            sin.add(s1);
            sin.add(s2);
            sin.add(s3);
        }
        if (sin.size() > 0) {
            BasicDBList inor = new BasicDBList();
            BasicDBObject cl3 = new BasicDBObject("$or", sin);
            inor.add(cl3);
            findQuery.append("$and", inor);
        }
        //findQuery.append("next_followup_date_filter",
        //      new BasicDBObject("$gte",DateParser.getStartTimeOfToday()).append("$lte",DateParser.getEndTimeOfToday()));
        Helper.printConsole(findQuery);
        total = collection.countDocuments(findQuery);
        FindIterable<Document> find = collection.find(findQuery);
        if (project.size() > 0) {

            project.put("last_modify_followup", 1);
            project.put("follow_up_by", 1);
            project.put("direct_assign_to", 1);
            find.projection(project);
        }

        find.sort(new BasicDBObject("last_modify_ms", -1))
                .skip(this.pageNumber())
                .limit(Integer.parseInt(this.limit));

        // BasicDBObject findQuery1 = new BasicDBObject("isActive", true);
        // Date sdate = DateParser.getStartTimeOfToday();
        // Date edate = DateParser.getEndTimeOfToday();
        //  plusSeconds = DateTime.parse(date.toString()).plusHours(23).plusMinutes(59).plusSeconds(59);
        // Helper.printConsole(sdate);
        // findQuery1.append("next_followup_date_filter",
        //         new BasicDBObject("$gte", sdate).append("$lte", edate)
        //  );
        //long todayFolloup = collection.countDocuments(findQuery1);
        Map<String, String> users = cm.getUserMapByORG(this.app_code);
        for (Document s : find) {
            if (s.containsKey("direct_assign_to")) {
                s.put("direct_assign_to_text", users.get(s.getString("direct_assign_to")));
            }
            if (s.containsKey("follow_up_by")) {
                s.put("follow_up_by_text", users.get(s.getString("follow_up_by")));
            }

            if (s.containsKey("last_modify_followup")) {
                Date today = DateParser.returnDateFromZuluString(s.getString("last_modify_followup"));
                Date sdate = fh.todaydate.isEmpty() ? DateParser.getStartTimeOfToday(this.tz) : DateParser.getStartTimeOfByDate(fh.todaydate,this.tz);
                Date edate = fh.todaydate.isEmpty() ? DateParser.getEndTimeOfToday(this.tz): DateParser.getEndTimeOfByDate(fh.todaydate,this.tz);
                System.out.println(today);
                System.out.println(sdate);
                System.out.println(edate);
                if (today.after(sdate) 
                        && today.before(edate)) { 
                    s.put("followup_done", true);
                } else {
                    s.put("followup_done", false);
                }
            } else {
                s.put("followup_done", false);
            }
            arr.add(s);
        }
        find.iterator().close();
        users.clear();
        json.put("data", arr);
        json.put("total", total);
      //  Helper.printConsole(json);
        // mongo.close();

        return json.toJSONString();
    }

    public String getMeetingList(String modid) {
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();

        JSONArray fillarr = (JSONArray) JSONValue.parse(this.filter);
        JSONObject fill = (JSONObject) fillarr.get(0);
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        BasicDBObject findQuery = new BasicDBObject();
        BasicDBObject project = null;
        FilterHelper fh = new FilterHelper();
        fh.tz = this.tz;
        if (null != this.filter && !this.filter.isEmpty()) {
            
            fh.app_code = app_code;
            fh.cm = cm;
           

//            JSONArray fillarr = (JSONArray) JSONValue.parse(this.filter);
//            JSONObject fill = (JSONObject) fillarr.get(0);
            if (fill.get("fieldId").toString().equals("today")
                    || fill.get("fieldId").toString().equals("next_week")
                    || fill.get("fieldId").toString().equals("last_week_action")) {
                modid = cm.getFormIdBySlug(this.app_code, "next-action");
                fh.modid = modid;
                JSONArray farr = (JSONArray) fill.get("fieldValue");
                
                fh.applyTodayMeetingFilter(farr, findQuery, fill.get("fieldId").toString());
                BasicDBObject cl1 = new BasicDBObject("manager_user_key", this.session);
                BasicDBObject cl2 = new BasicDBObject("assigned_to_key", this.session);
                BasicDBList d = new BasicDBList();
                d.add(cl1);
                d.add(cl2);
                findQuery.append("$or", d);
                findQuery.append("action_key", "1580633049016");
                project = this.getProjection(modid);
                project.append("last_modify", 1);
            } else if (fill.get("fieldId").toString().equals("last_week")) {
                modid = cm.getFormIdBySlug(this.app_code, "activity");

                fh.modid = modid;
                JSONArray farr = (JSONArray) fill.get("fieldValue");
                fh.applyLastUpdatedMeetingFilter(farr, findQuery);
                findQuery.append("manager_user_key", this.session);
                project = this.getProjection(modid);
            }

        }
        MongoCollection<Document> collection = this.supplyMongo().getDatabase(app_code)
                .getCollection(modid);
        long total = 0;
        if(!fill.get("fieldId").toString().equals("today")){
            findQuery.append("isActive", true);
        }
        total = collection.countDocuments(findQuery);

       // Helper.printConsole(findQuery);

        FindIterable<Document> find = collection.find(findQuery);

        if (project.size() > 0) {
            find.projection(project);

        }

        find.sort(new BasicDBObject("last_modify_ms", -1))
                .skip(this.pageNumber())
                .limit(Integer.parseInt(this.limit));
        for (Document s : find) {
            if (s.containsKey("assigned_to_key")
                    && s.getString("assigned_to_key").equals(this.session)) {
                s.put("assigned_to", "Me");
            }
            if (fill.get("fieldId").toString().equals("today") && s.containsKey("last_modify")) {
                Date today = DateParser.returnDateFromZuluStringTZ(s.getString("last_modify"),this.tz);
                Date sdate = fh.todaydate.isEmpty() ? DateParser.getStartTimeOfToday(this.tz) : DateParser.getStartTimeOfByDate(fh.todaydate,this.tz);
                Date edate = fh.todaydate.isEmpty() ? DateParser.getEndTimeOfToday(this.tz): DateParser.getEndTimeOfByDate(fh.todaydate,this.tz);
                if (today.after(sdate) 
                        && today.before(edate)) { 
                    if (fill.get("fieldId").toString().equals("today")) {
                        s.put("lastmodify_done", true);

                    }
                }
            }
            arr.add(s);

            // }
        }

        find.iterator().close();
        json.put("data", arr);
        json.put("total", total);

        return json.toJSONString();
    }

    public String getListAction(String modid, String u, String action) {
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();
        try {

            BasicDBObject findQuery = new BasicDBObject("isActive", true);
            BasicDBObject project = this.getProjection(modid);

            MongoCollection<Document> collection = this.supplyMongo().getDatabase(app_code)
                    .getCollection(modid);
            Date start = DateParser.getStartTimeOfToday(this.tz);
            // System.out.println(start);
            Date end = DateParser.getEndTimeOfToday(this.tz);
            BasicDBObject cl1 = new BasicDBObject("manager_user_key", u);
            BasicDBObject cl2 = new BasicDBObject("assigned_to_key", u);
            BasicDBList d = new BasicDBList();
            d.add(cl1);
            d.add(cl2);
            findQuery.append("$or", d);
            switch (action) {
                case "missed":
                    findQuery.append("date_time_date_filter", new BasicDBObject("$lt", start));
                    findQuery.append("is_closed",new BasicDBObject("$exists",false));
                    break;
                case "todays":
                    findQuery.append("date_time_date_filter", new BasicDBObject("$gte", start).append("$lte", end));
                    break;
                case "month":
                    JSONObject j = (JSONObject) JSONValue.parse(this.filter);
                    start = DateParser.getStartTimeOfByDate(j.get("start").toString(),this.tz);
                    //   Helper.printConsole(j);
                    end = DateTime.parse(j.get("end").toString()).plusHours(23).plusMinutes(59).plusSeconds(59).toDate();
                    findQuery.append("date_time_date_filter", new BasicDBObject("$gt", start).append("$lt", end));
                    break;
                default:
                    findQuery.append("date_time_date_filter", new BasicDBObject("$gt", end));
                    break;

            }
            //Helper.printConsole(findQuery);
            FindIterable<Document> find = collection.find(findQuery);
//            if (project.size() > 0) {
//                project.put("is_closed", true);
//                find.projection(project);
//            }
            find.sort(new BasicDBObject("last_modify_ms", -1))
                    .skip(this.pageNumber())
                    .limit(Integer.parseInt(this.limit));
            Helper.printConsole(findQuery);
            long total = collection.countDocuments(findQuery);
            CommonModel cm = new CommonModel();
            cm.con = this.supplyMySql();
            Map<String, String> users = cm.getUserMapByORG(this.app_code);
            for (Document s : find) {
                JSONObject j = new JSONObject();
                s.forEach((k, v) -> {
                    //if (!k.matches("(.*)_date_filter(.*)")) {
                    switch (k) {
                        case "assigned_to_key":
                            if (v.equals(u)) {
                                j.put("assigned_to", "Me");
                            } else {
                                j.put("assigned_to", users.get(v.toString()));
                            }

                            j.put(k, v);
                            break;
                        default:
                            if (v.equals(session)) {
                                j.put("can_close", true);
                            }
                            j.put(k, v);
                    }

                    // }
                });

              // j.putIfAbsent("is_closed", false);
               
           
                arr.add(j);
            }
            find.iterator().close();
            users.clear();
           
            json.put("data", arr);
            json.put("total", total);
            
            // mongo.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return json.toJSONString();
    }

    public String getAccountInformation(String modid) {
        JSONObject record = new JSONObject();

        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        String conFormId = cm.getFormIdBySlug(app_code, "contacts");
        // String dealFormid = cm.getFormIdBySlug(app_code, "deals");
        MongoDatabase db = this.supplyMongo().getDatabase(app_code);
        MongoCollection<Document> collection = db.getCollection(conFormId);
        // Helper.printConsole("collection=" + conFormId);
        FindIterable<Document> find = collection.find(new BasicDBObject("acc_name_key", modid))
                .projection(new BasicDBObject("con_name", 1)
                        .append("con_email", 1)
                        .append("con_phone", 1)
                        .append("con_designation", 1)
                        .append("alt_con_no", 1)
                        .append("_id", 1));
        JSONArray arr = new JSONArray();
        for (Document s : find) {
            arr.add(s);
        }
        find.iterator().close();
        record.put("contacts", arr);
        JSONArray arr1 = new JSONArray();
        String addId = cm.getFormIdBySlug(app_code, "address");
        collection = db.getCollection(addId);
        find = collection.find(new BasicDBObject("acc_name_key", modid))
                .projection(new BasicDBObject("address_type", 1)
                        .append("address_name", 1)
                        .append("state", 1)
                        .append("city", 1)
                        .append("address", 1)
                        .append("_id", 1)
                        .append("pin_code", 1)
                        .append("country", 1));
        for (Document s : find) {
            arr1.add(s);
        }
        //Helper.printConsole("address" + arr1);
        find.iterator().close();
        record.put("address", arr1);
        // record.put("conid",conFormId);
        // record.put("dealsid",dealFormid);
        //  record.put("addressid","account_address");
        this.closeMySql();
        Helper.printConsole(record.toJSONString());
        return record.toJSONString();
    }

    public String getAccountHistory(String modid) {
        try {
            JSONObject record = new JSONObject();

            CommonModel cm = new CommonModel();
            cm.con = this.supplyMySql();
            String conFormId = cm.getFormIdBySlug(app_code, "next-action");
            // String dealFormid = cm.getFormIdBySlug(app_code, "deals");
            MongoDatabase db = this.supplyMongo().getDatabase(app_code);
            MongoCollection<Document> collection = db.getCollection(conFormId);
            //Date start = DateTime.now().withTimeAtStartOfDay().toDate();
            //Helper.printConsole(conFormId);
            Map exist = new HashMap<>();
            FindIterable<Document> find = collection.find(new BasicDBObject("acc_name_key", modid)
                    .append("isActive", true))
                    .projection(new BasicDBObject("deal_key", 1)
                            .append("deal", 1)
                            .append("date_time", 1)
                            .append("remarks", 1)
                            .append("createon_date", 1)
                            .append("createby", 1)
                            .append("manager_user_key", 1)
                            .append("con_name", 1)
                            .append("action", 1)).sort(new BasicDBObject("date_time_date_filter", -1));
            JSONArray arr = new JSONArray();
            Map<String, String> users = cm.getUserMapByORG(this.app_code);
            for (Document s : find) {

                s.put("created_by_text", users.get(s.getString("createby")));
                // Helper.printConsole(s);
                arr.add(s);
            }
            find.iterator().close();
            record.put("historyNext", arr);
            // Helper.printConsole(arr);
            arr = new JSONArray();
            // cm.con = this.supplyMySql();
            // conFormId = cm.getFormIdBySlug(app_code, "timeline");
            collection = db.getCollection("timeline");
            find = collection.find(new BasicDBObject("tag_with", modid))
                    .projection(new BasicDBObject("meta", 1)
                            .append("timeline", 1)
                            .append("created_on", 1)
                            .append("created_by", 1)
                            .append("ftype", 1)
                            .append("_id", 0)).sort(new BasicDBObject("created_date", -1));
            // Helper.printConsole(find);
            for (Document s : find) {

                s.put("created_by_text", users.get(s.getString("created_by")));
                arr.add(s);
            }
            find.iterator().close();
            record.put("historyAct", arr);
            users.clear();
            this.closeMySql();
            Helper.printConsole(record.toJSONString());
            //Helper.printConsole("in history next action"+record.get("historyNext"));
            //Helper.printConsole("in history activity"+record.get("historyAct"));
            // Helper.printConsole(record.toJSONString());
            return record.toJSONString();
        } catch (Exception e) {

        }
        return null;
    }

    public String getListAsCalender(String modid) {
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();
        BasicDBObject findQuery = new BasicDBObject("isActive", true);
        BasicDBObject project = new BasicDBObject();
        project.append("appointment_date", 1).append("field_1563416481017", 1);
        MongoCollection<Document> collection = this.supplyMongo().getDatabase(app_code)
                .getCollection(modid);

        if (null != this.filter && !this.filter.isEmpty()) {
            FilterHelper fh = new FilterHelper();
            fh.applyDateRangeFilter(this.filter, findQuery);
        }
        Helper.printConsole(findQuery);
        FindIterable<Document> find = collection.find(findQuery)
                .projection(project);
        long total = collection.countDocuments(findQuery);
        for (Document s : find) {
            JSONObject in = new JSONObject();
            in.put("title", s.getString("field_1563416481017"));
            in.put("date", s.getString("appointment_date"));
            in.put("id", s.getString("_id"));
            arr.add(in);
        }
        json.put("data", arr);
        json.put("total", total);

        return json.toJSONString();
    }

    public String getSingleRecord(String module_id, String record) {

        String output = "";
        MongoCollection<Document> collection = this.supplyMongo().getDatabase(app_code)
                .getCollection(module_id);
        BasicDBObject find = new BasicDBObject("_id", record);
        FindIterable<Document> find1 = collection.find(find);

        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.collection = this.supplyMongo().getDatabase("module_forms")
                .getCollection(app_code);
        JSONObject formFieldsByType = cm.getFormFieldsByType(module_id, "autocomplete");
        // Helper.printConsole(formFieldsByType);
        JSONObject json = new JSONObject();
        if (null != find1 && null != find1.first()) {
            Map<String, String> users = cm.getUserMapByORG(this.app_code);
            find1.first().entrySet().forEach((entry) -> {
                if (!entry.getKey().matches("(.*)_date_filter(.*)")) {
                    if (formFieldsByType.containsKey(entry.getKey())) {
                        Document ind = (Document) formFieldsByType.get(entry.getKey());
                        if (null != ind.get("field_id")) {
                            String field = "";
                            Helper.printConsole(ind.getString("autocompleteField"));
                            if (null != ind.get("autocompleteField")) {
                                switch (ind.getString("autocompleteField")) {
                                    case "app_users":
                                        field = users.get(find1.first().getString(entry.getKey() + "_key"));
                                        break;
                                    case "territory_master":
                                        field = cm.getTerritoryNameByID(find1.first().getString(entry.getKey() + "_key"));
                                        break;
                                    case "deal_status":
                                        field = cm.getDealStatusByID(find1.first().getString(entry.getKey() + "_key"));
                                        break;
                                    default:
                                        field = cm.getFieldValueWithField(app_code, ind.getString("module_id"),
                                                find1.first().getString(entry.getKey() + "_key"), ind.getString("field_id"));
                                        break;

                                }

                            }

                            json.put(entry.getKey(), field);
                        }
                    } else {
                        if (entry.getKey().equals("createby") || entry.getKey().equals("last_modify_by")) {
                            json.put(entry.getKey(), users.get(entry.getValue().toString()));
                        } else if (entry.getKey().equals("follow_up_by")) {
                            json.put("follow_up_by_text", users.get(entry.getValue().toString()));
                        } else if (entry.getKey().equals("direct_assign_to")) {
                            json.put("direct_assign_to_text", users.get(entry.getValue().toString()));
                        } else {
                            json.put(entry.getKey(), entry.getValue());
                        }
                    }

                }

            });
        }
        // json.put("comments_count", cm.getCommentCount(app_code, record));
        boolean can_view_details = true;
        this.slug = cm.getFormSlugById(this.app_code, module_id);
        cm.jedis = this.supplyJedis();
        List<String> myTeam = cm.getMyTeamAsList(this.session, app_code);
        if (null != json.get("manager_user_key") && this.slug.equals("accounts")) {
            if (!myTeam.contains(json.get("manager_user_key").toString())) {
                can_view_details = false;
            }
        }
        BasicDBObject findQuery = new BasicDBObject("isActive", true);
        BasicDBObject cl1 = new BasicDBObject("manager_user_key", new BasicDBObject("$in", myTeam));
        BasicDBObject cl2 = new BasicDBObject("inquery_manager", this.session);
        BasicDBList d = new BasicDBList();
        d.add(cl1);
        d.add(cl2);
        findQuery.append("$or", d);
        json.put("can_view_detail", can_view_details);
        json.put("next_record_id", cm.getNextRecordId(app_code, module_id, record, findQuery, json.get("last_modify_ms")));
        json.put("prev_record_id", cm.getPrevRecordId(app_code, module_id, record, findQuery, json.get("last_modify_ms")));
        output = json.toJSONString();
        this.closeMySql();
        return output;
    }

    public String getDealManager(String uid, String uname, String ro) {
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        JSONObject myteam = cm.getMyTeamDealManager(uid, app_code, uname, ro);
        return myteam.toJSONString();
    }

    public String getDealManagerAsTree(String uid, String uname, String ro) {
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        JSONArray myteam = cm.getMyTeamDealManagerAsTree(uid, app_code, uname, ro);
        return myteam.toJSONString();
    }

    public String getActionManager(String uid, String uname, String ro) {
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        Helper.printConsole(uid);
        JSONObject myteam = cm.getMyTeamActionManager(uid, app_code, uname, ro);
        return myteam.toJSONString();
    }

    public String getActionManagerAsTree(String uid, String uname, String ro) {
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        cm.tz = this.tz;
        JSONArray myteam = cm.getMyTeamActionManagerAsTree(uid, app_code, uname, ro);
        return myteam.toJSONString();
    }

    private int pageNumber() {
        int pageLocal = Integer.parseInt(this.page);
        if (pageLocal == 0) {
            return 0;
        }

        return (Integer.parseInt(this.limit) * pageLocal);
    }

    private JSONArray resolveProduct() {
        return null;
    }

    private BasicDBObject getProjection(String modid) {
        String columns;
        this.supplyJedis().select(1);
        columns = this.supplyJedis().hget("user:settings:" + this.session, "columns_" + modid);
        JSONArray pro = (JSONArray) JSONValue.parse(columns);
        BasicDBObject project = new BasicDBObject();
        pro.forEach((s) -> {
            project.append(s.toString(), 1);
            project.append(s.toString() + "_key", 1);
        });
        return project;
    }

}
