/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBObject;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import helpers.DateParser;
import interfaces.BaseModal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import models.helper.CommonModel;
import org.bson.Document;
import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author viru
 */
public class DataList extends BaseModal
{
        public String filter = "";

    public String initCalendarList(String start,String end)
    {
      
        Date date = DateParser.getStartTimeOfByDate(start,this.tz);
        DateTime plusSeconds = DateTime.parse(end).plusHours(23).plusMinutes(59).plusSeconds(59);
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.jedis = this.supplyJedis();
        String form = cm.getFormIdBySlug(this.app_code, "next-action");
        BasicDBObject findQuery = new BasicDBObject("isActive", true);
        List<String> myTeamAsList = cm.getMyTeamAsList(this.logged, app_code);
        findQuery.append("manager_user_key", new BasicDBObject("$in", myTeamAsList));
        findQuery.append("date_time_date_filter", new BasicDBObject("$gte", date)
                .append("$lte", plusSeconds.toDate()));
      
        FindIterable<Document> find = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(form)
                .find(findQuery)
                .projection(new BasicDBObject("acc_name",1)
                .append("_id", 1)
                .append("action", 1)
                .append("date_time", 1)
                .append("remarks", 1));
        JSONArray jarr = new JSONArray();
        for(Document d : find)
        {
            jarr.add(d);
        }
          
        return jarr.toJSONString();
        
    } 
    
//     public List<Map<String,Object>> listOfProducts(String app_code)
//        {
//        CommonModel cm = new CommonModel();
//        cm.con = this.supplyMySql();
//        cm.mongo = this.supplyMongo();
//        cm.jedis = this.supplyJedis();
//      //List<Map<String,Object>> map=cm.getAllProduct(this.app_code);
//       // Helper.printConsole(map);
//        return map;    
//        }

//    public List<Map<String,Object>> listOfCategories(String app_code)
//        {
//        CommonModel cm = new CommonModel();
//        cm.con = this.supplyMySql();
//        cm.mongo = this.supplyMongo();
//        cm.jedis = this.supplyJedis();
//       
//         List<Map<String,Object>>list=cm.getAllCategories(this.app_code);
//        return list;    
//        }
//    
//       public List<Map<String,Object>> listOfSuppliers(String app_code)
//        {
//        CommonModel cm = new CommonModel();
//        cm.con = this.supplyMySql();
//        cm.mongo = this.supplyMongo();
//        cm.jedis = this.supplyJedis();
//        List<Map<String,Object>> list =cm.getAllSupplier(this.app_code);
//        return list;    
//        }
//        
        public String getSaleStats() {
            
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        String deal = cm.getFormIdBySlug(app_code, "deals");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(deal);
        JSONObject fil = (JSONObject) JSONValue.parse(this.filter);
        String uid = null != fil.get("uid") && !fil.get("uid").toString().isEmpty()
                ? fil.get("uid").toString()
                : fil.get("my_team").toString();
        int month = Integer.parseInt(fil.get("month").toString());
        int year = Integer.parseInt(fil.get("year").toString());
         
         BasicDBObject clause2
                = new BasicDBObject("$gte", DateParser.getFirstDateOfMonth(month, year))
                        .append("$lte", DateParser.getLastDateOfMonth(month, year));

        JSONArray headArr = new JSONArray();
        JSONArray dataArr = new JSONArray();
        JSONObject heads = new JSONObject();
       
        heads.put(" type", "u");
        heads.put("id", "uname");
        heads.put("label", "Employee");
        headArr.add(heads);

        JSONObject heads2 = new JSONObject();
        heads2.put("type", "p");
        heads2.put("id", "number_sale");
        heads2.put("label", "Total Number Sales");
        headArr.add(heads2);
        
        JSONObject heads3 = new JSONObject();
        heads3.put("type", "price");
        heads3.put("id", "total_sale");
        heads3.put("label", "Total Sale");
        headArr.add(heads3);
      
           JSONObject cols1 = new JSONObject();
           
             List<String> myTeamAsList = cm.getMyDirectJunior(uid, app_code);
        
           //   Helper.printConsole(myTeamAsList); 
          
               AggregateIterable<Document> aggregate = col.aggregate(Arrays.asList(
                 new BasicDBObject("$match", new BasicDBObject("deal_status_key", "won")
                .append("deal_convert_by", new BasicDBObject("$in", myTeamAsList))
                 .append("deal_convert_on_date_filter", clause2)),
                  new BasicDBObject("$unwind", "$product"),
                  new BasicDBObject("$group", new BasicDBObject(
                            new BasicDBObject("_id","$deal_convert_by")
                                  .append("total", new BasicDBObject("$sum", "$product.total"))
                                    .append("count", new BasicDBObject("$sum", 1)))),
                     new BasicDBObject("$sort",new BasicDBObject("total_amount",1)),
                     new BasicDBObject("$limit",5)

               ) );
                                
            //JSONObject p=new JSONObject();
             for(Document s :aggregate) {
                  // p.put("uname",cm.getUserNameByUID(s.get("_id").toString(),this.app_code));
                   //dataArr.add(p);
                   dataArr.add(s);
            // Helper.printConsole(s);
             }
              AggregateIterable<Document> aggregate1= col.aggregate(Arrays.asList(
                 new BasicDBObject("$match", new BasicDBObject("deal_status_key", "won")
                .append("deal_convert_by", new BasicDBObject("$in", myTeamAsList))
                 .append("deal_convert_on_date_filter", clause2)),
                  new BasicDBObject("$unwind", "$product"),
                  new BasicDBObject("$group", new BasicDBObject(
                            new BasicDBObject("_id","$deal_convert_by")
                                  .append("total", new BasicDBObject("$sum", "$product.total"))
                                    .append("count", new BasicDBObject("$sum", 1)))),
                     new BasicDBObject("$sort",new BasicDBObject("total_amount",-1)),
                     new BasicDBObject("$limit",5)

               ) );
                                
            JSONObject p1=new JSONObject();
            JSONArray dataArr1=new JSONArray();
             for(Document s :aggregate1) {
                   //p1.put("uname",cm.getUserNameByUID(s.get("_id").toString(),this.app_code));
                   //dataArr1.add(p);
                   dataArr1.add(s);
        //     Helper.printConsole(s);
             }
        cols1.put("heads", headArr);
        cols1.put("data",dataArr);
        cols1.put("data1",dataArr1);
        
        
        return cols1.toJSONString();
        
        }
 
       public String SalesStatsByProducts()
       {
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        String deal = cm.getFormIdBySlug(app_code, "deals");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(deal);
        JSONObject fil = (JSONObject) JSONValue.parse(this.filter);
        int month = Integer.parseInt(fil.get("month").toString());
        int year = Integer.parseInt(fil.get("year").toString());
        JSONArray dataArr=new JSONArray();
        JSONArray ascdataArr=new JSONArray();
        JSONArray descdataArr=new JSONArray();
        BasicDBObject clause2
                = new BasicDBObject("$gte", DateParser.getFirstDateOfMonth(month, year))
                        .append("$lte", DateParser.getLastDateOfMonth(month, year));
 
        BasicDBObject matchQuery= new BasicDBObject("$match", new BasicDBObject("deal_status_key", "won")
                .append("deal_convert_on_date_filter", clause2));
        BasicDBObject Query2= new BasicDBObject("$unwind", "$product");
        BasicDBObject Query3=new BasicDBObject("$group", new BasicDBObject(
                          new BasicDBObject("_id","$product.product")
                          .append("total Amount",new BasicDBObject("$sum","$product.total"))
                          .append("quantity",new BasicDBObject("$sum","$product.quantity"))));
                 
                  AggregateIterable<Document> aggregate1 = col.aggregate(Arrays.asList(
                          matchQuery,Query2,Query3,  
                    new BasicDBObject("$sort",new BasicDBObject("quantity",-1)),
                    new BasicDBObject("$limit",5)));
                  
              for(Document s :aggregate1) {
              //   Helper.printConsole(s);  
                 descdataArr.add(s);            
             }    
                  
         AggregateIterable<Document> aggregate = col.aggregate(Arrays.asList(
                 matchQuery,Query2,Query3, 
                 new BasicDBObject("$sort",new BasicDBObject("quantity",1)),
                 new BasicDBObject("$limit",5)
               ) );
                                
             for(Document s :aggregate) {
               //  Helper.printConsole(s);  
                 ascdataArr.add(s);            
             }
            dataArr.add(ascdataArr);
            dataArr.add(descdataArr);
         return dataArr.toJSONString();
        
        }
 

}
