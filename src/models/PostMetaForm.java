/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import helpers.DateParser;
import helpers.Helper;
import interfaces.BaseModal;
import interfaces.Emails;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import models.helper.CommonModel;
import models.helper.MongoModel;
import org.bson.Document;
import org.joda.time.Instant;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class PostMetaForm extends BaseModal {

    private boolean exist = false;
    private boolean updating = false;
    private final MongoModel mongoModal = new MongoModel();

    public void saveComments(String data) {
        this.mongoModal.mc = this.supplyMongo();
        this.mongoModal.db = this.app_code;
        this.mongoModal.collection = "comments";
        this.mongoModal.id = UUID.randomUUID().toString();
        Document parse = Document.parse(data);
        this.mongoModal.insertDocument(parse);
    }

    public void saveAttachment(String data) {
        this.mongoModal.mc = this.supplyMongo();
        this.mongoModal.db = this.app_code;
        this.mongoModal.collection = "attachments";
        this.mongoModal.id = UUID.randomUUID().toString();
        Document parse = Document.parse(data);
        this.mongoModal.insertDocument(parse);
    }

    public void saveApprovals(String data) {
        this.mongoModal.mc = this.supplyMongo();
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        this.mongoModal.db = this.app_code;
        this.mongoModal.collection = "approvals";
        this.mongoModal.id = UUID.randomUUID().toString();
        Document parse = Document.parse(data);
        parse.put("createon_ms", System.currentTimeMillis());
        parse.put("createon_date", Instant.now().toString());
        this.mongoModal.insertDocument(parse);

        Notifications noti = new Notifications();
        noti.app_code = this.app_code;
        JSONObject jin = new JSONObject();
        jin.put("content", "<b>" + cm.getUserNameByUID(parse.getString("sent_by"), this.app_code) + "</b> sent request for Sale order Approval ");
        JSONArray jarr = new JSONArray();
        jarr.add(parse.getString("sent_by"));
        jin.put("to", jarr);
        jin.put("screen", "approvals");
        jin.put("meta", new ArrayList<String>());
        jin.put("type", "approvals");
        jin.put("head", "Sale Order Approval");
        jin.put("generation", "soapproval");
        noti.data = jin.toJSONString();
        noti.setNotification();
        try{
        Emails email=new Emails();
        email.setConfig();
        email.subject="Sale Order for Approval";
        email.message="New Sale order is generated and sent to you for approval";
           String receiver=cm.getEmail(app_code,parse.getString("sent_by"));
           Helper.printConsole(receiver);
           Helper.printConsole(email.message);
          email.SendEmail(receiver);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void savePayments(String data) {
        this.mongoModal.mc = this.supplyMongo();
        this.mongoModal.db = this.app_code;
        this.mongoModal.collection = "payments";
        this.mongoModal.id = UUID.randomUUID().toString();
        Document parse = Document.parse(data);
        parse.put("createon_ms", System.currentTimeMillis());
        parse.put("createon_date", Instant.now().toString());
        String npd = parse.getString("next_pay_date");
        parse.put("next_pay_date", DateParser.returnDateFromZuluString(npd));
      //  Helper.printConsole(parse);
        this.mongoModal.insertDocument(parse);
    }

    public void updateFollowup(String data, String formid) {
        this.mongoModal.mc = this.supplyMongo();
        this.mongoModal.db = this.app_code;
        this.mongoModal.collection = "inquiry_followup";
        this.mongoModal.id = UUID.randomUUID().toString();
        Document parse = Document.parse(data);
        this.mongoModal.insertDocument(parse);
        this.mongoModal.collection = formid;
        this.mongoModal.id = parse.getString("modid");
        this.mongoModal.UpdateOrInsertDocument(new Document("next_followup", parse.getString("next_followup"))
                .append("remarks", parse.getString("remarks"))
                .append("next_followup_date_filter", DateParser.returnDateFromZuluString(parse.getString("next_followup")))
                .append("last_modify_date_filter", DateParser.returnDateFromZuluString(Instant.now().toString()))
                .append("last_modify_followup", Instant.now().toString())
                .append("followup_done", true));
               
    }

    public String saveSimpleEmail(String data) {
        this.mongoModal.mc = this.supplyMongo();
        this.mongoModal.db = this.app_code;
        this.mongoModal.collection = "emails";
        this.mongoModal.id = UUID.randomUUID().toString();
        Document parse = Document.parse(data);
        parse.put("sent_status", "Pending");
        this.mongoModal.insertDocument(parse);
        this.setFormActivityLogs(parse, "");
        return this.mongoModal.id;
    }

    public void updateEmailById(String uid, Document d) {
        this.mongoModal.mc = this.supplyMongo();
        this.mongoModal.db = this.app_code;
        this.mongoModal.collection = "emails";
        this.mongoModal.id = uid;
        this.mongoModal.update(d);
    }

    public void updateDirectAssignInquery(String uid, String source) {
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        String col = cm.getFormIdBySlug(this.app_code, "inquiries");
        this.supplyMongo().getDatabase(app_code)
                .getCollection(col)
                .updateMany(new BasicDBObject("follow_up_by", "").append("deal_source_key", source),
                        new BasicDBObject("$set", new BasicDBObject("direct_assign_to", uid)));

    }

    public void updateUserMeta(String meta, String uid, String type) {
        MongoCollection<Document> col = this.supplyMongo()
                .getDatabase(this.app_code)
                .getCollection("user_meta");
        Document parse = Document.parse(meta);

        if (type.equals("meta")) {
            col.updateOne(new BasicDBObject("uid", uid), new BasicDBObject("$set", parse), new UpdateOptions().upsert(true));
        } else if (type.equals("exp")) {
            col.updateOne(new BasicDBObject("uid", uid),
                    new BasicDBObject("$push", new BasicDBObject("experince", parse)), new UpdateOptions().upsert(true));
        }

    }

    private void setFormActivityLogs(Document email, String ftype) {

        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code).
                getCollection("timeline");
        //CommonModel cm = new CommonModel();
        // cm.con = this.supplyMySql();
        // String u = cm.getUserNameByUID(email.getString("sent_by"), this.app_code);
        ArrayList<String> to = email.get("email_to", new ArrayList<>());
        ArrayList<String> cc = email.get("email_cc", new ArrayList<>());
        String msg = "Email Sent "
                + "<br/>To : <b>" + String.join(",", to) + "</b>"
                + "<br/>CC : <b>" + String.join(",", cc) + "</b>"
                + "<br/>Subject : <b>" + email.getString("subject") + "</b>"
                + "<br/>Messgae : <b>" + email.getString("message") + "</b>";

        Document doc = new Document();
        doc.put("timeline", msg);
        doc.put("tag_with", email.getString("modid"));
        doc.put("created_on", DateParser.getCurrentDateASZulu());
        doc.put("created_date", new Date());
        doc.put("created_by", email.getString("sent_by"));
        doc.put("meta", new Document());
        doc.put("ftype", ftype);
        //Helper.printConsole(doc);
        col.insertOne(doc);

    }

    public String updateApproval(String uid, String apId, String stTo, String cat) {
        MongoDatabase db = this.supplyMongo().getDatabase(this.app_code);
        MongoCollection<Document> col = db.
                getCollection("approvals");

       // Helper.printConsole(cat);
        switch (cat) {
            case "so_approval":
                UpdateResult updateOne = col.updateOne(new BasicDBObject("_id", apId),
                        new BasicDBObject("$set", new BasicDBObject("status", stTo)
                                .append("approve_by", uid)
                                .append("appoval_time", DateParser.getCurrentDateASZulu())),
                        new UpdateOptions().upsert(true));
                Document first = col.find(new BasicDBObject("_id", apId))
                        .projection(new BasicDBObject("soid", 1).append("sent_by", 1))
                        .first();
                if (Helper.notNullDocument(first)) {
                    String soid = first.getString("soid");
                    String sentBy = first.getString("sent_by");
                    db.getCollection("final_sale_products")
                            .updateMany(new BasicDBObject("soid", soid),
                                    new BasicDBObject("$set", new BasicDBObject("publish", true)));
                    db.getCollection("attachments")
                            .updateMany(new BasicDBObject("soid", soid),
                                    new BasicDBObject("$set", new BasicDBObject("publish", true)));
                    Notifications noti = new Notifications();
                    noti.app_code = this.app_code;
                    JSONObject jin = new JSONObject();
                    jin.put("content", "Your request for SO Order " + soid + " has been approved ");
                    JSONArray jarr = new JSONArray();
                    jarr.add(sentBy);
                    jin.put("to", jarr);
                    jin.put("meta", new ArrayList<String>());
                    jin.put("type", "normal");
                    jin.put("generation", "soapproval");
                    noti.data = jin.toJSONString();
                    noti.setNotification();
                }
                break;

        }
        return null;
    }
}
