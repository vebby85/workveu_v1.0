/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.helper;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import helpers.DateParser;
import helpers.Helper;
import java.util.Date;
import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author viru
 */
public class FilterHelper {

    public String app_code = "";
    public String modid = "";
    public boolean is_manager_added = false;
    public boolean is_account_key = false;
    public String acoount_key = "";
    public String todaydate = "";
    public String tz = "";
    public CommonModel cm;

    public void applyMonoFilter(String filter, BasicDBObject apply) {
        // try {
        BasicDBObject inapply = new BasicDBObject();
        JSONArray outer = (JSONArray) JSONValue.parse(filter);
        BasicDBList list = new BasicDBList();
        int size = outer.size();
        int i = 0;
        for (Object inner : outer) {
            System.out.println(i++);
            JSONObject innerJson = (JSONObject) inner;
            String field = innerJson.get("fieldId").toString();
            // System.out.print("field="+field);
            JSONArray valueArr = (JSONArray) innerJson.get("fieldValue");
            Helper.printConsole("valueArr=" + valueArr);

            String type = "";
            if (field.equals("next_followup")) {
                type = "date";
//                    BasicDBList in = new BasicDBList();
//                    in.add("1584258731645");
//                    in.add("1596809210957");
//                    inapply.append("inquiry_status_key", new BasicDBObject("$in",in));
                this.todaydate = valueArr.get(0).toString();
            } else {
                type = this.cm.getFormFieldType(app_code, modid, field);
            }

            switch (type) {
                case "date":
                case "date_time":

                    Date date = DateParser.getStartTimeOfByDate(valueArr.get(0).toString(), this.tz);
                    //Date dateTo = DateParser.returnDateFromZuluString(valueArr.get(1).toString());
                    Date plusSeconds;
                    if (valueArr.size() > 1) {
                        plusSeconds = DateParser.getEndTimeOfByDate(valueArr.get(1).toString(), this.tz);
                    } else {
                        plusSeconds = DateParser.getEndTimeOfByDate(valueArr.get(0).toString(), this.tz);
                    }
                    Helper.printConsole(date + " ==============================");
                    Helper.printConsole(plusSeconds + " =============================");
                    if (field.equals("next_followup")) {
                        BasicDBList orDate = new BasicDBList();
                        BasicDBObject obj = new BasicDBObject();
                        obj.put(field + "_date_filter",
                                new BasicDBObject("$gte", date).append("$lte", plusSeconds)
                        );
                        BasicDBObject obj1 = new BasicDBObject();
                        obj1.put("last_modify_date_filter",
                                new BasicDBObject("$gte", date).append("$lte", plusSeconds)
                        );
                        orDate.add(obj);
                        orDate.add(obj1);
                        inapply.append("$or", orDate);

                    } else {
                        inapply.append(field + "_date_filter",
                                new BasicDBObject("$gte", date).append("$lte", plusSeconds)
                        );
                    }
                    break;
                case "autocomplete":
                    inapply.append(field + "_key", new BasicDBObject("$in", valueArr));
                    if (field.equals("manager_user")) {
                        this.is_manager_added = true;
                    } else if (field.equals("acc_name")) {
                        this.is_account_key = true;
                        this.acoount_key = valueArr.get(0).toString();
                    }
                    break;
                case "select":
                    inapply.append(field + "_key", new BasicDBObject("$in", valueArr));
                    if (field.equals("manager_user")) {
                        this.is_manager_added = true;

                    }
                    break;
                default:
                    if (field.equals("custom_option")) {
                        switch (valueArr.get(0).toString()) {
                            case "new":
                                inapply.append("is_new", true);
                                break;
                            case "Not_Interested":
                                inapply.append("inquiry_status_key", "1584258740752");
                                break;
                            case "pending":
                                Date start = DateParser.getStartTimeOfToday(this.tz);
                                inapply.append("next_followup_date_filter",
                                        new BasicDBObject("$lt", start));
                                inapply.append("inquiry_status_key", new BasicDBObject("$ne", "1584258740752"));
                                break;
                            default:
                                break;
                        }

                    }
            }

        }
        if (size > 0) {
            list.add(inapply);
        }
        if (!list.isEmpty()) {
            apply.append("$and", list);
        }

        // Helper.printConsole(apply);
        //} catch (Exception e) {
        //    
        //}
        //return apply;
    }

    public void applyDateRangeFilter(String filter, BasicDBObject apply) {

        JSONObject outer = (JSONObject) JSONValue.parse(filter);
        Date start = DateParser.returnDateFromZuluString(outer.get("start").toString());
        Date end = DateParser.returnDateFromZuluString(outer.get("end").toString());
        apply.append("appointment_date_date_filter", new BasicDBObject("$gte", start).append("$lte", end));
        // return apply;
    }

    public void applyTodayMeetingFilter(JSONArray valueArr, BasicDBObject apply, String tTrig) {
        Date start = null;
        Date endate = null;
        //Helper.printConsole(valueArr);
        //Helper.printConsole(tTrig);
        switch (tTrig) {
            case "next_week":
                start = DateParser.getStartTimeWithAddDays(this.tz, 1);
                endate = DateParser.getStartTimeWithAddDays(this.tz, 7);
                break;
            case "last_week_action":
                endate = DateParser.getEndTimeWithSubDays(this.tz, 1);
                start = DateParser.getStartTimeWithSubDays(this.tz, 7);
                // Helper.printConsole("last week filter"+date);
                break;
            case "today":
                start = DateParser.getStartTimeOfToday(this.tz);
                endate = DateParser.getEndTimeOfToday(this.tz);
                // Helper.printConsole("last week filter"+date);
                break;
            default:
                this.todaydate = valueArr.get(0).toString();
                start = DateParser.getStartTimeOfByDate(valueArr.get(0).toString(), this.tz);
                if (valueArr.size() > 1) {
                    endate = DateParser.getEndTimeOfByDate(valueArr.get(1).toString(), this.tz);
                }
                else
                {
                    endate = DateParser.getEndTimeOfByDate(valueArr.get(0).toString(), this.tz);
                }

            //    Helper.printConsole("today"+date);
        }

        //Date dateTo = DateParser.returnDateFromZuluString(valueArr.get(1).toString());
        Helper.printConsole(start);
        Helper.printConsole(endate);
        apply.append("date_time_date_filter",
                new BasicDBObject("$gte", start).append("$lte", endate)
        );

    }

    public void applyLastUpdatedMeetingFilter(JSONArray valueArr, BasicDBObject apply) {

        Date start = DateParser.getStartTimeWithSubDays(this.tz, 1);
        Date endate = DateParser.getStartTimeWithSubDays(this.tz, 7);
        System.out.println(start);
        System.out.println(endate);
        apply.append("meeting_date_date_filter",
                new BasicDBObject("$gte", endate).append("$lt", start)
        );
    }

}
