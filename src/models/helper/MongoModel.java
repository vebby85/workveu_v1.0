/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.helper;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import helpers.Helper;
import java.util.List;

import org.bson.Document;
import org.json.simple.JSONObject;

/**
 *
 * @author Varinder Singh
 */
public class MongoModel {
    public String db;
    public String collection;
    public String id;
    
    public MongoClient mc;
//    public MongoModel()
//    {
//        this.mc = DbConnection.returnMongoConnection();
//    }
//    public MongoModel(String conId)
//    {
//        this.mc = DbConnection.getMongoPoolForUser(conId);
//    }
    public long insertDocument(Document json)
    {
        MongoDatabase ldb = this.mc.getDatabase(this.db);
        MongoCollection<Document> col = ldb.getCollection(this.collection);
        json.append("_id", this.id);
        col.insertOne(json);
        return 1;
    }
    public long insertDocumentAutoId(Document json)
    {
        MongoDatabase ldb = this.mc.getDatabase(this.db);
        MongoCollection<Document> col = ldb.getCollection(this.collection);
        col.insertOne(json);
        return 1;
    }
    public long delete()
    {
        MongoDatabase ldb = this.mc.getDatabase(this.db);
        MongoCollection<Document> col = ldb.getCollection(this.collection);
        col.deleteOne(new Document("_id", this.id));
        return 1;
    }
    public long UpdateOrInsertDocument(Document json)
    {
        MongoDatabase ldb = this.mc.getDatabase(this.db);
        Helper.printConsole("My Database "  + this.db);
        MongoCollection<Document> col = ldb.getCollection(this.collection);
        Document lid = new Document("_id", this.id);
        UpdateResult updateOne = col.updateOne(lid, new Document("$set" , json),new UpdateOptions().upsert(true) );
        Helper.printConsole( updateOne.getModifiedCount());
        return updateOne.getModifiedCount();
    }
    public void UpdateOrInsertList(List<Document> list)
    {
        MongoDatabase ldb = this.mc.getDatabase(this.db);
        Helper.printConsole("My Database "  + this.db);
        MongoCollection<Document> col = ldb.getCollection(this.collection);
        col.insertMany(list);
    }
    public long UpdateOrInsertJSONObject(JSONObject json)
    {
        MongoDatabase ldb = this.mc.getDatabase(this.db);
        Helper.printConsole("My Database "  + this.collection);
        MongoCollection<Document> col = ldb.getCollection(this.collection);
        Document lid = new Document("_id", this.id);
        UpdateResult updateOne = col.updateOne(lid, new Document("$set" , json),new UpdateOptions().upsert(true) );
        Helper.printConsole( updateOne.getModifiedCount());
        return updateOne.getModifiedCount();
    }
    public long update(Document json)
    {
        MongoDatabase ldb = this.mc.getDatabase(this.db);
        MongoCollection<Document> col = ldb.getCollection(this.collection);
        Document lid = new Document("_id", this.id);
        UpdateResult updateOne = col.updateOne(lid, new Document("$set" , json),new UpdateOptions().upsert(false) );
        return updateOne.getModifiedCount();
    }
    public long UpdateOrInsertDocumentPush(Document json)
    {
        MongoDatabase ldb = this.mc.getDatabase(this.db);
        MongoCollection<Document> col = ldb.getCollection(this.collection);
        Document lid = new Document("_id", this.id);
        UpdateResult updateOne = col.updateOne(lid, new Document("$push" , json),new UpdateOptions().upsert(true) );
        return updateOne.getModifiedCount();
    }
    public long UpdateOrInsertFindDocumentPush(Document json,BasicDBObject find)
    {
        MongoDatabase ldb = this.mc.getDatabase(this.db);
        MongoCollection<Document> col = ldb.getCollection(this.collection);
        UpdateResult updateOne = col.updateOne(find, new Document("$push" , json),new UpdateOptions().upsert(true) );
        return updateOne.getModifiedCount();
    }
    public MongoCollection<Document> returnCollection(String db,String col)
    {
        return this.mc.getDatabase(db).getCollection(col);
    }
    public void createDatabase(String dbname)
    {
        try{
        MongoDatabase db = this.mc.getDatabase("admin");
        BasicDBList basicDBList = new BasicDBList();
        basicDBList.add("readWrite");
        basicDBList.add(new Document("role", "readWrite").append("db", dbname));
       
          db.runCommand(new Document("grantRolesToUser", "viru")
            .append("roles", basicDBList));
          
        MongoDatabase  db1 =  this.mc.getDatabase(dbname);
            Helper.printConsole(db1.getReadConcern().asDocument());
        db1.getCollection("hellotest").insertOne(new Document("say","hello")); 
        }catch(Exception e)
        {
            
        }
    }
    public String getNewPrimaryId(String inital)
    {
        MongoDatabase ldb = this.mc.getDatabase(this.db);
        MongoCollection<Document> col = ldb.getCollection(this.collection);
        String id = String.format("%06d" ,col.countDocuments() + 1);
        return inital  + id;
    }
    public String getNewPrimaryIdForArray(String inital,long count)
    {
        MongoDatabase ldb = this.mc.getDatabase(this.db);
        MongoCollection<Document> col = ldb.getCollection(this.collection);
        String id = String.format("%06d" ,(col.countDocuments() + count));
        return inital  + id;
    }
    public String getOldPrimaryId(String inital)
    {
        MongoDatabase ldb = this.mc.getDatabase(this.db);
        MongoCollection<Document> col = ldb.getCollection(this.collection);
        Document first = col.find(new BasicDBObject("_id",this.id)).projection(new BasicDBObject("createon_ms",1)).first();
        String i = null != first ? String.valueOf(first.getLong("createon_ms")) : String.valueOf(System.currentTimeMillis());
        String id = String.format("%06d" ,col.countDocuments(new BasicDBObject("createon_ms",
                new Document("$lt", i))) + 1);
        return inital  + id;
    }
    
}
