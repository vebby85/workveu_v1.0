/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.helper;

import helpers.Helper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author viru
 */
public class Inventory {
    public Connection con;
    public Inventory()
    {
       
    }
    public boolean reverseStock(int sold,String product)
    {
        try {
            PreparedStatement ps = this.con.prepareStatement("UPDATE app_inventory_products "
                    + "SET app_inventory_products.quantity = (app_inventory_products.quantity+?)"
                    + " WHERE product_id=?");
            ps.setInt(1, sold);
            ps.setString(2, product);
            int exe = ps.executeUpdate();
            Helper.printConsole(ps.toString());
            ps.close();
            if(exe > 0)
            {
                return true;
            }
            return false;
        } catch (SQLException ex) {ex.printStackTrace();}
        return false;
    }
    public boolean updateStock(int sold,String product)
    {
        try {
            PreparedStatement ps = this.con.prepareStatement("UPDATE app_inventory_products "
                    + "SET app_inventory_products.quantity = (app_inventory_products.quantity-?)"
                    + " WHERE product_id=?");
            ps.setInt(1, sold);
            ps.setString(2, product);
            int exe = ps.executeUpdate();
            Helper.printConsole(ps.toString());
            ps.close();
            if(exe > 0)
            {
                return true;
            }
            return false;
        } catch (SQLException ex) {}
        return false;
    }
    public boolean addToProductSale(String product)
    {
        try {
            PreparedStatement ps = this.con.prepareStatement("INSERT ");
            ps.setString(2, product);
            int exe = ps.executeUpdate();
            Helper.printConsole(ps.toString());
            ps.close();
            if(exe > 0)
            {
                return true;
            }
            return false;
        } catch (SQLException ex) {}
        return false;
    }
    public boolean inStock(int sold,String product)
    {
        boolean instock = false;
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT quantity FROM app_inventory_products "
                    + " WHERE product_id=?");
            ps.setString(1, product);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                if(rs.getInt("quantity") >= sold)
                {
                    instock = true;
                }
            }
            ps.close();
            return instock;
        } catch (SQLException ex) {}
        return instock;
    }
}
