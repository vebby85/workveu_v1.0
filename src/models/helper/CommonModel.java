/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.helper;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import helpers.DateParser;
import helpers.Helper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.bson.Document;
import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import redis.clients.jedis.Jedis;

/**
 *
 * @author viru
 */
public class CommonModel {

    public Connection con;
    public MongoClient mongo;
    public Jedis jedis;
    public MongoCollection<Document> collection;
    public String tz="";

    public JSONArray maplist = new JSONArray();

    public String getUserNameByUID(String uid, String orgid) {
        String name = "";
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT name FROM org_portal_users WHERE uid=? AND org_id=?");
            ps.setString(1, uid);
            ps.setString(2, orgid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                name = rs.getString("name");
            }
            rs.close();
            ps.close();
            // this.con.close();
        } catch (SQLException ex) {
        }
        return name;
    }

    public List<String> getUserListByORG(String orgid) {
        List<String> name = new ArrayList<>();
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT uid FROM org_portal_users WHERE org_id=?");
            ps.setString(1, orgid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                name.add(rs.getString("uid"));
            }
            ps.close();
        } catch (SQLException ex) {
        }
        return name;
    }

    public Map<String, String> getUserMapByORG(String orgid) {
        Map<String, String> name = new HashMap<>();
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT uid,name FROM org_portal_users WHERE org_id=?");
            ps.setString(1, orgid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                name.put(rs.getString("uid"), rs.getString("name"));
            }
            ps.close();
        } catch (SQLException ex) {
        }
        return name;
    }

    public String getUserEmpCodeByUID(String uid, String orgid) {
        String name = "";
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT emp_code FROM org_portal_users WHERE uid=? AND org_id=?");
            ps.setString(1, uid);
            ps.setString(2, orgid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                name = rs.getString("emp_code");
            }
            ps.close();
            // this.con.close();
        } catch (SQLException ex) {
        }
        return name;
    }

    public int getUserTarget(String uid) {
        int name = 0;
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT target FROM user_targets  WHERE uid=? "
                    + "AND user_targets.isActive='1'");
            ps.setString(1, uid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                name = rs.getInt("target");
            }
            ps.close();
            // this.con.close();
        } catch (SQLException ex) {
        }
        return name;
    }

    public Map<String, String> getUserTargetMap(String uid) {
        Map<String, String> target = new HashMap<>();
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT user_targets.uid,user_targets.target "
                    + "FROM user_targets "
                    + "JOIN org_portal_users "
                    + "ON org_portal_users.uid = user_targets.uid "
                    + "WHERE org_portal_users.org_id=? "
                    + "AND user_targets.isActive='1'");
            ps.setString(1, uid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                target.put(rs.getString("uid"), rs.getString("target"));
            }
            ps.close();
            // this.con.close();
        } catch (SQLException ex) {
        }
        return target;
    }

    public String getTerritoryNameByID(String id) {
        String name = "";
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT territory_name FROM master_territory WHERE id=?");
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                name = rs.getString("territory_name");
            }
            ps.close();
            // this.con.close();
        } catch (SQLException ex) {
        }
        return name;
    }

    public String getDealStatusByID(String id) {
        String name = "";
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT deal_status FROM master_deal_status WHERE deal_status_key=?");
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                name = rs.getString("deal_status");
            }
            ps.close();
            // this.con.close();
        } catch (SQLException ex) {
        }
        return name;
    }

    public boolean isFormFieldExist(String app_code, String module_id, String fid) {
        MongoCollection<Document> collection = this.mongo.getDatabase("module_forms")
                .getCollection(app_code);
        long countDocuments = collection.countDocuments(new BasicDBObject("_id", module_id)
                .append("fields.id", fid));
        return countDocuments > 0;
    }

    public JSONObject getFormFieldsByType(String module_id, String type) {

        AggregateIterable<Document> aggregate = this.collection.aggregate(Arrays.asList(
                new BasicDBObject("$unwind", "$fields"),
                new BasicDBObject("$match", new BasicDBObject(
                        "_id", module_id
                ).append("fields.type", type)
                        .append("fields.id", new BasicDBObject("$nin", Arrays.asList("city", "country", "state")))),
                new BasicDBObject("$project", new BasicDBObject("fields.autocompleteTrigger", 1)
                        .append("fields.id", 1)
                        .append("fields.autocompleteField", 1)
                        .append("_id", 0))
        ));
        JSONObject j = new JSONObject();
        for (Document d : aggregate) {

            if (null != d.get("fields")) {
                Document ind = (Document) d.get("fields");
                Document autocomp = (Document) ind.get("autocompleteTrigger");
                autocomp.put("autocompleteField", ind.get("autocompleteField"));
                j.put(ind.getString("id"), autocomp);
            }
        }
        aggregate.iterator().close();
        return j;

    }

    public String getFormFieldType(String app_code, String module_id, String fid) {

        MongoCollection<Document> collection = mongo.getDatabase("module_forms")
                .getCollection(app_code);

        Document first = collection.aggregate(Arrays.asList(
                new BasicDBObject("$unwind", "$fields"),
                new BasicDBObject("$match", new BasicDBObject(
                        "_id", module_id
                ).append("fields.id", fid)),
                new BasicDBObject("$project", new BasicDBObject("fields.type", 1)
                        .append("autocompleteTrigger", 1)
                        .append("_id", 0))
        )).first();
        String type = "";

        if (null != first && null != first.get("fields")) {
            Document d = (Document) first.get("fields");
            type = d.getString("type");
        }
        return type;
    }

    public Document getFormFieldAttributes(String app_code, String module_id) {

        MongoCollection<Document> collection = mongo.getDatabase("module_forms")
                .getCollection(app_code);

        Document first = collection.aggregate(Arrays.asList(
                new BasicDBObject("$unwind", "$fields"),
                new BasicDBObject("$match", new BasicDBObject(
                        "_id", module_id
                )),
                new BasicDBObject("$project", new BasicDBObject("fields.type", 1)
                        .append("autocompleteTrigger", 1)
                        .append("options", 1)
                        .append("_id", 0))
        )).first();

        return null != first && null != first.get("fields") ? (Document) first.get("fields") : new Document();
    }

    public String getFormFieldLabel(String app_code, String module_id, String fid) {

        MongoCollection<Document> collection = mongo.getDatabase("module_forms")
                .getCollection(app_code);

        Document first = collection.aggregate(Arrays.asList(
                new BasicDBObject("$unwind", "$fields"),
                new BasicDBObject("$match", new BasicDBObject(
                        "_id", module_id
                ).append("fields.id", fid)),
                new BasicDBObject("$project", new BasicDBObject("fields.label", 1).append("_id", 0))
        )).first();
        String type = "";

        if (null != first && null != first.get("fields")) {
            Document d = (Document) first.get("fields");
            type = d.getString("label");
        }
        return type;
    }

    public List<Object> getFormFieldOptions(String app_code, String module_id, String fid) {

        MongoCollection<Document> collection = mongo.getDatabase("module_forms")
                .getCollection(app_code);
        List<Object> A = null;
        Document first = collection.aggregate(Arrays.asList(
                new BasicDBObject("$unwind", "$fields"),
                new BasicDBObject("$match", new BasicDBObject(
                        "_id", module_id
                ).append("fields.id", fid)),
                new BasicDBObject("$project", new BasicDBObject("fields.options", 1).append("_id", 0))
        )).first();

        if (null != first && null != first.get("fields")) {
            Document d = (Document) first.get("fields");
            A = (ArrayList) d.get("options");
        }
        return A;
    }

    public AggregateIterable<Document> getFormFieldAndLabels(String app_code, String module_id) {
        Helper.printConsole(mongo);
        MongoCollection<Document> collection = mongo.getDatabase("module_forms")
                .getCollection(app_code);

        AggregateIterable<Document> aggregate = collection.aggregate(Arrays.asList(
                new BasicDBObject("$unwind", "$fields"),
                new BasicDBObject("$match", new BasicDBObject(
                        "_id", module_id
                )),
                new BasicDBObject("$project", new BasicDBObject("fields.id", 1)
                        .append("fields.label", 1)
                        .append("fields.type", 1)
                        .append("_id", 0))
        ));
        aggregate.iterator().close();
        return aggregate;
    }

    public AggregateIterable<Document> getFormFieldIDs(String app_code, String module_id) {
        Helper.printConsole(mongo);
        MongoCollection<Document> collection = mongo.getDatabase("module_forms")
                .getCollection(app_code);

        AggregateIterable<Document> aggregate = collection.aggregate(Arrays.asList(
                new BasicDBObject("$unwind", "$fields"),
                new BasicDBObject("$match", new BasicDBObject(
                        "_id", module_id
                )),
                new BasicDBObject("$project", new BasicDBObject("fields.id", 1)
                        .append("_id", 0))
        ));

        return aggregate;
    }

    public boolean isUniqueInDB(String app_code, String module_id, String fid) {

        MongoCollection<Document> collection = mongo.getDatabase("module_forms")
                .getCollection(app_code);

        Document first = collection.aggregate(Arrays.asList(
                new BasicDBObject("$unwind", "$fields"),
                new BasicDBObject("$match", new BasicDBObject(
                        "_id", module_id
                ).append("fields.id", fid)),
                new BasicDBObject("$project", new BasicDBObject("fields.uniqueInDb", 1).append("_id", 0))
        )).first();
        boolean type = false;
        if (null != first && null != first.get("fields")) {
            Document d = (Document) first.get("fields");
            type = null == d.get("uniqueInDb") ? false : d.getBoolean("uniqueInDb");
        }
        return type;
    }

    public boolean isRecordExistInMongo(String app_code, String module_id, String fid, String fvalue) {

        MongoCollection<Document> collection = mongo.getDatabase(app_code)
                .getCollection(module_id);
        long countDocuments = collection.countDocuments(new BasicDBObject(fid, fvalue));
        boolean type = false;
        if (countDocuments > 0) {
            type = true;
        }
        return type;
    }

    public String getFormUserRoleSelected(String app_code, String module_id, String fieldid) {

        MongoCollection<Document> collection = mongo.getDatabase("module_forms")
                .getCollection(app_code);

        Document first = collection.aggregate(Arrays.asList(
                new BasicDBObject("$unwind", "$fields"),
                new BasicDBObject("$match", new BasicDBObject(
                        "_id", module_id
                ).append("fields.id", fieldid)),
                new BasicDBObject("$project", new BasicDBObject("fields.autocompleteUserRole", 1).append("_id", 0))
        )).first();

        String type = "";
        if (null != first && null != first.get("fields")) {
            Document d = (Document) first.get("fields");
            type = d.getString("autocompleteUserRole");
        }
        return type;
    }

    public String getFieldValueWithField(String app_code, String module_id, String record_id, String fieldid) {
        MongoCollection<Document> collectionl = mongo.getDatabase(app_code)
                .getCollection(module_id);
        Document first = collectionl.find(new BasicDBObject("_id", record_id))
                .projection(new BasicDBObject(fieldid, 1)).first();
        return null == first ? "" : first.getString(fieldid);
    }

    public long getCommentCount(String app_code, String rid) {
        MongoCollection<Document> col = this.mongo.getDatabase(app_code)
                .getCollection("comments");
        long countDocuments = col.countDocuments(new BasicDBObject("record_id", rid));
        return countDocuments;
    }

    public String getFormIdBySlug(String org_id, String id) {
        String name = "";
        try {

            PreparedStatement ps = this.con.prepareStatement("SELECT module_id FROM app_modules WHERE org_id=? AND module_slug=?");
            ps.setString(1, org_id);
            ps.setString(2, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                name = rs.getString("module_id");
            }
            ps.close();
            // this.con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return name;
    }

    public String getFormSlugById(String org_id, String id) {
        String name = "";
        try {

            PreparedStatement ps = this.con.prepareStatement("SELECT module_slug FROM app_modules WHERE org_id=? AND module_id=?");
            ps.setString(1, org_id);
            ps.setString(2, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                name = rs.getString("module_slug");
            }
            ps.close();
            // this.con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return name;
    }

    public String getFormNameById(String org_id, String id) {
        String name = "";
        try {

            PreparedStatement ps = this.con.prepareStatement("SELECT module_name FROM app_modules WHERE org_id=? AND module_id=?");
            ps.setString(1, org_id);
            ps.setString(2, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                name = rs.getString("module_name");
            }
            ps.close();
            // this.con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return name;
    }

    public JSONObject getMyTeamDealManager(String uid, String app_code, String uname, String ro) {

        JSONArray jarr = new JSONArray();
        JSONArray cols = new JSONArray();
        JSONArray d = new JSONArray();
        JSONArray d2 = new JSONArray();
        JSONArray d1 = new JSONArray();
        this.jedis.select(2);
        String team = this.jedis.hget("team:" + app_code, uid);
        String conFormId = this.getFormIdBySlug(app_code, "deals");
        JSONArray myTeam = new JSONArray();
        if (team != null && !team.isEmpty()) {
            myTeam.addAll((JSONArray) JSONValue.parse(team));
        }

        JSONObject jj = new JSONObject();
        jj.put("uid", uid);
        jj.put("name", uname);
        jj.put("reports_to", ro);
        myTeam.add(0, jj);
        MongoCollection<Document> col = this.mongo.getDatabase(app_code)
                .getCollection(conFormId);
        Map<String, String> lstatus = this.getLeadStatusList(app_code);

        for (Object t : myTeam) {
            JSONObject inteam = (JSONObject) t;
            BasicDBObject fact = new BasicDBObject();
            BasicDBObject pro = new BasicDBObject();
            lstatus.forEach((k, v) -> {
                if (!cols.contains(v)) {
                    cols.add(v);
                }
                String key = k.equals("won") ? "deal_convert_by" : "manager_user_key";
                fact.append(v, Arrays.asList(
                        new BasicDBObject("$match", new BasicDBObject("deal_status_key", k).append(key, inteam.get("uid"))),
                        new BasicDBObject("$count", v)
                ));

                pro.append(v, new BasicDBObject("$arrayElemAt", Arrays.asList("$" + v + "." + v, 0)));
            });

            AggregateIterable<Document> aggregate = col.aggregate(Arrays.asList(
                    new BasicDBObject("$facet", fact),
                    new BasicDBObject("$project", pro)
            ));
            for (Document a : aggregate) {
                JSONArray p = new JSONArray();
                JSONArray pg = new JSONArray();
                pg.add(inteam.get("uid"));
                p.add(inteam.get("name"));
                p.add(inteam.containsKey("reports_to_text") ? inteam.get("reports_to_text") : "");
                lstatus.forEach((k, v) -> {
                    if (!a.containsKey(v)) {
                        p.add(0);
                        pg.add(k);
                    } else {
                        p.add(a.get(v));
                        pg.add(k);
                    }
                });
                d.add(p);
                d2.add(pg);

            }
            aggregate.iterator().close();
            d1.add(inteam.get("reports_to"));
        }
        // this.con.close();
        cols.add(0, "Name");
        cols.add(1, "Reports To");
        JSONObject cols1 = new JSONObject();
        cols1.put("columns", cols);
        cols1.put("rows", d);
        cols1.put("rowkeys", d2);
        cols1.put("uids", d1);
        jarr.add(cols);
        return cols1;

    }

    public JSONArray getMyTeamDealManagerAsTree(String uid, String app_code, String uname, String ro) {

        JSONArray jarr = new JSONArray();
        this.jedis.select(2);
        String team = this.jedis.hget("team:" + app_code, uid);
        String conFormId = this.getFormIdBySlug(app_code, "deals");
        JSONArray myTeam = new JSONArray();
        if (team != null && !team.isEmpty()) {
            myTeam.addAll((JSONArray) JSONValue.parse(team));
        }

        JSONObject jj = new JSONObject();

        jj.put("uid", uid);
        jj.put("name", uname);
        jj.put("reports_to", "0");
        jj.put("reports_to_text", this.getUserNameByUID(ro, app_code));
        jj.put("territory", String.join(",", this.getUserTerriroryList(uid, app_code)));
        jj.put("emp_code", this.getUserEmpCodeByUID(uid, app_code));

        myTeam.add(0, jj);
        MongoCollection<Document> col = this.mongo.getDatabase(app_code)
                .getCollection(conFormId);
        Map<String, String> lstatus = this.getLeadStatusList(app_code);
        Map<String, String> pp = this.getUserMapProfilePic(app_code);

        for (Object t : myTeam) {
            JSONObject inteam = (JSONObject) t;
            BasicDBObject fact = new BasicDBObject();
            BasicDBObject pro = new BasicDBObject();

            lstatus.forEach((k, v) -> {
                String key = k.equals("won") ? "deal_convert_by" : "manager_user_key";
                fact.append(v, Arrays.asList(
                        new BasicDBObject("$match", new BasicDBObject("deal_status_key", k).append(key, inteam.get("uid"))),
                        new BasicDBObject("$count", v)
                ));

                pro.append(v, new BasicDBObject("$arrayElemAt", Arrays.asList("$" + v + "." + v, 0)));
            });

            AggregateIterable<Document> aggregate = col.aggregate(Arrays.asList(
                    new BasicDBObject("$facet", fact),
                    new BasicDBObject("$project", pro)
            ));
            for (Document a : aggregate) {
                JSONObject p = new JSONObject();
                p.put("uid", inteam.get("uid"));
                p.put("name", inteam.get("name"));
                p.put("pid", inteam.get("reports_to"));
                p.put("reports_to_text", inteam.get("reports_to_text"));
                p.put("territory", inteam.get("territory"));
                p.put("emp_code", inteam.get("emp_code"));
                p.put("pp", pp.get(inteam.get("uid").toString()));
                JSONArray indata = new JSONArray();
                lstatus.forEach((k, v) -> {
                    JSONObject pin = new JSONObject();
                    pin.put("label", v);
                    pin.put("key", k);
                    if (!a.containsKey(v)) {
                        pin.put("value", 0);
                    } else {
                        pin.put("value", a.get(v));
                    }
                    indata.add(pin);
                });
                p.put("meta", indata);
                jarr.add(p);
            }
            aggregate.iterator().close();

        }
        return jarr;

    }

    public JSONObject getMyTeamActionManager(String uid, String app_code, String uname, String ro) {
        try {
            JSONArray jarr = new JSONArray();

            JSONArray d = new JSONArray();
            JSONArray lstatus = new JSONArray();
            JSONArray d2 = new JSONArray();

            this.jedis.select(2);
            String team = this.jedis.hget("team:" + app_code, uid);
            String conFormId = this.getFormIdBySlug(app_code, "next-action");
            JSONArray myTeam = new JSONArray();
            if (team != null && !team.isEmpty()) {
                myTeam.addAll((JSONArray) JSONValue.parse(team));
            }
            JSONObject jj = new JSONObject();
            jj.put("uid", uid);
            jj.put("name", uname);
            jj.put("reports_to", ro);

            myTeam.add(0, jj);
            MongoCollection<Document> col = this.mongo.getDatabase(app_code)
                    .getCollection(conFormId);
            for (Object t : myTeam) {
                JSONObject inteam = (JSONObject) t;
                BasicDBObject fact = new BasicDBObject();
                BasicDBObject pro = new BasicDBObject();
                Date start = DateTime.now().withTimeAtStartOfDay().toDate();
                Date end = DateTime.now().withTimeAtStartOfDay().plusHours(23).plusMinutes(59).toDate();
                BasicDBObject cl1 = new BasicDBObject("manager_user_key", inteam.get("uid"));
                BasicDBObject cl2 = new BasicDBObject("assigned_to_key", inteam.get("uid"));
                BasicDBList dl = new BasicDBList();
                dl.add(cl1);
                dl.add(cl2);

                fact.append("reminder", Arrays.asList(
                        new BasicDBObject("$match", new BasicDBObject("date_time_date_filter", new BasicDBObject("$lte", start))
                                //.append("manager_user_key",inteam.get("uid"))
                                .append("$or", dl)
                                .append("isActive", true)),
                        new BasicDBObject("$count", "reminder")
                ));
                pro.append("reminder", new BasicDBObject("$arrayElemAt", Arrays.asList("$reminder.reminder", 0)));

                fact.append("todays", Arrays.asList(
                        new BasicDBObject("$match", new BasicDBObject("date_time_date_filter", new BasicDBObject("$gte", start).append("$lte", end))
                                //.append("manager_user_key", inteam.get("uid"))
                                .append("$or", dl)
                                .append("isActive", true)),
                        new BasicDBObject("$count", "todays")
                ));
                pro.append("todays", new BasicDBObject("$arrayElemAt", Arrays.asList("$todays.todays", 0)));
                fact.append("schduled", Arrays.asList(
                        new BasicDBObject("$match", new BasicDBObject("date_time_date_filter", new BasicDBObject("$gte", end))
                                //.append("manager_user_key",inteam.get("uid"))
                                .append("$or", dl)
                                .append("isActive", true)),
                        new BasicDBObject("$count", "schduled")
                ));
                pro.append("schduled", new BasicDBObject("$arrayElemAt", Arrays.asList("$schduled.schduled", 0)));

                AggregateIterable<Document> aggregate = col.aggregate(Arrays.asList(
                        new BasicDBObject("$facet", fact),
                        new BasicDBObject("$project", pro)
                ));
                for (Document a : aggregate) {
                    JSONArray p = new JSONArray();
                    JSONArray pg = new JSONArray();
                    p.add(inteam.get("name"));
                    pg.add(inteam.get("uid"));
                    p.add(inteam.containsKey("reports_to_text") ? inteam.get("reports_to_text") : "");
                    if (!a.containsKey("reminder")) {
                        p.add(0);
                    } else {
                        p.add(a.get("reminder"));
                    }
                    if (!a.containsKey("todays")) {
                        p.add(0);
                    } else {
                        p.add(a.get("todays"));
                    }
                    if (!a.containsKey("schduled")) {
                        p.add(0);
                    } else {
                        p.add(a.get("schduled"));
                    }

                    d.add(p);
                    d2.add(pg);

                }
                aggregate.iterator().close();
            }

            lstatus.add("Name");
            lstatus.add("Reports To");
            lstatus.add("Missed");
            lstatus.add("Today's Action");
            lstatus.add("Scheduled");
            JSONObject cols = new JSONObject();
            cols.put("columns", lstatus);
            cols.put("rows", d);
            cols.put("rowskeys", d2);
            jarr.add(cols);
            Helper.printConsole(jarr);
            return cols;
        } catch (Exception e) {

        }
        return null;
    }

    public JSONArray getMyTeamActionManagerAsTree(String uid, String app_code, String uname, String ro) {
        JSONArray jarr = new JSONArray();
        this.jedis.select(2);
        String team = this.jedis.hget("team:" + app_code, uid);
        String conFormId = this.getFormIdBySlug(app_code, "next-action");
        JSONArray myTeam = new JSONArray();
        if (team != null && !team.isEmpty()) {
            myTeam.addAll((JSONArray) JSONValue.parse(team));
        }
        //Helper.printConsole(myTeam);
        JSONObject jj = new JSONObject();

        jj.put("uid", uid);
        jj.put("name", uname);
        jj.put("reports_to", "0");
        jj.put("reports_to_text", this.getUserNameByUID(ro, app_code));
        jj.put("territory", String.join(",", this.getUserTerriroryList(uid, app_code)));
        jj.put("emp_code", this.getUserEmpCodeByUID(uid, app_code));
        myTeam.add(0, jj);
        Map<String, String> pp = this.getUserMapProfilePic(app_code);
        MongoCollection<Document> col = this.mongo.getDatabase(app_code)
                .getCollection(conFormId);
        for (Object t : myTeam) {
            JSONObject inteam = (JSONObject) t;
            BasicDBObject fact = new BasicDBObject();
            BasicDBObject pro = new BasicDBObject();
            Date start = DateParser.getStartTimeOfToday(this.tz);
            Date end = DateParser.getEndTimeOfToday(this.tz);
            BasicDBObject cl1 = new BasicDBObject("manager_user_key", inteam.get("uid"));
            BasicDBObject cl2 = new BasicDBObject("assigned_to_key", inteam.get("uid"));
            BasicDBList d = new BasicDBList();
            d.add(cl1);
            d.add(cl2);
            fact.append("missed", Arrays.asList(
                    new BasicDBObject("$match", new BasicDBObject("date_time_date_filter", new BasicDBObject("$lte", start))
                            .append("$or", d)
                            // .append("manager_user_key",uid)
                            .append("isActive", true)),
                    new BasicDBObject("$count", "missed")
            ));
            pro.append("missed", new BasicDBObject("$arrayElemAt", Arrays.asList("$missed.missed", 0)));

            fact.append("todays", Arrays.asList(
                    new BasicDBObject("$match", new BasicDBObject("date_time_date_filter", new BasicDBObject("$gte", start).append("$lte", end))
                            .append("$or", d)
                            //.append("manager_user_key",uid)
                            .append("isActive", true)),
                    new BasicDBObject("$count", "todays")
            ));
            pro.append("todays", new BasicDBObject("$arrayElemAt", Arrays.asList("$todays.todays", 0)));
            fact.append("schduled", Arrays.asList(
                    new BasicDBObject("$match", new BasicDBObject("date_time_date_filter", new BasicDBObject("$gte", end))
                            .append("$or", d)
                            //.append("manager_user_key",uid)
                            .append("isActive", true) ),
                    new BasicDBObject("$count", "schduled")
            ));
            pro.append("schduled", new BasicDBObject("$arrayElemAt", Arrays.asList("$schduled.schduled", 0)));

            AggregateIterable<Document> aggregate = col.aggregate(Arrays.asList(
                    new BasicDBObject("$facet", fact),
                    new BasicDBObject("$project", pro)
            ));
            for (Document a : aggregate) {
                JSONObject p = new JSONObject();
                p.put("uid", inteam.get("uid"));
                p.put("name", inteam.get("name"));
                p.put("pid", inteam.get("reports_to"));
                p.put("territory", inteam.get("territory"));
                p.put("emp_code", inteam.get("emp_code"));
                p.put("pp", pp.get(inteam.get("uid").toString()));
                JSONObject pin = new JSONObject();
                JSONArray meta = new JSONArray();
                pin.put("label", "Missed");
                pin.put("key", "missed");
                if (!a.containsKey("missed")) {
                    pin.put("value", 0);
                } else {
                    pin.put("value", a.get("missed"));
                }
                meta.add(pin);
                JSONObject pin1 = new JSONObject();
                pin1.put("label", "Today's Action");
                pin1.put("key", "todays");
                if (!a.containsKey("todays")) {
                    pin1.put("value", 0);
                } else {
                    pin1.put("value", a.get("todays"));
                }
                meta.add(pin1);
                JSONObject pin2 = new JSONObject();
                pin2.put("label", "Scheduled");
                pin2.put("key", "schduled");
                if (!a.containsKey("schduled")) {
                    pin2.put("value", 0);
                } else {
                    pin2.put("value", a.get("schduled"));
                }
                meta.add(pin2);
                p.put("meta", meta);
                jarr.add(p);

            }
            aggregate.iterator().close();
        }
        return jarr;
    }

    public List<String> getMyTeamAsList(String uid, String app_code) {
        List<String> jarr = new ArrayList<>();
        this.jedis.select(2);
        String team = this.jedis.hget("team:" + app_code, uid);
        if (team != null && !team.isEmpty()) {
            JSONArray myTeam = (JSONArray) JSONValue.parse(team);
            for (Object a : myTeam) {
                JSONObject j = (JSONObject) a;
                jarr.add(j.get("uid").toString());
            }
        }
        jarr.add(uid);
        return jarr;
    }

    public JSONArray getMyTeamAsResourceMap(String uid, String app_code) {
        JSONArray jarr = new JSONArray();
        this.jedis.select(2);
        String team = this.jedis.hget("team:" + app_code, uid);
        if (team != null && !team.isEmpty()) {
            JSONArray myTeam = (JSONArray) JSONValue.parse(team);
            for (Object a : myTeam) {
                JSONObject j = (JSONObject) a;
                JSONObject jin = new JSONObject();
                jin.put("id", j.get("uid").toString());
                jin.put("name", j.get("name").toString());
                jarr.add(jin);
            }
        }
        JSONObject jin = new JSONObject();
        jin.put("id", uid);
        jin.put("name", "Me");
        jarr.add(jin);
        return jarr;
    }

    public long getMyTeamCount(String uid, String app_code) {
        this.jedis.select(2);
        String team = this.jedis.hget("team:" + app_code, uid);
        if (team != null && !team.isEmpty()) {
            JSONArray myTeam = (JSONArray) JSONValue.parse(team);
            return myTeam.size();
        }

        return 0;
    }

    public Map<String, String> getLeadStatusList(String app_code) {
        Map<String, String> l = new LinkedHashMap<>();
        try {

            PreparedStatement ps = this.con.prepareStatement("SELECT deal_status,deal_status_key "
                    + "FROM master_deal_status WHERE org_id=? AND isActive='1' ORDER BY orderBy ASC");
            ps.setString(1, app_code);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                l.put(rs.getString("deal_status_key"), rs.getString("deal_status"));
            }
            ps.close();
            // this.con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return l;
    }

    public List<String> getMyDirectJunior(String uid, String app_code) {
        List<String> jarr = new ArrayList<>();
        jarr.add(uid);
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT uid FROM org_portal_users WHERE reports_to=? AND org_id=?");
            ps.setString(1, uid);
            ps.setString(2, app_code);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                jarr.add(rs.getString("uid"));
            }
            rs.close();
            ps.close();

        } catch (SQLException ex) {
        }
        return jarr;
    }

    public Map<String, String> getMyDirectJuniorMap(String uid, String app_code) {
        Map<String, String> map = new HashMap<>();
        map.put(uid, "Me");
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT uid,name FROM org_portal_users WHERE reports_to=? AND org_id=?");
            ps.setString(1, uid);
            ps.setString(2, app_code);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                map.put(rs.getString("uid"), rs.getString("name"));
            }
            rs.close();
            ps.close();

        } catch (SQLException ex) {
        }
        return map;
    }

    public Document getAccountDetails(String app_code, String accFormId, String rid) {
        Document first = this.mongo.getDatabase(app_code)
                .getCollection(accFormId)
                .find(new Document("_id", rid))
                .projection(new BasicDBObject("manager_user_key", 1).append("territory_master_key", 1))
                .first();
        return first;

    }

    public double getProductTotalByDeal(String id, String app_code, String col) {
        Document first = this.mongo.getDatabase(app_code)
                .getCollection(col)
                .aggregate(Arrays.asList(
                        new BasicDBObject("$match", new BasicDBObject("_id", id)),
                        new BasicDBObject("$project", new BasicDBObject("total", new BasicDBObject("$sum", "$product.total")))
                )).first();
        double total = 0;
        if (first != null) {
            if (first.get("total") instanceof Integer) {
                total = (int) first.get("total");
            } else if (first.get("total") instanceof Double) {
                total = (double) first.get("total");
            }
        }

        return total;
    }

    public String getUserProfilePic(String uid) {
        String n = "";
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT uploaded_name FROM user_profile_image WHERE uid=?");
            ps.setString(1, uid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                n = rs.getString("uploaded_name");
            }
            rs.close();
            ps.close();

        } catch (SQLException ex) {
        }
        return n;
    }

    public Map<String, String> getUserMapProfilePic(String orgid) {
        Map<String, String> pp = new HashMap<>();
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT org_portal_users.uid,uploaded_name FROM user_profile_image "
                    + "JOIN org_portal_users"
                    + " ON org_portal_users.uid = user_profile_image.uid "
                    + "WHERE org_portal_users.org_id=?");
            ps.setString(1, orgid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                pp.put(rs.getString("uid"), rs.getString("uploaded_name"));
            }
            rs.close();
            ps.close();

        } catch (SQLException ex) {
        }
        return pp;
    }

    public List<String> getUserTerriroryList(String uid, String orgid) {
        String n = "";
        List<String> l = new LinkedList<>();
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT assign_branch FROM org_portal_users WHERE uid=? AND org_id=?");
            ps.setString(1, uid);
            ps.setString(2, orgid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                n = rs.getString("assign_branch");
            }
            rs.close();
            ps.clearParameters();
            if (!n.isEmpty()) {
                String[] split = n.split(",");
                n = "";
                for (String split1 : split) {
                    n += "'" + split1 + "',";

                }
                n = n.substring(0, n.length() - 1);
                ps = this.con.prepareStatement("SELECT territory_name FROM master_territory WHERE id IN(" + n + ") AND org_id=?");
                ps.setString(1, orgid);
                ResultSet rs1 = ps.executeQuery();
                while (rs1.next()) {
                    l.add(rs1.getString("territory_name"));
                }
                rs1.close();
                ps.close();
            }
        } catch (SQLException ex) {
        }
        return l;
    }

    public String getPrevRecordId(String db, String col, String id, BasicDBObject q, Object gt) {
        q.append("last_modify_ms", new BasicDBObject("$gt", (long) gt));
        FindIterable<Document> find = this.mongo.getDatabase(db)
                .getCollection(col)
                .find(q)
                .projection(new Document("_id", 1))
                .sort(new BasicDBObject("last_modify_ms", 1))
                .limit(1);
        return null != find && null != find.first() ? find.first().getString("_id") : "";

    }

    public String getNextRecordId(String db, String col, String id, BasicDBObject q, Object gt) {
        q.append("last_modify_ms", new BasicDBObject("$lt", (long) gt));
        FindIterable<Document> find = this.mongo.getDatabase(db)
                .getCollection(col)
                .find(q)
                .projection(new Document("_id", 1))
                .sort(new BasicDBObject("last_modify_ms", -1))
                .limit(1);
        return null != find && null != find.first() ? find.first().getString("_id") : "";

    }

    public Map<String, String> getTerritoriesRecurcive(String territories) {
        String[] s = territories.split(",");
        Map<String, String> l = new HashMap<>();
        if (s.length > 0) {
            String in = "";

            for (String t : s) {
                if (!t.isEmpty()) {
                    in += "'" + t + "',";
                }
            }
            in = in.substring(0, (in.length() - 1));

            if (!in.isEmpty()) {
                try {
                    PreparedStatement ps = this.con.prepareStatement("SELECT id,territory_name as name FROM master_territory \n"
                            + "  WHERE id IN(" + in + ") OR parent_id IN(" + in + ") \n"
                            + "  UNION \n"
                            + "    SELECT id,territory_name as name \n"
                            + "  FROM master_territory \n"
                            + "  WHERE parent_id IN(SELECT id FROM master_territory WHERE parent_id IN(" + in + "))");
                    ResultSet rsin = ps.executeQuery();
                    while (rsin.next()) {
                        l.put(rsin.getString("id"), rsin.getString("name"));
                    }
                    ps.close();
                } catch (SQLException e) {

                }
            }
        }
        return l;
    }

    /**
     * @return Document String of form field's value by Form id from Mongo DB
     */
    public String getFieldValueById(String db, String col, String rid, String fid) {
        Document first = this.mongo.getDatabase(db)
                .getCollection(col)
                .find(new BasicDBObject("_id", rid))
                .projection(new BasicDBObject(fid, 1))
                .first();
        return null != first && null != first.get(fid) ? first.getString(fid) : "";
    }

    /**
     * @return Inquiry Manager followup user's list as Map from Mysql
     */
    public Map<String, String> getInquiryUsersMap(String app_id) {
        Map<String, String> map = new HashMap<>();
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT role_ids FROM master_inquiries "
                    + "WHERE ownership='can_action' AND org_id=?");
            ps.setString(1, app_id);
            ResultSet rs = ps.executeQuery();
            String rids = "";
            while (rs.next()) {
                rids = rs.getString("role_ids");
            }
            rs.close();
            ps.clearParameters();
            String[] roles = rids.split(",");
            for (String role : roles) {
                map.putAll(this.getUsersByRole(role, app_id));
            }
            ps.close();
        } catch (SQLException ex) {

        }
        return map;
    }

    /**
     * @return Get users as Map list by User Role Id
     */
    public Map<String, String> getUsersByRole(String roleid, String app_code) {
        Map<String, String> map = new HashMap<>();
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT uid,name FROM org_portal_users "
                    + "WHERE role_id=? AND org_id=?");
            ps.setString(1, roleid);
            ps.setString(2, app_code);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                map.put(rs.getString("uid"), rs.getString("name"));
            }
            rs.close();
            ps.clearParameters();
            ps.close();
            return map;
        } catch (SQLException ex) {

        }
        return map;
    }

    /*
    public List<Map<String, Object>> getAllProduct(String app_code) {

        List<Map<String, Object>> listdata = new ArrayList<Map<String, Object>>();

        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT product_id,product FROM app_inventory_products where org_id=? ");
            ps.setString(1, app_code);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Map<String, Object> jarr = new HashMap<String, Object>();
                jarr.put("id:", rs.getString("product_id"));
                jarr.put("name:", rs.getString("product"));
                listdata.add(jarr);
            }

            rs.close();
            ps.clearParameters();
            ps.close();

            return listdata;
        } catch (SQLException ex) {
        }

        return null;
    }

    public List<Map<String, Object>> getAllCategories(String app_code) {

        List<Map<String, Object>> listdata = new ArrayList<Map<String, Object>>();

        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT id,category FROM app_inventory_product_category where org_id=? ");
            ps.setString(1, app_code);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Map<String, Object> jarr = new HashMap<String, Object>();
                jarr.put("id", rs.getString("id"));
                jarr.put("category", rs.getString("category"));
                listdata.add(jarr);
            }

        } catch (SQLException ex) {
        }

        return listdata;
    }

    public List<Map<String, Object>> getAllSupplier(String app_code) {

        List<Map<String, Object>> listdata = new ArrayList<Map<String, Object>>();
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT product_id, purchased_from FROM app_inventory_products where org_id=? ");
            ps.setString(1, app_code);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Map<String, Object> jarr = new HashMap<String, Object>();
                jarr.put("product_id", rs.getString("product_id"));
                jarr.put("purchased_from", rs.getString("purchased_from"));
                listdata.add(jarr);
            }

        } catch (SQLException ex) {
        }

        return listdata;
    }
     */
    public void getMyUpperTeam(String app_code, String uid) {

        try {
            PreparedStatement ps = this.con.prepareStatement("WITH RECURSIVE recparent(reports_to ) AS\n" +
"				(\n" +
"				  SELECT reports_to\n" +
"				    FROM org_portal_users\n" +
"				    WHERE uid= ? AND org_id=?\n" +
"                                     AND reports_to !=\"0\"\n" +
"				  UNION ALL\n" +
"				  SELECT c.reports_to\n" +
"				    FROM recparent AS cp JOIN org_portal_users AS c\n" +
"				      ON cp.reports_to = c.uid\n" +
"				)\n" +
"				SELECT * FROM recparent ");
            ps.setString(1, uid);
            ps.setString(2, app_code);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (!rs.getString("reports_to").equals("0")) {
                    JSONObject map = new JSONObject();
                    map.put("id", rs.getString("reports_to"));
                    map.put("name", getUserNameByUID(rs.getString("reports_to"), app_code));
                    maplist.add(map);
                }
            }

        } catch (SQLException e) {
        }
    }

    public String getEmail(String app_code, String uid) {
        String email = "";
        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT email FROM users WHERE org_id=? and uid=? ");
            ps.setString(1, app_code);
            ps.setString(2, uid);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                email = rs.getString("email");
            }

        } catch (SQLException e) {
        }
        return email;
    }

    public String getWarranty(String id) {
        String warranty = new String();

        try {
            PreparedStatement ps = this.con.prepareStatement("SELECT warrenty FROM master_warrenties WHERE id=? ");
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                warranty = rs.getString("warrenty");
            }
        } catch (SQLException e) {
        }
        Helper.printConsole(warranty);
        return warranty;
    }
}
