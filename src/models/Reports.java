/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import helpers.DateParser;
import helpers.Helper;
import interfaces.BaseModal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import models.helper.CommonModel;
import org.bson.Document;
import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import services.PostFormService;

/**
 *
 * @author viru
 */
public class Reports extends BaseModal {

    public String filter = "";

    PostFormService pfs = new PostFormService();

    public String getSaleFunnel() {
        try {
            CommonModel cm = new CommonModel();
            cm.con = this.supplyMySql();
            cm.jedis = this.supplyJedis();
            String deal = cm.getFormIdBySlug(app_code, "deals");
            MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                    .getCollection(deal);
            //  System.out.print("table=" + deal);
            Helper.printConsole(this.filter);
            JSONObject fil = (JSONObject) JSONValue.parse(this.filter);
            Helper.printConsole(fil);

            Map<String, String> list = cm.getLeadStatusList(app_code);
            String uid = null != fil.get("employee") && !fil.get("employee").toString().isEmpty()
                    ? fil.get("employee").toString()
                    : fil.get("manager").toString();

            BasicDBObject clause2 = new BasicDBObject();
            Date today = DateParser.returnDateFromZuluString(fil.get("start_date").toString());
            Helper.printConsole("today=" + today);
            Date end = DateParser.returnDateFromZuluString(fil.get("end_date").toString());
            clause2.append("$gte", today).append("$lte", end);
            Helper.printConsole("end=" + end);
            Helper.printConsole(uid);

            List<String> myTeamAsList = cm.getMyDirectJunior(uid, app_code);

            BasicDBObject unwind = new BasicDBObject(
                    "$unwind", "$product");
            BasicDBObject id = new BasicDBObject("$group", new BasicDBObject(
                    "_id", null
            ).append("total", new BasicDBObject("$sum", "$product.total"))
                    .append("count", new BasicDBObject("$sum", 1)));

            JSONArray jarr = new JSONArray();
            Map<String, String> users = cm.getUserMapByORG(this.app_code);
            list.forEach((k, v) -> {
                JSONObject json = new JSONObject();
                json.put("status", v);
                JSONArray udata = new JSONArray();

                myTeamAsList.forEach((_item) -> {
                    List<String> mynlist = cm.getMyTeamAsList(_item, app_code);
                    BasicDBObject query = new BasicDBObject("deal_status_key", k);
                    query.put("createon_date_filter", clause2);
                    query.put("manager_user_key", new BasicDBObject("$in", mynlist));

                    if (k.equals("won")) {
                        query.put("deal_convert_by", new BasicDBObject("$in", mynlist));
                        //query.put("deal_convert_on_date_filter", clause2);
                    }
                    // Helper.printConsole(query);
                    Document adata = col.aggregate(Arrays.asList(
                            new BasicDBObject("$match", query),
                            unwind,
                            id)).first();

                    Document count = col.aggregate(Arrays.asList(
                            new BasicDBObject("$match", query),
                            new BasicDBObject("$count", "total"))).first();
                    JSONObject injson = new JSONObject();

                    injson.put("employee", users.get(_item));
                    injson.put("total_deals", count == null ? 0 : count.get("total"));
                    injson.put("total_sale", adata == null ? 0 : adata.get("total"));
                    udata.add(injson);

                });
                json.put("deal_stat", udata);
                jarr.add(json);
            });
            users.clear();
            return jarr.toJSONString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getMyAccountList() {

        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        String deal = cm.getFormIdBySlug(app_code, "accounts");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(deal);
        
        JSONObject fil = (JSONObject) JSONValue.parse(this.filter);
        
        String uid = null != fil.get("employee") && !fil.get("employee").toString().isEmpty()
                ? fil.get("employee").toString()
                : fil.get("manager").toString();
        
        
        BasicDBObject findQuery = new BasicDBObject();
        findQuery.append("manager_user_key", new BasicDBObject("$in", cm.getMyTeamAsList(uid, app_code)));
        findQuery.append("isActive", true);
        findQuery.append("isCustomer", true);
        BasicDBObject pro = new BasicDBObject();
        pro.append("_id", 0);
        pro.append("acc_name", 1);
        pro.append("acc_type", 1);
        pro.append("territory_master", 1);
        pro.append("field_1578168298713", 1);
        FindIterable<Document> find = col.find(findQuery).projection(pro).sort(new BasicDBObject("acc_name", 1));
        JSONArray j = new JSONArray();
        AggregateIterable<Document> formFieldAndLabels = cm.getFormFieldAndLabels(app_code, deal);

        for (Document s : find) {
            j.add(s);
            
        }
        JSONArray heads = new JSONArray();
        for (Document d1 : formFieldAndLabels) {
            Document m = (Document) d1.get("fields");
            if (pro.containsKey(m.get("id"))) {
                heads.add(m);
            }

        }
        JSONObject jin = new JSONObject();
        jin.put("heads", heads);
        jin.put("data", j);
        find.iterator().close();
        formFieldAndLabels.iterator().close();
        return jin.toJSONString();

    }

    public String getMyDealList() {

        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        String deal = cm.getFormIdBySlug(app_code, "deals");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(deal);
         JSONObject fil = (JSONObject) JSONValue.parse(this.filter);
        
        String uid = null != fil.get("employee") && !fil.get("employee").toString().isEmpty()
                ? fil.get("employee").toString()
                : fil.get("manager").toString();
        
        
        BasicDBObject findQuery = new BasicDBObject();
        
        findQuery.append("manager_user_key", new BasicDBObject("$in", cm.getMyTeamAsList(uid, app_code)));
        findQuery.append("isActive", true);
        
        BasicDBObject pro = new BasicDBObject();
        pro.append("_id", 0);
        pro.append("field_1579092826995", 1);
        pro.append("acc_name", 1);
        pro.append("deal_status", 1);
        pro.append("ex_close_date", 1);
        FindIterable<Document> find = col.find(findQuery).projection(pro);
        JSONArray j = new JSONArray();
        
        AggregateIterable<Document> formFieldAndLabels = cm.getFormFieldAndLabels(app_code, deal);

        for (Document s : find) {
            j.add(s);
        }
        JSONArray heads = new JSONArray();
        for (Document d1 : formFieldAndLabels) {
            Document m = (Document) d1.get("fields");
            if (pro.containsKey(m.get("id"))) {
                heads.add(m);
            }

        }
        JSONObject jin = new JSONObject();
        jin.put("heads", heads);
        jin.put("data", j);
        find.iterator().close();
        formFieldAndLabels.iterator().close();
        return jin.toJSONString();

    }

    public String getDealsBySource() {

        JSONArray jarr = new JSONArray();
        Map<String, String> lstatus = new HashMap<>();
        JSONArray cols = new JSONArray();
        JSONObject heads = new JSONObject();
        JSONArray headArray = new JSONArray();
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        String deal = cm.getFormIdBySlug(app_code, "accounts");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(deal);
        Helper.printConsole(this.filter);
        JSONObject fil = (JSONObject) JSONValue.parse(this.filter);

        String uid = null != fil.get("employee") && !fil.get("employee").toString().isEmpty()
                ? fil.get("employee").toString()
                : fil.get("manager").toString();

        JSONObject jj = new JSONObject();
        jj.put("uid", uid);
        List<String> myTeamAsList = cm.getMyDirectJunior(uid, app_code);
        heads.put("id", "manager_user");
        heads.put("label", "Employee");
        heads.put("type", "s");
        headArray.add(heads);

        BasicDBObject fact = new BasicDBObject();
        BasicDBObject pro = new BasicDBObject();

        List<Object> options = cm.getFormFieldOptions(app_code, deal, "deal_source");

        for (Object s : options) {
            Document d = (Document) s;
            lstatus.put(d.getString("value"), d.getString("label"));
            JSONObject headin = new JSONObject();
            headin.put("id", d.getString("value"));
            headin.put("label", d.getString("label"));
            headin.put("type", "s");
            headArray.add(headin);
        }
        for (String t : myTeamAsList) {
            List<String> mynlist = cm.getMyTeamAsList(t, app_code);
            lstatus.forEach((k, v) -> {

                fact.append(v, Arrays.asList(
                        new BasicDBObject("$match", new BasicDBObject("deal_source_key", k).append("manager_user_key", new BasicDBObject("$in", mynlist))),
                        new BasicDBObject("$count", v)
                ));
                pro.append(v, new BasicDBObject("$arrayElemAt", Arrays.asList("$" + v + "." + v, 0)));
            });

            AggregateIterable<Document> aggregate = col.aggregate(Arrays.asList(
                    new BasicDBObject("$facet", fact),
                    new BasicDBObject("$project", pro)
            ));
            String name = cm.getUserNameByUID(t, this.app_code);
            for (Document a : aggregate) {
                JSONObject p = new JSONObject();
                p.put("manager_user", name);
                lstatus.forEach((k, v) -> {
                    if (!a.containsKey(v)) {
                        p.put(k, 0);
                    } else {
                        p.put(k, a.get(v));
                    }
                });
                jarr.add(p);
            }
            aggregate.iterator().close();

        }

        // this.con.close();
        cols.add(0, "Name");
        JSONObject cols1 = new JSONObject();
        cols1.put("heads", headArray);
        cols1.put("data", jarr);
        return cols1.toJSONString();

    }

    public String getLeadsBySource() {

        JSONArray jarr = new JSONArray();
        Map<String, String> lstatus = new HashMap<>();
        JSONArray cols = new JSONArray();
        JSONObject heads = new JSONObject();
        JSONArray headArray = new JSONArray();
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        String deal = cm.getFormIdBySlug(app_code, "accounts");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(deal);
        JSONObject fil = (JSONObject) JSONValue.parse(this.filter);

        String uid = null != fil.get("employee") && !fil.get("employee").toString().isEmpty()
                ? fil.get("employee").toString()
                : fil.get("manager").toString();

        JSONObject jj = new JSONObject();
        jj.put("uid", uid);
        List<String> myTeamAsList = cm.getMyDirectJunior(uid, app_code);
        heads.put("id", "manager_user");
        heads.put("label", "Employee");
        heads.put("type", "s");
        headArray.add(heads);

        BasicDBObject fact = new BasicDBObject();
        BasicDBObject pro = new BasicDBObject();

        List<Object> options = cm.getFormFieldOptions(app_code, deal, "deal_source");

        for (Object s : options) {
            Document d = (Document) s;
            lstatus.put(d.getString("value"), d.getString("label"));
            JSONObject headin = new JSONObject();
            headin.put("id", d.getString("value"));
            headin.put("label", d.getString("label"));
            headin.put("type", "s");
            headArray.add(headin);
        }
        for (String t : myTeamAsList) {
            List<String> mynlist = cm.getMyTeamAsList(t, app_code);
            lstatus.forEach((k, v) -> {

                fact.append(v, Arrays.asList(
                        new BasicDBObject("$match", new BasicDBObject("deal_source_key", k).append("manager_user_key", new BasicDBObject("$in", mynlist))),
                        new BasicDBObject("$count", v)
                ));
                pro.append(v, new BasicDBObject("$arrayElemAt", Arrays.asList("$" + v + "." + v, 0)));
            });

            AggregateIterable<Document> aggregate = col.aggregate(Arrays.asList(
                    new BasicDBObject("$facet", fact),
                    new BasicDBObject("$project", pro)
            ));
            String name = cm.getUserNameByUID(t, this.app_code);
            for (Document a : aggregate) {
                JSONObject p = new JSONObject();
                p.put("manager_user", name);
                lstatus.forEach((k, v) -> {
                    if (!a.containsKey(v)) {
                        p.put(k, 0);
                    } else {
                        p.put(k, a.get(v));
                    }
                });
                jarr.add(p);
            }
            aggregate.iterator().close();

        }

        // this.con.close();
        cols.add(0, "Name");
        JSONObject cols1 = new JSONObject();
        cols1.put("heads", headArray);
        cols1.put("data", jarr);
        return cols1.toJSONString();

    }

    public String getMonthlySale() {

        CommonModel cm = new CommonModel();

        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        String deal = cm.getFormIdBySlug(app_code, "deals");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(deal);
        JSONObject fil = (JSONObject) JSONValue.parse(this.filter);
        String uid = null != fil.get("employee") && !fil.get("employee").toString().isEmpty()
                ? fil.get("employee").toString()
                : fil.get("manager").toString();
        Helper.printConsole(uid);
        Date today = DateParser.getStartTimeOfByDate(fil.get("start_date").toString(),this.tz);
       
        Date end = DateParser.getEndTimeOfByDate(fil.get("end_date").toString(),this.tz);
        BasicDBObject clause2
                = new BasicDBObject("$gte", today)
                        .append("$lte", end);
        JSONArray headArr = new JSONArray();
        JSONArray dataArr = new JSONArray();
        JSONObject heads = new JSONObject();
        heads.put("type", "u");
        heads.put("id", "uname");
        heads.put("label", "Employee");
        headArr.add(heads);

        JSONObject heads2 = new JSONObject();
        heads2.put("type", "p");
        heads2.put("id", "number_sales");
        heads2.put("label", "Total Number Sales");
        headArr.add(heads2);
        JSONObject heads3 = new JSONObject();
        heads3.put("type", "price");
        heads3.put("id", "target");
        heads3.put("label", "Target(Monthly)");
        headArr.add(heads3);
        JSONObject heads1 = new JSONObject();
        heads1.put("type", "price");
        heads1.put("id", "total_sale");
        heads1.put("label", "Total Sale");
        headArr.add(heads1);
        List<String> myTeamAsList = cm.getMyDirectJunior(uid, app_code);
        Map<String, String> users = cm.getUserMapByORG(this.app_code);
        Map<String, String> targets = cm.getUserTargetMap(this.app_code);
        // Helper.printConsole(targets);
        myTeamAsList.stream().map((t) -> {

            List<String> mt = cm.getMyTeamAsList(t, app_code);
            JSONObject json = new JSONObject();
            Document first = col.aggregate(Arrays.asList(
                    new BasicDBObject("$unwind", "$product"),
                    new BasicDBObject("$match", new BasicDBObject("deal_status_key", "won")
                            .append("deal_convert_by", new BasicDBObject("$in", mt)).append("deal_convert_on_date_filter", clause2)),
                    new BasicDBObject("$group", new BasicDBObject(
                            new BasicDBObject("_id", new BasicDBObject())
                                    .append("total", new BasicDBObject("$sum", "$product.total"))
                                    .append("count", new BasicDBObject("$sum", 1))
                    ))
            )).first();

            json.put("uname", users.get(t));
            json.put("target", targets.get(t));
            json.put("number_sales", 0);
            json.put("total_sale", 0);
            if (first != null) {
                json.put("number_sales", first.get("count"));
                json.put("total_sale", first.get("total"));
            }
            return json;
        }).forEachOrdered((json) -> {
            dataArr.add(json);
        });
        JSONObject gopt = new JSONObject();
        gopt.put("type", "bar");
        gopt.put("barChartLabelKey", "uname");
        gopt.put("barsKeys", "target,total_sale");
        JSONObject cols1 = new JSONObject();
        cols1.put("heads", headArr);
        cols1.put("data", dataArr);
        cols1.put("type", "bar_type");
        cols1.put("graph_opt", gopt);
        return cols1.toJSONString();

    }

    public String getAnnualSaleReport() {

        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        String deal = cm.getFormIdBySlug(app_code, "deals");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(deal);
        JSONObject fil = (JSONObject) JSONValue.parse(this.filter);
        String uid = null != fil.get("employee") && !fil.get("employee").toString().isEmpty()
                ? fil.get("employee").toString()
                : fil.get("manager").toString();
        Helper.printConsole(uid);
        Date today = DateParser.returnDateFromZuluString(fil.get("start_date").toString());
        Date end = DateParser.returnDateFromZuluString(fil.get("end_date").toString());

        Helper.printConsole("start=" + today);
        Helper.printConsole("end=" + end);
        //int year = Integer.parseInt(fil.get("year").toString());
        JSONArray headArr = new JSONArray();
        JSONArray dataArr = new JSONArray();
        JSONObject heads = new JSONObject();
        heads.put("type", "u");
        heads.put("id", "month");
        heads.put("label", "Month");
        headArr.add(heads);
        JSONObject heads1 = new JSONObject();
        heads1.put("type", "u");
        heads1.put("id", "sale_number");
        heads1.put("label", "Sale Numbers");
        headArr.add(heads1);
        JSONObject heads2 = new JSONObject();
        heads2.put("type", "price");
        heads2.put("id", "total_sale");
        heads2.put("label", "Total Sale");
        headArr.add(heads2);
        List<String> mt = cm.getMyTeamAsList(uid, app_code);
        long totalCount = 0;
        double totalSale = 0;
        DateFormat format = new SimpleDateFormat("MMM-yyy");
        String month = new String();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        try {
            while (today.before(end)) {
                JSONObject json = new JSONObject();
                BasicDBObject clause2 = new BasicDBObject();
                DateTime start = new DateTime(today);
                int sm = start.getMonthOfYear();
                int sy = start.getYear();

                //Helper.printConsole(m);
                //System.err.println(y);
                Date startDate = DateParser.getFirstDateOfMonth(sm, sy);
                Date endDate = DateParser.getLastDateOfMonth(sm, sy);
                Helper.printConsole("start=" + startDate);
                Helper.printConsole("end=" + endDate);
                clause2.append("$gte", startDate).append("$lte", endDate);
                month = format.format(today);
                c.add(Calendar.MONTH, 1);
                today = c.getTime();
                json.put("month", month);
                Helper.printConsole(month);

                // }
                Document first = col.aggregate(Arrays.asList(
                        new BasicDBObject("$unwind", "$product"),
                        new BasicDBObject("$match", new BasicDBObject("deal_status_key", "won")
                                .append("deal_convert_by", new BasicDBObject("$in", mt)).append("deal_convert_on_date_filter", clause2)),
                        //                    new BasicDBObject("$gte", DateParser.getFirstDateOfMonth(i, year))
                        //.append("$lte", DateParser.getLastDateOfMonth(i, year)))),
                        new BasicDBObject("$group", new BasicDBObject(
                                new BasicDBObject("_id", new BasicDBObject())
                                        .append("total", new BasicDBObject("$sum", "$product.total"))
                                        .append("count", new BasicDBObject("$sum", 1))
                        ))
                )).first();
                json.put("sale_number", 0);
                json.put("total_sale", 0);
                if (first != null) {
                    json.put("sale_number", first.get("count"));
                    json.put("total_sale", first.get("total"));
                    totalCount = totalCount + first.getInteger("count");
                    if (first.get("total") instanceof Double) {
                        totalSale = totalSale + first.getDouble("total");
                    } else if (first.get("total") instanceof Long) {
                        totalSale = totalSale + first.getLong("total");
                    } else {
                        totalSale = totalSale + first.getInteger("total");
                    }

                }
                dataArr.add(json);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject gopt = new JSONObject();
        gopt.put("type", "bar");
        gopt.put("barChartLabelKey", "month");
        gopt.put("barsKeys", "sale_number,total_sale");

        JSONObject tot = new JSONObject();
        tot.put("month", "Total");
        tot.put("sale_number", totalCount);
        tot.put("total_sale", totalSale);
        dataArr.add(tot);
        JSONObject cols1 = new JSONObject();
        cols1.put("heads", headArr);
        cols1.put("data", dataArr);
        cols1.put("type", "bar_type");
        cols1.put("graph_opt", gopt);
        return cols1.toJSONString();

    }

    public String getSourceReport() {

        JSONArray jarr = new JSONArray();
        JSONArray headArray = new JSONArray();
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        String deal = cm.getFormIdBySlug(app_code, "accounts");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(deal);
        JSONObject fil = (JSONObject) JSONValue.parse(this.filter);
        String uid = null != fil.get("employee") && !fil.get("employee").toString().isEmpty()
                ? fil.get("employee").toString()
                : fil.get("manager").toString();
        JSONObject jj = new JSONObject();
        jj.put("uid", uid);
        List<String> myTeamAsList = cm.getMyTeamAsList(uid, app_code);
        JSONObject heads = new JSONObject();
        heads.put("id", "source_name");
        heads.put("label", "Lead Source");
        heads.put("type", "s");
        headArray.add(heads);
        JSONObject heads1 = new JSONObject();
        heads1.put("id", "source_count");
        heads1.put("label", "Accounts");
        heads1.put("type", "s");
        headArray.add(heads1);

        BasicDBObject fact = new BasicDBObject();
        BasicDBObject pro = new BasicDBObject();

        List<Object> options = cm.getFormFieldOptions(app_code, deal, "deal_source");

        for (Object s : options) {
            Document d = (Document) s;

            fact.append(d.getString("value"), Arrays.asList(
                    new BasicDBObject("$match", new BasicDBObject("deal_source_key", d.getString("value"))
                            .append("manager_user_key", new BasicDBObject("$in", myTeamAsList))),
                    new BasicDBObject("$count", d.getString("value"))
            ));
            pro.append(d.getString("value"), new BasicDBObject("$arrayElemAt", Arrays.asList("$" + d.getString("value")
                    + "." + d.getString("value"), 0)));

            AggregateIterable<Document> aggregate = col.aggregate(Arrays.asList(
                    new BasicDBObject("$facet", fact),
                    new BasicDBObject("$project", pro)
            ));

            JSONObject p = new JSONObject();
            p.put("source_name", d.getString("label"));

            if (!aggregate.first().containsKey(d.getString("value"))) {
                p.put("source_count", 0);
            } else {
                p.put("source_count", aggregate.first().get(d.getString("value")));
            }

            jarr.add(p);

            aggregate.iterator().close();

        }

        // this.con.close();
        JSONObject cols1 = new JSONObject();
        cols1.put("heads", headArray);
        cols1.put("data", jarr);
        cols1.put("type", "bar_type");
        return cols1.toJSONString();

    }

    public String getAnnualConsalidateSaleReport() {

        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        String deal = cm.getFormIdBySlug(app_code, "deals");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(deal);
        JSONObject fil = (JSONObject) JSONValue.parse(this.filter);
        String uid = null != fil.get("employee") && !fil.get("employee").toString().isEmpty()
                ? fil.get("employee").toString()
                : fil.get("manager").toString();
        Helper.printConsole(uid);
        Date today = DateParser.returnDateFromZuluString(fil.get("start_date").toString());
        Date end = DateParser.returnDateFromZuluString(fil.get("end_date").toString());
        BasicDBObject clause2 = new BasicDBObject();
        clause2.append("$gte", today)
                .append("$lte", end);

        //int year = Integer.parseInt(fil.get("year").toString());
        JSONArray headArr = new JSONArray();
        JSONArray dataArr = new JSONArray();
        JSONObject heads = new JSONObject();
        heads.put("type", "u");
        heads.put("id", "month");
        heads.put("label", "Month");
        headArr.add(heads);
        List<String> mt = cm.getMyDirectJunior(uid, app_code);
        Map<String, String> users = cm.getUserMapByORG(this.app_code);

        for (String u : mt) {
            JSONObject heads1 = new JSONObject();
            heads1.put("type", "price");
            heads1.put("id", u);
            heads1.put("label", users.get(u) + " ( INR )");
            headArr.add(heads1);
        }

        JSONObject json = new JSONObject();

        for (String u : mt) {
            List<String> myl = cm.getMyTeamAsList(u, app_code);

            Document first = col.aggregate(Arrays.asList(
                    new BasicDBObject("$unwind", "$product"),
                    new BasicDBObject("$match", new BasicDBObject("deal_status_key", "won")
                            .append("deal_convert_by", new BasicDBObject("$in", myl))
                            .append("deal_convert_on_date_filter", clause2)),
                    new BasicDBObject("$group", new BasicDBObject(
                            new BasicDBObject("_id", new BasicDBObject())
                                    .append("total", new BasicDBObject("$sum", "$product.total"))
                    ))
            )).first();
            json.put(u, 0);
            if (first != null) {
                json.put(u, first.get("total"));
            }

        }
        dataArr.add(json);
        //}
        JSONObject gopt = new JSONObject();
        gopt.put("type", "bar");
        gopt.put("barChartLabelKey", "month");
        gopt.put("barsKeys", String.join(",", mt));
        JSONObject cols1 = new JSONObject();
        cols1.put("heads", headArr);
        cols1.put("data", dataArr);
        cols1.put("type", "bar_type");
        cols1.put("graph_opt", gopt);
        return cols1.toJSONString();

    }

    public String getInqueryReport() {

        JSONArray jarr = new JSONArray();
        JSONArray headArray = new JSONArray();
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        String form = cm.getFormIdBySlug(app_code, "inquiries");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(form);

        JSONObject fil = (JSONObject) JSONValue.parse(this.filter);
        String uid = null != fil.get("employee") && !fil.get("employee").toString().isEmpty()
                ? fil.get("employee").toString()
                : fil.get("manager").toString();

        Helper.printConsole(uid);
        JSONObject jj = new JSONObject();
        jj.put("uid", uid);
        Map<String, String> inqUsers = cm.getMyDirectJuniorMap(uid, app_code);
        List<String> myJuniors = cm.getMyDirectJunior(uid, app_code);
        inqUsers.put(uid, cm.getUserNameByUID(uid, app_code));
        Helper.printConsole(inqUsers);
        
        JSONObject heads = new JSONObject();
        heads.put("id", "uname");
        heads.put("label", "Employee");
        heads.put("type", "u");
        headArray.add(heads);

        JSONObject heads1 = new JSONObject();
        heads1.put("id", "new_inquiries");
        heads1.put("label", "New Enqueries");
        heads1.put("type", "s");
        headArray.add(heads1);

        JSONObject heads2 = new JSONObject();
        heads2.put("id", "follow_up_inquiries");
        heads2.put("label", "Follow up");
        heads2.put("type", "s");
        headArray.add(heads2);

        JSONObject heads3 = new JSONObject();
        heads3.put("id", "total_calls");
        heads3.put("label", "Total Calls");
        heads3.put("type", "s");
        headArray.add(heads3);

        JSONObject heads4 = new JSONObject();
        heads4.put("id", "interested_inquiries");
        heads4.put("label", "Interested");
        heads4.put("type", "s");
        headArray.add(heads4);

        JSONObject heads5 = new JSONObject();
        heads5.put("id", "not_interested_inquiries");
        heads5.put("label", "Not Interested");
        heads5.put("type", "s");
        headArray.add(heads5);

        JSONObject heads6 = new JSONObject();
        heads6.put("id", "follow_up_again");
        heads6.put("label", "Follow up Again");
        heads6.put("type", "s");
        headArray.add(heads6);

        JSONObject heads7 = new JSONObject();
        heads7.put("id", "account_created_by");
        heads7.put("label", "Account Created");
        heads7.put("type", "s");
        headArray.add(heads7);

    
        BasicDBObject clause2 = new BasicDBObject();

        Date today = DateParser.getStartTimeOfByDate(fil.get("start_date").toString(),this.tz);
       
        Date end = DateParser.getEndTimeOfByDate(fil.get("end_date").toString(),this.tz);
        
        clause2.append("$gte", today)
                .append("$lte", end);
        
        Helper.printConsole(clause2);
        for (String mjUid : myJuniors) {
            BasicDBObject fact = new BasicDBObject();
            BasicDBObject pro = new BasicDBObject();
            BasicDBList basicDBList = new BasicDBList();
            basicDBList.add("Intersted");
            basicDBList.add("New Enquiry");
            basicDBList.add("Not Intersted");
            basicDBList.add("Folllow Up");

            BasicDBList basicDBList1 = new BasicDBList();
            BasicDBObject db1=new BasicDBObject("is_new", true);
            BasicDBObject db2= new BasicDBObject("last_modify_followup", clause2);
            basicDBList1.add(db1);
            basicDBList1.add(db2);
            fact.append("new", Arrays.asList(
                    new BasicDBObject("$match", new BasicDBObject("is_new", true)
                            .append("isActive", true)
                            .append("follow_up_by", mjUid)),
                    new BasicDBObject("$match", new BasicDBObject("createon_date", clause2)),
                    new BasicDBObject("$count", "new")
            ));
            pro.append("new", new BasicDBObject("$arrayElemAt", Arrays.asList("$new.new", 0)));
            fact.append("follow up", Arrays.asList(
                    new BasicDBObject("$match", new BasicDBObject("follow_up_by", mjUid)
                            .append("isActive", true)),
                    new BasicDBObject("$match", new BasicDBObject("last_modify_followup", clause2)),
                    new BasicDBObject("$count", "follow up")
            ));
            pro.append("follow up", new BasicDBObject("$arrayElemAt", Arrays.asList("$follow up.follow up", 0)));

            fact.append("interested", Arrays.asList(
                    new BasicDBObject("$match", new BasicDBObject("follow_up_by", mjUid)
                    .append("inquiry_status", basicDBList.get(0)).append("isActive", true)),
                    new BasicDBObject("$match", new BasicDBObject("last_modify_followup", clause2)),
                    new BasicDBObject("$count", "interested")));
            pro.append("interested", new BasicDBObject("$arrayElemAt", Arrays.asList("$interested.interested", 0)));

            fact.append("not_interested", Arrays.asList(
                    new BasicDBObject("$match", new BasicDBObject("follow_up_by", mjUid)
                    .append("isActive", true).append("inquiry_status", basicDBList.get(2))),
                    new BasicDBObject("$match", new BasicDBObject("last_modify_followup", clause2)),
                    new BasicDBObject("$count", "not_interested")));
            pro.append("not_interested", new BasicDBObject("$arrayElemAt", Arrays.asList("$not_interested.not_interested", 0)));

            fact.append("followup_again", Arrays.asList(
                    new BasicDBObject("$match", new BasicDBObject("follow_up_by", mjUid)
                    .append("isActive", true).append("inquiry_status", basicDBList.get(3))),
                    new BasicDBObject("$match", new BasicDBObject("last_modify_followup", clause2)),                 
                    new BasicDBObject("$count", "followup_again")));
            pro.append("followup_again", new BasicDBObject("$arrayElemAt", Arrays.asList("$followup_again.followup_again", 0)));
            
            fact.append("total", Arrays.asList(
             new BasicDBObject("$match", new BasicDBObject("follow_up_by", mjUid).append("isActive", true)
             .append("$or", basicDBList1)),
           new BasicDBObject("$count", "total")                    
 
            ));
            pro.append("total", new BasicDBObject("$arrayElemAt", Arrays.asList("$total.total", 0)));

            AggregateIterable<Document> aggregate = col.aggregate(Arrays.asList(
                    new BasicDBObject("$facet", fact),
                    new BasicDBObject("$project", pro)
            ));

            JSONObject p = new JSONObject();
            p.put("uname", inqUsers.get(mjUid));
            if (null != aggregate && !aggregate.first().containsKey("total")) {
                p.put("total_calls", 0);
            } else {
                p.put("total_calls", aggregate.first().get("total"));
            }
            if (!aggregate.first().containsKey("new")) {
                p.put("new_inquiries", 0);
            } else {
                p.put("new_inquiries", aggregate.first().get("new"));
            }
            if (!aggregate.first().containsKey("follow_up")) {
                p.put("follow_up_inquiries", 0);
            } else {
                p.put("follow_up_inquiries", aggregate.first().get("follow_up"));
            }

            if (!aggregate.first().containsKey("not_interested")) {
                p.put("not_interested_inquiries", 0);
            } else {
                p.put("not_interested_inquiries", aggregate.first().get("not_interested"));
            }

            if (!aggregate.first().containsKey("interested")) {
                p.put("interested_inquiries", 0);
            } else {
                p.put("interested_inquiries", aggregate.first().get("interested"));
            }
            if (!aggregate.first().containsKey("followup_again")) {
                p.put("follow_up_again", 0);
            } else {
                p.put("follow_up_again", aggregate.first().get("followup_again"));
            }

            if (!aggregate.first().containsKey("follow up")) {
                p.put("follow_up_inquiries", 0);
            } else {
                p.put("follow_up_inquiries", aggregate.first().get("follow up"));
            }

            aggregate.iterator().close();

            String form1 = cm.getFormIdBySlug(app_code, "accounts");
            MongoCollection<Document> col1 = this.supplyMongo().getDatabase(this.app_code)
                    .getCollection(form1);
            BasicDBObject fact1 = new BasicDBObject();
            BasicDBObject pro1 = new BasicDBObject();

            fact1.append("account_created", Arrays.asList(
                    new BasicDBObject("$match", new BasicDBObject("follow_up_by", mjUid)
                            .append("isActive", true)
                            .append("createon_date_filter", clause2)),
                    new BasicDBObject("$count", "account_created")
            ));
            pro1.append("account_created", new BasicDBObject("$arrayElemAt", Arrays.asList("$account_created.account_created", 0)));
            AggregateIterable<Document> aggregate1 = col1.aggregate(Arrays.asList(
                    new BasicDBObject("$facet", fact1),
                    new BasicDBObject("$project", pro1)
            ));
            JSONObject p1 = new JSONObject();
            if (null != aggregate1 && !aggregate1.first().containsKey("account_created")) {
                p.put("account_created_by", 0);

            } else {
                p.put("account_created_by", aggregate1.first().get("account_created"));
            }

            jarr.add(p);
            aggregate1.iterator().close();

        }

        JSONObject cols1 = new JSONObject();
        cols1.put("heads", headArray);
        cols1.put("data", jarr);
        cols1.put("type", "bar_type");

        return cols1.toJSONString();

    }

    public String getSourceReportEnquery() {

        JSONArray jarr = new JSONArray();
        JSONArray headArray = new JSONArray();
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        
        String deal = cm.getFormIdBySlug(app_code, "inquiries");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(deal);
        
        JSONObject fil = (JSONObject) JSONValue.parse(this.filter);
        String uid = null != fil.get("employee") && !fil.get("employee").toString().isEmpty()
                ? fil.get("employee").toString()
                : fil.get("manager").toString();
        JSONObject jj = new JSONObject();
        jj.put("uid", uid);
        
        List<String> myTeamAsList = cm.getMyTeamAsList(uid, app_code);
        
        JSONObject heads = new JSONObject();
        heads.put("id", "source_name");
        heads.put("label", "Lead Source");
        heads.put("type", "s");
        headArray.add(heads);
        JSONObject heads1 = new JSONObject();
        heads1.put("id", "source_count");
        heads1.put("label", "Enqueries");
        heads1.put("type", "s");
        headArray.add(heads1);

        BasicDBObject fact = new BasicDBObject();
        BasicDBObject pro = new BasicDBObject();

        List<Object> options = cm.getFormFieldOptions(app_code, deal, "deal_source");

        for (Object s : options) {
            Document d = (Document) s;

            fact.append(d.getString("value"), Arrays.asList(
                    new BasicDBObject("$match", new BasicDBObject("deal_source_key", d.getString("value"))
                    .append("follow_up_by", new BasicDBObject("$in", myTeamAsList))),
                    new BasicDBObject("$count", d.getString("value"))
            ));
            pro.append(d.getString("value"), new BasicDBObject("$arrayElemAt", Arrays.asList("$" + d.getString("value")
                    + "." + d.getString("value"), 0)));

            AggregateIterable<Document> aggregate = col.aggregate(Arrays.asList(
                    new BasicDBObject("$facet", fact),
                    new BasicDBObject("$project", pro)
            ));

            JSONObject p = new JSONObject();
            p.put("source_name", d.getString("label"));

            if (!aggregate.first().containsKey(d.getString("value"))) {
                p.put("source_count", 0);
            } else {
                p.put("source_count", aggregate.first().get(d.getString("value")));
            }

            jarr.add(p);

            aggregate.iterator().close();

        }

        // this.con.close();
        JSONObject cols1 = new JSONObject();
        cols1.put("heads", headArray);
        cols1.put("data", jarr);
        cols1.put("type", "bar_type");
        return cols1.toJSONString();

    }

    public String getMeetingReport() {

        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();

        BasicDBObject clause2 = new BasicDBObject();
        JSONObject fil = (JSONObject) JSONValue.parse(this.filter);
        JSONArray product = (JSONArray) fil.get("product");
        JSONArray category = (JSONArray) fil.get("category");
        JSONArray supplier = (JSONArray) fil.get("supplier");

        String uid = null != fil.get("employee") && !fil.get("employee").toString().isEmpty()
                ? fil.get("employee").toString()
                : fil.get("manager").toString();
    
        Date today = DateParser.getStartTimeOfByDate(fil.get("start_date").toString(),this.tz);
       
        Date end = DateParser.getEndTimeOfByDate(fil.get("end_date").toString(),this.tz);
 
        clause2.append("$gt", today)
                .append("$lt", end);
        //System.out.println("start"+today+"end"+end);
        //System.out.println("product"+product.get(0)+"category"+category.get(0)+"supplier"+supplier.get(0));
        String meeting = cm.getFormIdBySlug(app_code, "activity");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(meeting);

        JSONArray j = new JSONArray();
        JSONArray headArray = new JSONArray();

        JSONObject heads = new JSONObject();
        heads.put("id", "last_modify");
        heads.put("label", "Updated Date");
        heads.put("type", "s");
        headArray.add(heads);

        JSONObject heads1 = new JSONObject();
        heads1.put("id", "meeting_date");
        heads1.put("label", "Meeting Date");
        heads1.put("type", "s");
        headArray.add(heads1);

        JSONObject heads2 = new JSONObject();
        heads2.put("id", "acc_name");
        heads2.put("label", "Account name");
        heads2.put("type", "s");
        headArray.add(heads2);

        JSONObject heads3 = new JSONObject();
        heads3.put("id", "con_name");
        heads3.put("label", "Contact Name");
        heads3.put("type", "s");
        headArray.add(heads3);

        JSONObject heads5 = new JSONObject();
        heads5.put("id", "category");
        heads5.put("label", "Category");
        heads5.put("type", "s");
        headArray.add(heads5);

        JSONObject heads4 = new JSONObject();
        heads4.put("id", "field_1580055617309");
        heads4.put("label", "Deal");
        heads4.put("type", "s");
        headArray.add(heads4);

        JSONObject heads6 = new JSONObject();
        heads6.put("id", "products");
        heads6.put("label", "Product");
        heads6.put("type", "s");
        headArray.add(heads6);

        JSONObject heads7 = new JSONObject();
        heads7.put("id", "deal_status");
        heads7.put("label", "Deal Status");
        heads7.put("type", "s");
        headArray.add(heads7);

        JSONObject heads8 = new JSONObject();
        heads8.put("id", "est_close_date");
        heads8.put("label", "Est Closed Date");
        heads8.put("type", "s");
        headArray.add(heads8);

        JSONObject heads9 = new JSONObject();
        heads9.put("id", "field_1580057636833");
        heads9.put("label", "Remarks");
        heads9.put("type", "s");
        headArray.add(heads9);

        JSONObject heads10 = new JSONObject();
        heads10.put("id", "brands");
        heads10.put("label", "Brand");
        heads10.put("type", "s");
        headArray.add(heads10);

        List<String> mt = cm.getMyTeamAsList(uid, app_code);

        BasicDBObject pro = new BasicDBObject();
        pro.append("_id", 0);
        pro.append("last_modify", 1);
        pro.append("meeting_date", 1);
        pro.append("acc_name", 1);
        pro.append("con_name", 1);
        pro.append("product", 1);
        pro.append("field_1580055617309", 1);
        pro.append("deal_status", 1);
        pro.append("est_close_date", 1);
        pro.append("field_1580057636833", 1);

        BasicDBObject query = new BasicDBObject();
        query.append("isActive", true);
        query.append("meeting_date_date_filter", clause2);
        query.append("manager_user_key", new BasicDBObject("$in", mt));
        BasicDBList list_pro = new BasicDBList();
        BasicDBList list_cat = new BasicDBList();
        BasicDBList list_sup = new BasicDBList();

        List<BasicDBObject> criteria = new ArrayList<>();

        if (product.size() > 0) {
            product.forEach(p -> {
                list_pro.add(new BasicDBObject("product.product_details.product_id", p));
            });
            criteria.add(new BasicDBObject("$or", list_pro));
        }
        if (category.size() > 0) {
            category.forEach(p -> {
                list_cat.add(new BasicDBObject("product.category", p));
            });
            criteria.add(new BasicDBObject("$or", list_cat));
        }
        if (supplier.size() > 0) {
            supplier.forEach(p -> {
                list_sup.add(new BasicDBObject("product.brand.name", p));
            });
            criteria.add(new BasicDBObject("$or", list_sup));
        }
        if (!criteria.isEmpty()) {
            query.append("$and", criteria);
        }

       // Helper.printConsole(query);
        try {
            FindIterable<Document> first = col.find(query).projection(pro); 
            for (Document s : first) {
               JSONObject json = new JSONObject();
                if(null != s.get("product"))
                {
                    List<Document> prod = (List<Document>) s.get("product");
                    String products = "";
                    String brands = "";
                    String cats = "";
                    for(Document p : prod)
                    {
                        brands += (null != p.get("brand") 
                                ? p.get("brand",new Document()).getString("name") + "," : "");
                        cats += (null != p.get("category") 
                                ? p.getString("category") + "," : "") ;
                        products += (null != p.get("product") 
                                ? p.getString("product") 
                                : p.get("product_details",new Document()).getString("product")) + ",";
                        
                    }
                    json.put("products", products.substring(0, (products.length() > 0 ? products.length()-1 : 0)));
                    json.put("brands", brands.substring(0, (brands.length() > 0 ? brands.length()-1 : 0)));
                    json.put("category", cats.substring(0, (cats.length() >0 ? cats.length()-1 : 0)));
                }
                else
                {
                    json.put("products", "");
                    json.put("brands", "");
                    json.put("category", "");
                }
                json.put("meeting_date", s.get("meeting_date"));
                json.put("last_modify", s.get("last_modify"));
                json.put("est_close_date", s.get("est_close_date"));
                json.put("con_name",  s.get("con_name"));
                json.put("acc_name",s.get("acc_name"));
                json.put("field_1580055617309", s.get("field_1580055617309"));
                json.put("deal_status", s.get("deal_status"));
                json.put("field_1580057636833", s.get("field_1580057636833"));
                
                j.add(json);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject jin = new JSONObject();
        jin.put("heads", headArray);
        jin.put("data", j);
        return jin.toJSONString();
    }
    public String getContactList()
         {
        CommonModel cm = new CommonModel();
        cm.con = this.supplyMySql();
        cm.mongo = this.supplyMongo();
        cm.jedis = this.supplyJedis();
        String contact=cm.getFormIdBySlug(this.app_code,"contacts");
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection(contact);
        
        JSONArray j = new JSONArray();
        JSONArray headArray=new JSONArray();
        
        JSONObject fil = (JSONObject) JSONValue.parse(this.filter);
        
        String uid = null != fil.get("employee") && !fil.get("employee").toString().isEmpty()
                ? fil.get("employee").toString()
                : fil.get("manager").toString();
        
         List<String> myJuniors = cm.getMyDirectJunior(uid, app_code);
        
         JSONObject heads = new JSONObject();
          heads.put("id", "acc_name");
          heads.put("label", "Account Name");
          heads.put("type", "s");
          headArray.add(heads);

        JSONObject heads1 = new JSONObject();
        heads1.put("id", "con_email");
        heads1.put("label", "Email");
        heads1.put("type", "s");
        headArray.add(heads1);

        JSONObject heads3 = new JSONObject();
        heads3.put("id", "con_phone");
        heads3.put("label", "Contact No.");
        heads3.put("type", "s");
        headArray.add(heads3);
         
        JSONObject heads2= new JSONObject();
        heads2.put("id", "con_name");
        heads2.put("label", "Contact Name");
        heads2.put("type", "s");
        headArray.add(heads2);
        
        JSONObject heads4 = new JSONObject();
        heads4.put("id", "con_designation");
        heads4.put("label", "Designation");
        heads4.put("type", "s");
        headArray.add(heads4);
        
        
        for (String mjUid : myJuniors) {
        
        BasicDBObject findQuery = new BasicDBObject();
        findQuery.append("createby",mjUid);
        findQuery.append("isActive", true);
       
        BasicDBObject pro1 = new BasicDBObject();
        pro1.append("_id", 0);
        pro1.append("acc_name",1);
        //pro1.append("acc_name_key",1);
        pro1.append("con_email", 1);
        pro1.append("con_name",1);
        pro1.append("con_phone",1);
        pro1.append("con_designation",1);
        
        FindIterable<Document> findCon= col.find(findQuery).projection(pro1).sort(new BasicDBObject("acc_name", 1));
        for(Document s:findCon)
        {
           j.add(s);
            
        }
        }
           JSONObject jin = new JSONObject();
        jin.put("heads", headArray);
        jin.put("data", j);
        return jin.toJSONString();

        }

    
}
