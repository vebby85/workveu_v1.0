/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import helpers.DateParser;
import helpers.Helper;
import interfaces.BaseModal;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class Notifications extends BaseModal {

    public String data = "";
    public String uid = "";
    

    public void setNotification() {
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection("notifications");

        Document parse = Document.parse(this.data);
        String d = DateParser.getCurrentDateASZulu();
        parse.put("createDate", d);
        parse.put("nid", System.currentTimeMillis());
        parse.put("createDate_filter", DateParser.returnDateFromZuluString(d));

        col.insertOne(parse);

    }

    public String getNotifications() {
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection("notifications");
        FindIterable<Document> find = col.find(new BasicDBObject(
                "to", this.uid
        )).projection(new BasicDBObject("_id", 0).append("createDate_filter", 0).append("to", 0))
                .sort(new BasicDBObject("createDate_filter", -1))
                .skip(0)
                .limit(10);
        JSONArray arr = new JSONArray();
        for (Document d : find) {
            boolean read = false;
            if(d.containsKey("readby"))
            {
                ArrayList<String> m = d.get("readby",new ArrayList<>());
                if(m.contains(this.uid))
                {
                    read = true;
                }
            }
            d.put("read", read);
            arr.add(d);
        }
        find.iterator().close();
        return arr.toJSONString();
    }

    public String getNotificationsCount() {

        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection("notifications");
        List<String> l = new ArrayList<>();
        l.add(this.uid);
        long countDocuments = col.countDocuments(new BasicDBObject(
                "to", this.uid
        ).append("readby", new BasicDBObject("$nin", l)
        ));

        JSONObject j = new JSONObject();
        j.put("count", countDocuments);
        return j.toJSONString();

    }

    public void seen(String nid) {
        MongoCollection<Document> col = this.supplyMongo().getDatabase(this.app_code)
                .getCollection("notifications");
        List<String> l = new ArrayList<>();
        l.add(this.uid);
        if (null != nid && ! nid.isEmpty()) {  
             
        col.updateOne(new BasicDBObject("to", this.uid).append("nid",Long.parseLong(nid))
        .append("readby", new BasicDBObject("$nin", l)), new BasicDBObject("$addToSet", new BasicDBObject("readby", this.uid)));
          
       }
         else{
            
        col.updateMany(new BasicDBObject("to", this.uid)
        .append("readby", new BasicDBObject("$nin", l)), new BasicDBObject("$addToSet", new BasicDBObject("readby", this.uid)));
         
        }
       
     
    }
}
