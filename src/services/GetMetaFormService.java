/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.Helper;
import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.GetMetaForm;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class GetMetaFormService  extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   
        String trigger = request.getParameter("trigger");
        PrintWriter writer = response.getWriter();
        if(!RequestResponse.requiredParams(trigger))
        {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        String modid = request.getParameter("modid");
        String app_code = request.getParameter("app_code"); 
        GetMetaForm gm = new GetMetaForm();
        Helper.printConsole(trigger);
        gm.app_code = app_code;
        String output = "";
      //  Helper.printConsole(trigger);
                
        switch(trigger)
        {
            case "get_attachments" : 
                String doc_type = request.getParameter("doc_type");
                output = gm.getAttachments(modid,doc_type);
                break;
            case "get_comments" : 
                output = gm.getComments(modid);
                break;
            case "get_followups" : 
                output = gm.getFolloups(modid);
                break;
            case "get_emails_by_mod" : 
                output = gm.getEmailsByMod(modid);
                break;
            case "get_user_meta_by_id" :
                output = gm.getUserMetaById(modid);
                break;
            case "get_single_attachment" :
                String attachment = request.getParameter("attachment");
                Helper.printConsole(attachment);
                output = gm.getSingleAttachment(modid,attachment);
                break;
            case "get_approvals" :
               String uid = request.getParameter("uid");
               String listsort = request.getParameter("list_sort");
                output = gm.getApprovals(uid, listsort); 
                break;
            case "generate_quotation_id" : 
                output = gm.getQuotationId();
        }
        gm.closeMySql();
        RequestResponse.returnSucResponse(response);
        writer.print(output); 
        writer.close();
    }
    
}
