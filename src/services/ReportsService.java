/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Reports;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class ReportsService extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
        String app_code = request.getParameter("app_code");
        String filter = request.getParameter("filter");
        String logged = request.getParameter("logged");
    
        PrintWriter writer = response.getWriter();
        if (!RequestResponse.requiredParams(trigger)) {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        String out = "";
        Reports report = new Reports();
        report.app_code = app_code;
        report.logged = logged;
        report.filter = filter;
        
        
        
        switch (trigger) {
            case "sale_funnel":
                out = report.getSaleFunnel();
            break;
           //case "monthly_activity":
              //  out = report.getMonthlyActivityReport();
            //break;
            case "my_customer_list":
                out = report.getMyAccountList();
            break;
            case "my_deal_list":
                out = report.getMyDealList();
            break;
            case "my_deals_by_source":
                out = report.getDealsBySource();
            break;
            case "monthly_sale":
                out = report.getMonthlySale();
            break;
            case "annual_sale":
                out = report.getAnnualSaleReport();
            break;
            case "source_report":
                out = report.getSourceReport();
            break;
            case "consolidated_annual":
                out = report.getAnnualConsalidateSaleReport();
            break;
            case "get_enquery_by_source":
                out = report.getSourceReportEnquery();
            break;
            case "daily_enquery":
                out=report.getInqueryReport();
            break;
            case "meeting_report":
                out=report.getMeetingReport();
            break;
            case "contacts_list":
               out=report.getContactList();
            break;
        }
        report.closeMySql();
        
        
        writer.print(out);
        writer.close();
    }
}
