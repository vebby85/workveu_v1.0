/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.DataList;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class DataListsService extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   
        String trigger = request.getParameter("trigger");
        String app_code = request.getParameter("app_code");
        String filter = request.getParameter("filter");
        String logged = request.getParameter("uid");
    
     
        PrintWriter writer = response.getWriter();
        if(!RequestResponse.requiredParams(trigger))
        {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        
        
        DataList dl = new DataList();
        dl.app_code = app_code;
        dl.logged = logged;
        dl.filter=  filter;
        //Helper.printConsole(trigger);
        String output = "";
        switch(trigger)
        {
            case "calender_data_list" :
                String start = request.getParameter("start");
                String end = request.getParameter("end");
                output = dl.initCalendarList(start,end);
                break;
            case "products_data_list":
              //  output=dl.listOfProducts(dl.app_code).toString();
                break;
            case "list_all_categories":
                // output=dl.listOfCategories(dl.app_code).toString();
            break;
            case "list_all_suppliers":
                //output=dl.listOfSuppliers(dl.app_code).toString();
            break; 
            case "list_sale_stats":
                output=dl.getSaleStats();
            break;
            case "list_product_sale":
                output=dl.SalesStatsByProducts();
            break;    
            case "sale_product_detail":
                 String product_code="134";
                //output=dl.getProductSaleDetail(product_code);
                break;
        }
        dl.closeMySql();
        RequestResponse.returnSucResponse(response);
        writer.print(output);
        writer.close();
        
    }
    
}
