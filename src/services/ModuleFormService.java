/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.Helper;
import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.ModuleForm;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
@SuppressWarnings("serial")
public class ModuleFormService extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
        PrintWriter writer = response.getWriter();
        if(!RequestResponse.requiredParams(trigger))
        {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        ModuleForm form = new ModuleForm();
        form.app_code = request.getParameter("app_code");
        String module_id = request.getParameter("module_id");
        switch(trigger)
        {
            case "update_module_form" :
                form.saveForm( module_id);
            break;
            case "update_all_module_form" :
                form.saveAllForm();
            break;
        }
        form.closeClass();
        writer.close();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Helper.printConsole("Hiits");
        String trigger = request.getParameter("trigger");
        String app_code = request.getParameter("app_code");
        String module_id = request.getParameter("module_id");
        PrintWriter writer = response.getWriter();
        if(!RequestResponse.requiredParams(trigger,app_code))
        {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        ModuleForm form = new ModuleForm();
        form.app_code = app_code;
        String output = "";
        switch(trigger)
        {
            case "get_form_fields" :
                output = form.getFormFields( module_id);
            break;
            case "get_form_fields_all" :
                output = form.getFormFieldsAll();
            break;
            case "get_text_fields_autocomplete" :
                output = form.getTextFieldsAutocomplete( module_id);
            break;
            case "check_unique_indb" :
                String fid = request.getParameter("field_id");
                String value = request.getParameter("value");
                output = form.checkUniqueInDb(module_id,fid,value);
            break;
        }
        form.closeClass();
        RequestResponse.returnSucResponse(response);
        writer.print(output);
        writer.close();
    }
    
    
}
