/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.Helper;
import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.AnalyticsModel;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class AnalyticsService extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   
        String trigger = request.getParameter("trigger");
        PrintWriter writer = response.getWriter();
        if(!RequestResponse.requiredParams(trigger))
        {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        
        AnalyticsModel form = new AnalyticsModel();
        form.logged = request.getParameter("uid");
        form.app_code = request.getParameter("app_code");
        String modid = request.getParameter("module_id");
        String output = "";
        switch(trigger)
        {
            case "get_inquery_counts" :
                String followupby = request.getParameter("follow_up_by");
                String followuudate = request.getParameter("follow_up_date");
                String filter=request.getParameter("filter");
                Helper.printConsole(filter);
                output = form.getInqueryCount(modid,followupby,followuudate,filter);
                break;
            case "get_account_detail_stats" :
                output = form.getAccountDetailStats(modid);
                break;
        }
        form.closeMySql();
        writer.print(output);
        writer.close();
    }
    
}
