/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.Helper;
import helpers.ServerStats;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class ServerStatsService extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        Helper.printConsole("Hiii");
        String trigger = request.getParameter("trigger");
        ServerStats st = new ServerStats();
        JSONObject json = new JSONObject();
        switch (trigger) {
            case "server_stats":
                
                json.put("server_stats", st.getSystemMemStats());
                break;
            case "db_stats":
                json.put("db_stats", st.getMySqlStats());
                break;
        }
        st.closeMySql();
        writer.print(json.toJSONString());
        writer.close();
    }

}
