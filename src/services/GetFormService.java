/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.GetForm;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class GetFormService extends HttpServlet {
    @Override
    @SuppressWarnings("empty-statement")
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   
        String trigger = request.getParameter("trigger");
     
        PrintWriter writer = response.getWriter();
        if(!RequestResponse.requiredParams(trigger))
        {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        String uid = request.getParameter("uid");
        String api = request.getParameter("api");
      
        GetForm form = new GetForm();
        form.app_code = request.getParameter("app_code");
        form.session = uid;
        String module_id = request.getParameter("module_id");
        form.limit = request.getParameter("limit");
        form.page = request.getParameter("page");
        form.search = request.getParameter("search");
        form.filter = request.getParameter("filter");
        System.out.println(request.getParameter("tz") + " ===============================");
        form.tz = request.getParameter("tz");
        
        
       // Helper.printConsole(form.filter);
        String output = "";
        switch(trigger)
        {
            case "get_data_list" :
                
                if(api != null && api.equals("app"))
                {
                    output = form.getListForApp(module_id);
                              //    Helper.printConsole("am hello");
                

                }
                else
                {
                output = form.getList(module_id);
                }
            break;
            case "get_data_list_api" :
                 output = form.getListAPI(module_id);
                
            break;
            case "get_inquery_data_list" :
                String followupby = request.getParameter("follow_up_by");
                output = form.getInqueryList(module_id,followupby);
            break;
            case "get_meeting_data_list" :
                
                output = form.getMeetingList(module_id);
            break;
            case "get_data_list_global" :
                String with = request.getParameter("with");
                
                if(api != null && api.equals("app"))
                {
                    output = form.getListGlobalForApp(module_id);
                }
                else
                {
                    output = form.getListGlobal(module_id,with);
                }
                
            break;
            case "get_data_list_action" :
                
                String u = request.getParameter("u");
                String action = request.getParameter("action");
                output = form.getListAction(module_id,u,action);
            break;
            
            case "get_account_information" :
                output = form.getAccountInformation(module_id);
            break;
            case "get_account_history" :
                output = form.getAccountHistory(module_id);
            break;
            case "get_data_list_as_calender" :
                output = form.getListAsCalender( module_id);
            break;
            case "get_data_single_record" :
                String record_id = request.getParameter("record_id");
                output = form.getSingleRecord(module_id,record_id);
            break;
            case "get_deal_manager" :
                String uname= request.getParameter("uname");
                String ro = request.getParameter("reports_to");
                String as = request.getParameter("as");
                if(null == as || as.isEmpty())
                {
                    as = "list";
                }
                if(as.equals("list"))
                {
                    output = form.getDealManager(uid,uname,ro);
                }
                else if(as.equals("tree"))
                {
                    output = form.getDealManagerAsTree(uid,uname,ro);
                }
                
                
            break;
            case "get_action_manager" :
                String uname1= request.getParameter("uname");
                String ro1 = request.getParameter("reports_to");
                String as1 = request.getParameter("as");
                if(null == as1 || as1.isEmpty())
                {
                    as1 = "list";   
                }
                if(as1.equals("list"))
                {
                    output = form.getActionManager(uid,uname1,ro1);
                }
                else if(as1.equals("tree"))
                {
                    output = form.getActionManagerAsTree(uid,uname1,ro1);
                }
                 break;
                    
        }
        
        //Helper.printConsole(output);
        form.closeMySql();
        RequestResponse.returnSucResponse(response);
        writer.print(output);
       
        writer.close();
    }
}
    

   