/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.Helper;
import helpers.RequestResponse;
import interfaces.Emails;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.PostMetaForm;
import org.bson.Document;
import org.json.simple.JSONObject;


/**
 *
 * @author viru
 */
public class EmailService extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
        String app_code = request.getParameter("app_code");
        PrintWriter writer = response.getWriter();
        if(!RequestResponse.requiredParams(trigger))
        {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        Emails email = new Emails();
        String to = request.getParameter("to");
        switch(trigger)
        {
            case "send_welcome_email" : 
                String uname = request.getParameter("name");
                String pass = request.getParameter("pass");
                String reportTo=request.getParameter("reports_to");
                Helper.printConsole(reportTo);
                if(!RequestResponse.requiredParams(to,uname,pass))
                {
                    JSONObject err = RequestResponse.setErrorObj("Parameter missing");
                    writer.print(err.toJSONString());
                    return;
                }
                
                email.setConfig();
                email.sendWelcomeEmail(to,uname,pass,reportTo);    
                break;
            case "send_forgot_password_email" : 
                String otp = request.getParameter("otp");
                if(!RequestResponse.requiredParams(to))
                {
                    JSONObject err = RequestResponse.setErrorObj("Parameter missing");
                    writer.print(err.toJSONString());
                    return;
                }
                email.setConfig();
                email.sendForgotPasswordEmail(to,otp);  
                break;
            case "save_and_send_simple_email" : 
                String data = request.getParameter("data");
                PostMetaForm pm = new PostMetaForm();
                pm.app_code = app_code;
                String eid = pm.saveSimpleEmail(data); 
                 email.setConfig();
                 email.app_code = app_code;
                if(!email.sendSimpleEmail(data))
                {
                    pm.updateEmailById(eid, new Document("sent_status", "Not sent"));
                }
                pm.updateEmailById(eid, new Document("sent_status", "Sent"));
                pm.closeMySql();
                break;
                
              case "daily_action_report":
                 email.setConfig();
                 email.app_code = app_code;
                 String name= request.getParameter("name");
                 String uId=request.getParameter("uid");
                 String report_To=request.getParameter("reportTo");
                 email.dailyActionManagerMail(uId,name,report_To); 
                 break;

        } 
        
        
        
    }
    
    
}
