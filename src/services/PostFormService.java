/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.Helper;
import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.PostForm;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author viru
 */
public class PostFormService extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
      
        PrintWriter writer = response.getWriter();
        if (!RequestResponse.requiredParams(trigger)) {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        String session = request.getParameter("session");
        String module_id = request.getParameter("module_id");
        String recordid = request.getParameter("record_id");
        PostForm form = new PostForm();
        JSONObject sessionJson = (JSONObject) JSONValue.parse(session);
        form.app_code = sessionJson.get("app_code").toString();
        JSONObject json = new JSONObject();
        String out = "";
       //Helper.printConsole(trigger);
        switch (trigger) {
            case "save_form":
                String formdata = request.getParameter("form_data");
                //Helper.printConsole(formdata + " data");
                
                if (!RequestResponse.requiredParams(session, module_id, formdata)) {
                    JSONObject err = RequestResponse.setErrorObj("Parameter missing");
                    writer.print(err.toJSONString());
                    writer.close();
                    return;
                }
                out = form.save(module_id, formdata, sessionJson);
                json.put("recordid", out);
                if(null != form.dealName && !form.dealName.isEmpty())
                {
                    json.put("dealname", form.dealName);
                }
                break;
            case "save_form_as_list":
                String form_list = request.getParameter("form_list");
                //Helper.printConsole(formdata + " data");
                
                if (!RequestResponse.requiredParams(session, module_id, form_list)) {
                    JSONObject err = RequestResponse.setErrorObj("Parameter missing");
                    writer.print(err.toJSONString());
                    writer.close();
                    return;
                }
                out = form.saveAsArray(module_id, form_list, sessionJson);
                json.put("recordid", out);
                
                break;
            case "save_integrated_inqueries" :
                String formdata1 = request.getParameter("form_data");
                String lead_source=request.getParameter("deal_source");
                Helper.printConsole(formdata1);
                if (!RequestResponse.requiredParams(session, formdata1)) {
                    JSONObject err = RequestResponse.setErrorObj("Parameter missing");
                    writer.print(err.toJSONString());
                    writer.close();
                    return;
                }
                String lead_src=null != lead_source && !lead_source.isEmpty()
                    ?lead_source:"indiaMart";
                out = form.saveIntegratedInquery( formdata1,lead_src);
                json.put("recordid", out);
                break;
            case "trash_single_record":
                if (form.trashRecord(module_id, recordid)) {
                    json.put("msg", "Record trashed successfully");
                } else {
                    json.put("msg", "Record not found");
                }
                break;
            case "cancel_order":
                if (form.cancelOrder(module_id, recordid)) {
                    json.put("msg", "Order Cancel successfully");
                } else {
                    json.put("msg", "Record not found");
                }
                break;
            case "check_exist_record":
                String fieldid = request.getParameter("field_id");
                String value = request.getParameter("value");
                if (!RequestResponse.requiredParams(session, module_id, fieldid)) {
                    JSONObject err = RequestResponse.setErrorObj("Parameter missing");
                    writer.print(err.toJSONString());
                    writer.close();
                    return;
                }
                boolean re = form.isExistRecordInMongo(sessionJson, module_id, fieldid, value);
                json.put("exist", re);

                break;
            case "update_single_field" :
                
                String field = request.getParameter("field");
                String value_key = request.getParameter("value_key");
                String valueUp = request.getParameter("value");
               
                if (!RequestResponse.requiredParams(field, valueUp, module_id,recordid)) {
                    JSONObject err = RequestResponse.setErrorObj("Parameter missing");
                    writer.print(err.toJSONString());
                    writer.close();
                    return;
                }
                form.updateSingleField(module_id, recordid, field, valueUp,value_key);
                json.put("recordid", recordid);
                
                break;
            case "convert_to_account" :
                try{
                String territory = request.getParameter("territory");
                String assign_to = request.getParameter("assign_to");
                
                if (!RequestResponse.requiredParams(territory, assign_to, module_id,recordid)) {
                    JSONObject err = RequestResponse.setErrorObj("Parameter missing");
                    writer.print(err.toJSONString());
                    writer.close();
                    return;
                }
                form.convertToAccount(sessionJson,module_id, recordid, territory, assign_to);
                json.put("recordid", recordid);
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
                break;
            case "assign_account" :
                String assign_to = request.getParameter("assign_to");
                String assign_from = request.getParameter("assign_from");
                form.assignAccountToUser(sessionJson,module_id, recordid,assign_to,assign_from);
                break;
            case "assign_inquiries" :
                String assignTo= request.getParameter("assign_to");
                 Helper.printConsole(sessionJson);
                form.assignInqueryToUser(sessionJson, module_id, recordid,assignTo);
                break;
            case "save_final_sale_products" :
                String saledata = request.getParameter("data");
                String f = form.saveFinalSaleProducts(saledata);
                json.put("soid", f);
                break;
            case "close_next_action" :
                String actionId=request.getParameter("action_id");
                String modid=request.getParameter("module_id");
                 form.closeActions(modid, actionId);
            break;
            case "trash" :
                String record_id=request.getParameter("record_id");
                String modid1=request.getParameter("module_id");
                 form.trashRecord(modid1, record_id);
            break;
            
                
        }
        form.closeClass();
        Helper.printConsole(json);
        writer.print(json.toJSONString());
        writer.close();
        //form.closeClass();
    }
}
