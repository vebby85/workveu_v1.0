/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Reminders;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class ReminderService extends HttpServlet {

    private void handle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
        String app_code = request.getParameter("app_code");
        PrintWriter writer = response.getWriter();
        if (!RequestResponse.requiredParams(trigger, app_code)) {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        Reminders reminder = new Reminders();
        reminder.app_code = app_code;
        String output = "";
        switch (trigger) {
            case "set_reminder":
                String data = request.getParameter("reminder");
                reminder.setReminder(data);
                break;
            case "get_reminders":
                String modid = request.getParameter("modid");
                String type = request.getParameter("type");
                output = reminder.getReminder(modid,type);
                break;
        }
        RequestResponse.returnSucResponse(response);
        writer.print(output);
        writer.close();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handle(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handle(request, response);
    }

}
