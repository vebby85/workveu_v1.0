/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.PostForm;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author abc
 */
public class PostImportService extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
       // Helper.printConsole("hello new servlet");
         String trigger = request.getParameter("trigger");
      
        PrintWriter writer = response.getWriter();
        if (!RequestResponse.requiredParams(trigger)) {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        String session = request.getParameter("session");
        String module_id = request.getParameter("module_id");
        String recordid = request.getParameter("record_id");
        PostForm form = new PostForm();
        JSONObject sessionJson = (JSONObject) JSONValue.parse(session);
        form.app_code = sessionJson.get("app_code").toString();
        JSONObject json = new JSONObject();
        String out = "";
       //Helper.printConsole(trigger);
        switch (trigger) {
            case "save_upload_records":
             
                String form_list = request.getParameter("form_list");
                //Helper.printConsole(formdata + " data");
                
                if (!RequestResponse.requiredParams(session, module_id, form_list)) {
                    JSONObject err = RequestResponse.setErrorObj("Parameter missing");
                    writer.print(err.toJSONString());
                    writer.close();
                    return;
                }
                out = form.saveAsArray(module_id, form_list, sessionJson);
                                

                json.put("recordid", out);
                
                break;
        }
        form.closeClass();
       // Helper.printConsole(json);
        writer.print(json.toJSONString());
        writer.close();
        
}

}