/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package services;

import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.InternalServiceHelper;
import org.json.simple.JSONObject;
/**
 *
 * @author viru
 */
public class InternalGetService extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   
        String trigger = request.getParameter("trigger");
        PrintWriter writer = response.getWriter();
        if(!RequestResponse.requiredParams(trigger))
        {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        InternalServiceHelper ish = new InternalServiceHelper();
        ish.app_code = request.getParameter("app_code");
        ish.moduleid = request.getParameter("module_id");
        String output = "";
        switch(trigger)
        {
            case "get_deatil_for_invoice" :
                String record_id = request.getParameter("record_id");
                output = ish.getProductDetailForInvoice(record_id);
            break;
            case "generate_so" :
                String html = request.getParameter("html");
                String path = request.getParameter("path");
                
                output = ish.generateSOPdf(html,path);
            break;
        }
        ish.closeMySql();
        RequestResponse.returnSucResponse(response);
        writer.print(output);
        writer.close();
    }
}
