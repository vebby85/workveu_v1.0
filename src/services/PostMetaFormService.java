/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.Helper;
import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.PostMetaForm;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class PostMetaFormService extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
        String app_code = request.getParameter("app_code");
        String data = request.getParameter("data");
        PrintWriter writer = response.getWriter();
        if (!RequestResponse.requiredParams(trigger)) {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        PostMetaForm pm = new PostMetaForm();
        pm.app_code = app_code;
        String uid = request.getParameter("uid");
        switch (trigger) {
            case "save_comments":
                pm.saveComments(data);
                break;
            case "save_attachment":
                pm.saveAttachment(data);
                break;
            case "save_email_simple":
                pm.saveSimpleEmail(data);
                break;
            case "update_user_meta":
                String experience = request.getParameter("experience");
                Helper.printConsole(data);
                if (null != data && !data.isEmpty()) {
                    pm.updateUserMeta(data, uid, "meta");
                }
                if (null != experience && !experience.isEmpty()) {
                    pm.updateUserMeta(experience, uid, "exp");
                }

                break;
            case "update_folowup":
                String formid = request.getParameter("formid");
                pm.updateFollowup(data, formid);
                break;
            case "direct_assign_inquiries":
                String source = request.getParameter("source");
                pm.updateDirectAssignInquery(uid, source);
                break;
            case "save_approvals":
                pm.saveApprovals(data);
                break;
            case "update_approvals":
                String cat = request.getParameter("cat");
                String statTo = request.getParameter("status_to");
                String apId = request.getParameter("approval_id");
                pm.updateApproval(uid,apId,statTo,cat);
                break;
            case "save_payments":
              //  Helper.printConsole("Save Payments");
                pm.savePayments(data);
                break;
        }
        pm.closeMySql();
        writer.print("Comment saved sucessfully");
        writer.close();
    }
}
