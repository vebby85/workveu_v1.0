/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.Helper;
import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Notifications;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class NotifictionService extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
        
        PrintWriter writer = response.getWriter();
        if (!RequestResponse.requiredParams(trigger)) {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        Notifications noti = new Notifications();
        noti.app_code = request.getParameter("app_code");
        noti.data = request.getParameter("data");
        Helper.printConsole(trigger);
        switch (trigger) {
            case "set_notification":
                    noti.setNotification();
                break;
            
        }
        noti.closeMySql();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
        String notiId=request.getParameter("notification_id");
        PrintWriter writer = response.getWriter();
        if (!RequestResponse.requiredParams(trigger)) {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        Notifications noti = new Notifications();
        noti.app_code = request.getParameter("app_code");
        noti.uid = request.getParameter("uid");
        String out = "";
        switch (trigger) {
            case "get_notifications":
                   out = noti.getNotifications();
                break;
            case "get_notification_count":
                   out = noti.getNotificationsCount();
                break;
            case "read":
                  noti.seen(notiId);
                break;
        }
        writer.print(out); 
        writer.close();
    }
    
}
