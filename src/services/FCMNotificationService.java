/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.FCMSendNotification;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class FCMNotificationService extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
        
        PrintWriter writer = response.getWriter();
        if(!RequestResponse.requiredParams(trigger))
        {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        FCMSendNotification fcm = new FCMSendNotification();
        switch(trigger)
        {
            case "send_single_push" :
                String uid = request.getParameter("uid");
                String[] device = fcm.getDeviceIdDeviceTypeByUser(uid);
                if(!device[0].isEmpty() && device[1].equals("andriod")) 
                {
                    fcm.message = request.getParameter("msg");
                    fcm.title = request.getParameter("title");
                    fcm.sendNotifiactionAndriod(device[0]);
                }
                break;

        }
        //fcm.closeMySql();
        
    }   
    
}
