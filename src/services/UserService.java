/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import connections.DbConnection;
import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.User;
import org.json.simple.JSONObject;

/**
 *
 * @author viru 
 */
public class UserService extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
        String uid = request.getParameter("uid");
        String app_code = request.getParameter("app_code");
        PrintWriter writer = response.getWriter();
        if(!RequestResponse.requiredParams(trigger))
        {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        User user = new User();
        user.app_code = app_code;
        user.uid = uid;
        switch(trigger)
        {
            case "set_login_settings" :
                user.setUserBasicSettings();
            break;
            case "update_table_columns_settings" :
                String module_id = request.getParameter("module_id");
                String columns = request.getParameter("columns");
                user.updateColumnSettings(columns,module_id);
            break;
            case "destroy_session" :
                    DbConnection.destroyMongoPool(app_code); 
                break;
            case "create_database_with_role" : 
                    user.createNewDB();
                break;
        }
        user.closeClass();
        writer.close();
    }
}
