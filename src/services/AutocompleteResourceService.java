/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.Helper;
import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.AutocompleteResource;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class AutocompleteResourceService extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
        PrintWriter writer = response.getWriter();
        if(!RequestResponse.requiredParams(trigger))
        {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        AutocompleteResource ar = new AutocompleteResource();
        String output = "";
        String s = request.getParameter("search");
        ar.app_code = request.getParameter("app_code");
        ar.module_id = request.getParameter("module_id");
        ar.databindwith = request.getParameter("data_bind_type");
        ar.uid = request.getParameter("uid");
        Helper.printConsole(trigger + " here i am");
        switch(trigger)
        {
            case "get_user_resource_autocomplete" : 
                String field_id = request.getParameter("field_id");
                String parent_id = request.getParameter("parent_id");
                String pname = request.getParameter("pname");
                Helper.printConsole(field_id + " here i am");
                output = ar.getUserResourceWithField(field_id,s,parent_id,pname);
                
                break;
            case "get_master_resource_autocomplete" : 
                String field_id1 = request.getParameter("field_id");
                output = ar.getMasterResoursce(field_id1);
                break;
            case "get_deal_data_by_id" : 
                String record_id = request.getParameter("record_id");
                String filter = request.getParameter("filter");
                output = ar.getDealDataById(record_id,filter);
                
                break;
            case "get_account_emails" : 
                String record_id1 = request.getParameter("record_id");
                output = ar.getEmailsByAccount(record_id1);
                
                break;
        }
        ar.closeClass();
        writer.write(output); 
        writer.close();
    }
}
