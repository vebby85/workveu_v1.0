/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Api_links;
import models.Reports;
import org.json.simple.JSONObject;

/**
 *
 * @author abc
 */
public class Api_links_service extends HttpServlet {
     @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
        String app_code = request.getParameter("app_code");
        String filter = request.getParameter("filter");
        String logged = request.getParameter("logged");
    
        PrintWriter writer = response.getWriter();
        if (!RequestResponse.requiredParams(trigger)) {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        List<String> out = new ArrayList<>();
        Api_links api=new Api_links();
         api.app_code = app_code;
        //api.logged = logged;
        //api.filter = filter;
        switch (trigger) {
           
            case "list_all_products":
                //out=api.listOfProducts();
             break;
            case "list_all_categories":
               // out=api.listOfCategories();
                break;
            case "list_all_suppliers":
                //out=api.listOfSuppliers();
                break;  
                
            case "list_all_saled_products":
                String output=api.listofSaleProducts();
                out.add(output);
                break;
        }
        api.closeMySql();
        writer.print(out);
        writer.close();
}
}