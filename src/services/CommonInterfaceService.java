/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.Helper;
import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.CommonInterface;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class CommonInterfaceService extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handle(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handle(request, response);
    }

    private void handle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
        PrintWriter writer = response.getWriter();
        if (!RequestResponse.requiredParams(trigger)) {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        CommonInterface cm = new CommonInterface();
        cm.app_code = request.getParameter("app_code");
        String out = "";
        Helper.printConsole(trigger);
        switch (trigger) {
            case "save_comment_by_module":
                String data = request.getParameter("data");
                cm.saveComment(data);
                break;
            case "get_comment_by_module":
                String record_id = request.getParameter("record_id");
                out = cm.getComments(record_id);
                break;
            
        }
        cm.closeMySql();
        RequestResponse.returnSucResponse(response);
        writer.print(out);
        writer.close();
    }

}
