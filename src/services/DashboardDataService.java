/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import helpers.Helper;
import helpers.RequestResponse;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Dashboard;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class DashboardDataService extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String trigger = request.getParameter("trigger");
        
        PrintWriter writer = response.getWriter();
        if (!RequestResponse.requiredParams(trigger)) {
            JSONObject err = RequestResponse.setErrorObj("Parameter missing");
            writer.print(err.toJSONString());
            writer.close();
            return;
        }
        Dashboard dash = new Dashboard();
        dash.app_code = request.getParameter("app_code");
        dash.uid = request.getParameter("uid");
        String json = "";
        
        switch (trigger) {
            case "get_targets_achive":
                Helper.printConsole("Crossed");
                json = dash.getTargetVsAchiveWidget();
                break;
            case "get_analytics_count":
                Helper.printConsole("Crossed");
                json = dash.getAnalyticsCount();
                break;
            case "get_analytics_comparision":
                json = dash.getAnalyticsComparision();
                break;
            case "get_analytics_avg_meetings":
                json = dash.getAvgMeetingStatics();
                break;
            case "get_deals_by_branches":
                String branches = request.getParameter("branches");
                json = dash.getDealsByTerritory(branches);
                break;
                
            case "get_today_accounts_created":
                json=dash.getNewlyCreatedAccounts();
                break;
           
        }
        dash.closeMySql();
        RequestResponse.returnSucResponse(response);
        writer.print(json);
        writer.close();
    }
}
