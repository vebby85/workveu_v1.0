/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.Minutes;
import org.joda.time.format.ISODateTimeFormat;

/**
 *
 * @author viru
 */
public class DateParser{
    public static SimpleDateFormat s;
    public static Date returnDateFromZuluString(String Z)
    {
       // System.out.println(ISODateTimeFormat.dateTimeParser().parseDateTime(Z).toDate());
        return ISODateTimeFormat.dateTimeParser().parseDateTime(Z).toDate();
    }
    public static Date returnDateFromZuluStringTZ(String Z,String tz)
    {
       // System.out.println(ISODateTimeFormat.dateTimeParser().parseDateTime(Z).toDate());
       // return ISODateTimeFormat.dateTimeParser().parseDateTime(Z).toDate();
        DateTime now = ISODateTimeFormat.dateTimeParser().parseDateTime(Z);
        DateTimeZone timeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(tz));
        return now.toDateTime(timeZone).toDate();
    }
    public static Date getStartTimeOfToday(String tz)
    {
        DateTimeZone timeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(tz));
        DateTime now = DateTime.now( timeZone );
        return now.withTimeAtStartOfDay().toDate();
    }
    public static Date getStartTimeWithAddDays(String tz,int days)
    {
        DateTimeZone timeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(tz));
        DateTime now = DateTime.now( timeZone );
        return now.plusDays(days).withTimeAtStartOfDay().toDate();
    }
    public static Date getStartTimeWithSubDays(String tz,int days)
    {
        DateTimeZone timeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(tz));
        DateTime now = DateTime.now( timeZone );
        return now.minusDays(days).withTimeAtStartOfDay().toDate();
    }
    public static Date getEndTimeWithSubDays(String tz,int days)
    {
        DateTimeZone timeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(tz));
        DateTime now = DateTime.now( timeZone );
        return now.minusDays(days).withTimeAtStartOfDay()
                .plusHours(23)
                .plusMinutes(59)
                .plusSeconds(59).toDate();
    }
    public static Date getEndTimeOfToday(String tz)
    {
        DateTimeZone timeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(tz));
        DateTime now = new DateTime(timeZone);
        DateTime plusSeconds = now.withTimeAtStartOfDay()
                .plusHours(23)
                .plusMinutes(59)
                .plusSeconds(59);
        return new Date(plusSeconds.getMillis());
    }
    
    public static String returnFormattedDateFromZulu(String Z, String format,String tz)
    {
        s = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        s.setTimeZone(TimeZone.getTimeZone(tz)); 
        SimpleDateFormat toChange = new SimpleDateFormat(format);
        //toChange.setTimeZone(TimeZone.getTimeZone("IST")); 
        try { 
            return  toChange.format(s.parse(Z)); 
        } catch (ParseException ex) {
       
        }
        return new Date().toString();
    }
    public static Date returnDateFromZulu(String Z,String tz)
    {
        s = new SimpleDateFormat("yyyy-MM-dd");
        s.setTimeZone(TimeZone.getTimeZone(tz)); 
      //  SimpleDateFormat toChange = new SimpleDateFormat(format);
        //toChange.setTimeZone(TimeZone.getTimeZone("IST")); 
        try { 
            return  s.parse(Z); 
        } catch (ParseException ex) {
       
        }
        return new Date();
    }
    public static String addMinutesToDate(int mins,String Z)
    { 
        DateTime now = ISODateTimeFormat.dateTimeParser().parseDateTime(Z);
        DateTime plusSeconds = now.plusMinutes(mins);
        return new Date(plusSeconds.getMillis()).toInstant().toString();
    }
    public static String addOneDayAndMinutes(int min,String Z)
    { 
        DateTime now = ISODateTimeFormat.dateTimeParser().parseDateTime(Z);
        DateTime plusSeconds = now.withTimeAtStartOfDay().plusDays(1).plusMinutes(min);  
        return new Date(plusSeconds.getMillis()).toInstant().toString();
    }
    public static Date addOneDay(String Z)
    { 
        DateTime now = ISODateTimeFormat.dateTimeParser().parseDateTime(Z);
        DateTime plusSeconds = now.withTimeAtStartOfDay().plusDays(1);  
        return new Date(plusSeconds.getMillis());
    }
    public static Date minusOneDay(String Z)
    { 
        DateTime now = ISODateTimeFormat.dateTimeParser().parseDateTime(Z);
        DateTime plusSeconds = now.withTimeAtStartOfDay().minusDays(1);  
        return new Date(plusSeconds.getMillis());
    }
    public static String addHourMinutesToDate(int mins,String Z)
    { 
        int hours = mins / 60;
        int restMins = mins % 60;
        DateTime now = ISODateTimeFormat.dateTimeParser().parseDateTime(Z);
        DateTime plusHours = now.withTimeAtStartOfDay().plusHours(hours).plusMinutes(restMins); 
        return new Date(plusHours.getMillis()).toInstant().toString(); 
    }
    public static String addDaysToDate(int days,String Z)
    { 
        DateTime now = ISODateTimeFormat.dateTimeParser().parseDateTime(Z);
        DateTime plusHours = now.withTimeAtStartOfDay().plusDays(days); 
        return new Date(plusHours.getMillis()).toInstant().toString(); 
    }
    public static Date getStartTimeOfByDate(String Z,String tz)
    {
        DateTime now = ISODateTimeFormat.dateTimeParser().parseDateTime(Z);
        
        DateTimeZone timeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(tz));
        DateTime toDateTime = now.toDateTime(timeZone);
        
        return new Date(toDateTime.withTimeAtStartOfDay().getMillis());
    }
    public static Date getEndTimeOfByDate(String Z,String tz)
    {
         DateTime now = ISODateTimeFormat.dateTimeParser().parseDateTime(Z);
        DateTimeZone timeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(tz));
        DateTime toDateTime = now.withZone(timeZone);
       
        DateTime plusSeconds = toDateTime
                .withTimeAtStartOfDay()
                .plusHours(23)
                .plusMinutes(59)
                .plusSeconds(59);
        
        return new Date(plusSeconds.getMillis());
    }
    public static int differanceMinutesBetweenTwoDates(String start,String end)
    {
        DateTime startDate = ISODateTimeFormat.dateTimeParser().parseDateTime(start);
        DateTime endDate = ISODateTimeFormat.dateTimeParser().parseDateTime(end);
        return (Minutes.minutesBetween(endDate,startDate ).getMinutes());
    }
    public static int getTotalMinutesFromDate(String Z)
    {
        DateTime now = ISODateTimeFormat.dateTimeParser().parseDateTime(Z);
        return (now.getMinuteOfDay());
    }
    public static Date getFirstDateOfMonth(int month,int year)
    {
        LocalDate localDate = new LocalDate(year, month, 1);
        return localDate.toDate();
    }
    public static Date getLastDateOfMonth(int month,int year)
    {
        LocalDate localDate = new LocalDate(year, month, 1);
        return localDate.plusMonths(1).minusDays(1).toDate();
    }
    public static String getNameOfMonth(int month,int year)
    {
        LocalDate localDate = new LocalDate(year, month, 1);
        return localDate.monthOfYear().getAsText();
    }
    public static String getCurrentDateASZulu()
    { 
        DateTime nowUTC = new DateTime(DateTimeZone.UTC);
        return nowUTC.toString();
    }
     public static String getFirstdateofcurrentWeek()
    { 
             LocalDate now = LocalDate.now();
             LocalDate first = now.withDayOfWeek(1);
               return first.toString();
                    
    }
    
       public static String getlastdateofcurrentWeek()
    { 
             LocalDate now = LocalDate.now();
             LocalDate last = now.withDayOfWeek(7);
               return last.toString();
                    
    } 
        public static String getFirstWeekdateOfCurrentYear(int week_no)
    { 
           
       Calendar cal = Calendar.getInstance();   
       cal.add(Calendar.WEEK_OF_YEAR,week_no);
       cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
  return null;
    }
    public static String getfirstweekofYear(int year)
    { 
           
        DateTime weekStartDate=new DateTime().withWeekyear(year);
        return weekStartDate.toString();
        
    }
    public static String getyearFromDate(Date date)
    {
        String year= new SimpleDateFormat("yyyy", Locale.ENGLISH).format(date);

        return year;
    }
    public static String getmonthFromDate(Date date)
    {
        String year= new SimpleDateFormat("MM", Locale.ENGLISH).format(date);

        return year;
    }
     
}
