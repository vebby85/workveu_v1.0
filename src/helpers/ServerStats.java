/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import com.mongodb.BasicDBObject;
import com.sun.management.OperatingSystemMXBean;
import interfaces.BaseModal;
import java.lang.management.ManagementFactory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import org.bson.Document;
import org.json.simple.JSONObject;

/**
 *
 * @author viru
 */
public class ServerStats extends BaseModal {

    public JSONObject getMySqlStats() {
        JSONObject json = new JSONObject();
        try {
            PreparedStatement ps = this.supplyMySql().prepareStatement("SHOW VARIABLES LIKE 'max_connections';");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                json.put("total_connections", rs.getString("Value"));
            }
            rs.close();
            ps.clearParameters();
            ps = this.supplyMySql().prepareStatement("show status where variable_name = 'threads_connected';");
            ResultSet rs1 = ps.executeQuery();
            while (rs1.next()) {
                json.put("open_connections", rs1.getString("Value"));
            }
            rs1.close();
            ps.close();
            this.closeMySql();
            Document runCommand = this.supplyOpenMongo().getDatabase("admin")
                    .runCommand(new BasicDBObject("serverStatus",1));
            json.put("mongo_status", runCommand);
            this.supplyOpenMongo().close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return json;
    }

    public JSONObject getSystemMemStats() {
        JSONObject json = new JSONObject();
        try {
            Runtime runtime = Runtime.getRuntime();

            NumberFormat format = NumberFormat.getInstance();

            long maxMemory = runtime.maxMemory();
            long allocatedMemory = runtime.totalMemory();
            long freeMemory = runtime.freeMemory();
            int mb = 1024 * 1024;
            json.put("free_memory", format.format(freeMemory / mb));
            json.put("allocated_memory", format.format(allocatedMemory / mb));
            json.put("max_memory", format.format(maxMemory / mb));
            json.put("total_free_memory", format.format((freeMemory + (maxMemory - allocatedMemory)) / mb));

            OperatingSystemMXBean operatingSystemMXBean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
            
             json.put("cpu_load", operatingSystemMXBean.getSystemCpuLoad());
             json.put("total_ram", format.format(operatingSystemMXBean.getTotalPhysicalMemorySize()/ (mb * 1024)));
             json.put("free_ram", format.format(operatingSystemMXBean.getFreePhysicalMemorySize()/ mb));
        } catch (Exception e) {
            
        }
        return json;
    }

    public void closeClass() {
        this.closeMySql();
    }
}
