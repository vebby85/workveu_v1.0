/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import org.bson.Document;

/**
 *
 * @author viru
 */
public class Helper {
    public static void printConsole(Object a)
    {
        System.out.println(a);
    }
    public static boolean notNull(String o)
    {
        return null != o && !o.isEmpty();
    }
    public static boolean notNullDocument(Document o)
    {
        return null != o && !o.isEmpty();
    }
}
