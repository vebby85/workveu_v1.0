/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import jakarta.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author Virender
 */
public class RequestResponse {
    public static boolean requiredParams(String ...params)
    {
        if(params.length > 0)
        {
            for (String param : params) {
                if (null == param || param.isEmpty()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    public static String setError(String msg,int code)
    {
        JSONObject err = new JSONObject();
        err.put("status", false);
        err.put("code", code);
        err.put("message", msg);
        err.put("result", "fail");
        return err.toJSONString();
    }
    public static String setSuccess(String msg)
    {
        JSONObject suc = new JSONObject();
        suc.put("status", true);
        suc.put("code", 200);
        suc.put("message", msg);
        suc.put("result", "success");
        return suc.toJSONString();
    }
    public static JSONObject setSuccessObj(String msg)
    {
        JSONObject suc = new JSONObject();
        suc.put("status", true);
        suc.put("code", 200);
        suc.put("message", msg);
        suc.put("result", "success");
        return suc;
    }
    public static JSONObject setErrorObj(String msg)
    {
        JSONObject suc = new JSONObject();
        suc.put("status", false);
        suc.put("code", 200);
        suc.put("message", msg);
        suc.put("result", "fail");
        return suc;
    }
    public static void returnErrResponse(HttpServletResponse res,int code)
    {
        res.setContentType("application/json");
        res.setCharacterEncoding("utf-8");
        res.setStatus(code);
    }
    public static HttpServletResponse returnSucResponse(HttpServletResponse res)
    {
        res.setContentType("application/json");
        res.setCharacterEncoding("utf-8");
        res.setHeader("Connection","Keep-Alive");
        res.setHeader("Keep-Alive","timeout=3, max=100");
        res.setStatus(200);
        return res;
    }
    public String generateResponse(JSONObject insert)
    {
        String succ = "";
        if(!insert.get("error").toString().isEmpty())
        {
             succ = RequestResponse.setError(insert.get("error").toString(),200);
        }
        else
        {
            succ = RequestResponse.setSuccess(insert.get("message").toString());
        }
        return succ;
    }
}
