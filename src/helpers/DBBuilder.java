/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author viru
 */
public class DBBuilder {
    public String query="";
    private Connection con;
    private Statement ps;
    public void setConnection()
    {
        this.con = connections.DbConnection.returnMySQLConnection();

    }
    public DBBuilder table(String table)
    {
        this.query += " FROM " + table;
        return this;
    }
    public DBBuilder select(String ...arg)
    {
        String select = "SELECT ";
        if(arg.length > 0)
        {
            for(String s : arg)
            {
                select += s +",";
            }
            select = select.substring(0, select.length() - 1);
        }
        this.query = select + this.query;
        return this;
    }
    public DBBuilder where(String col,String concat,String cond)
    {
        this.query += " WHERE " + col + concat + "'" + cond + "'";
        return this;
    }
    public String getSingleColumn(String col)
    {
        String val = "";
        try {
            this.setConnection();
            this.ps = this.con.createStatement();
            ResultSet rs = this.ps.executeQuery(this.query); 
            while(rs.next())
            {
                val = rs.getString(col);
            }
        } catch (SQLException ex) {}
        return val;
    }
    public void printQuery()
    {
        Helper.printConsole(this.query);
    }
    public void close()
    {
        try {
            this.ps.close();
            this.con.close();
        } catch (SQLException ex) {}
    }
    
}
