/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import com.mongodb.MongoClient;
import connections.DbConnection;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
//import org.elasticsearch.client.RestHighLevelClient;
import redis.clients.jedis.Jedis;

/**
 *
 * @author viru
 */
public  class BaseModal {
    public String app_code = "";
    public String logged = "";
    private Connection con;
    private Jedis jcon;
    public String tz = "Asia/Calcutta";
   // public RestHighLevelClient client;
    private static List ext = new LinkedList();
    public MongoClient supplyMongo()
    {
        return DbConnection.getMongoPoolForUser(this.app_code);
    }
    public Jedis supplyJedis()
    {
        if(null == this.jcon || !this.jcon.isConnected())
        {
            this.jcon = DbConnection.RedisConnect();
        }
        return this.jcon;
    }
    public MongoClient supplyOpenMongo()
    {
        return DbConnection.openMongoCon();
    }
    public void supplyElastic()
    {
        //this.client = DbConnection.setElasticConnection();
    }
    public void closeElastic()
    {
        //try {
            //this.client.close();
       // } catch (IOException ex) {}
       
    }
    public Connection supplyMySql()
    {
        try {
            if(null == this.con || this.con.isClosed())
                this.con = DbConnection.returnMySQLConnection();
          //  System.out.println(this.con.getClientInfo().toString());
        } catch (SQLException ex) {
        ex.printStackTrace();
        }
        return this.con;
    }
    public void closeMySql()
    {
        try {
            if(null != this.con && !this.con.isClosed())
            {
               this.con.close(); 
            }
        } catch (SQLException ex) {}
        if(null != this.jcon && this.jcon.isConnected())
        {
           this.jcon.close(); 
        }
         
    }
    public void closeMongo()
    {
        this.supplyMongo().close();
    }
}
