/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import helpers.Helper;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import models.GetForm;
import models.helper.CommonModel;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import templates.BasicTemplates;
/**
 *
 * @author DELL
 */
public class Emails extends BaseModal {

    public String subject;
    public String message;
    private Session session;
    
    public void setConfig() {
        Properties props = new Properties();
        props.put("mail.smtp.host", "mail.workveu.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
       // props.put("java.net.preferIPv4Stack",true);
        this.session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication
                    getPasswordAuthentication() {
                return new PasswordAuthentication("notifications@workveu.com",
                        "Viru@123");
            }
        });
    }

    public void sendWelcomeEmail(String to, String uname, String pass, String reportTo) {
      
            BasicTemplates t = new BasicTemplates();
            Message message = new MimeMessage(this.session);
            Message msg = new MimeMessage(this.session);
                                   
            try{                 
            message.setFrom(new InternetAddress("notifications@workveu.com", "Workveu Team", "utf-8"));
            message.setRecipients(Message.RecipientType.TO,
                  InternetAddress.parse(to)); 
            message.setSubject("Welcome to Workveu");
            message.setContent(t.getWelcomeTemplate(pass,uname), "text/html");
            Transport transport = session.getTransport("smtps");
            transport.send(message);
            
            msg.setFrom(new InternetAddress("notifications@workveu.com", "Workveu Team", "utf-8"));
            msg.setRecipients(Message.RecipientType.TO,
                  InternetAddress.parse(reportTo));
            String msgs="please welcome "+uname+" is joined in your team <br> Emailid:"+to;
            msg.setSubject("A new User is added in your team");
            msg.setContent(t.SimpleTemplate(msgs), "text/html");
            transport.send(msg);
            
            transport.close();
        } catch (MessagingException e) {
            
            throw new RuntimeException(e);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
           
        
    }
          public void SendEmail(String to)
          {
              BasicTemplates t = new BasicTemplates();
             Message message = new MimeMessage(this.session);
            try{                 
            message.setFrom(new InternetAddress("notifications@workveu.com", "Workveu Team", "utf-8"));
            message.setRecipients(Message.RecipientType.TO,
                   InternetAddress.parse(to));
            message.setSubject(this.subject);
            message.setContent(t.SimpleTemplate(this.message),"text/html");
            Transport transport = session.getTransport("smtps");
            transport.send(message);
            transport.close();
        } catch (MessagingException e) {
            
            
            throw new RuntimeException(e);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
            
          }
            
    public void sendForgotPasswordEmail(String to, String otp) {
      
        BasicTemplates t = new BasicTemplates();
               
        try {
            Helper.printConsole("Here os ");
            Message message = new MimeMessage(this.session);
            Helper.printConsole("Here os " + "uyt");
            message.setFrom(new InternetAddress("notifications@workveu.com", "Workveu Team", "utf-8"));

            message.setRecipients(Message.RecipientType.TO,
                   InternetAddress.parse(to));
            //message.addRecipient(Message.RecipientType.TO,new InternetAddress("to"));
            message.setSubject("Reset Password Code");
            message.setContent(t.forgotPasswordTemplate(otp),"text/html");
            Transport transport = session.getTransport("smtps");
            Transport.send(message);
            transport.close();
            } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
            } 
       // catch (UnsupportedEncodingException ex) {
         //   ex.printStackTrace();
         //}
          
    }
    public String app_code = "";
    public boolean sendSimpleEmail(String data) {
        try {
            JSONObject email = (JSONObject) JSONValue.parse(data);
            JSONArray toJarr = (JSONArray) email.get("email_to");
            String attach = "";
            if(!email.get("file_attached").toString().isEmpty())
            {
               attach = "file=" + email.get("file_attached").toString() + "&modid=" 
                       + email.get("modid").toString() + "&_o=" + this.app_code;
            }
            int tlen = toJarr.size();
            InternetAddress toAdd[] = new InternetAddress[tlen];
            for (int i = 0; i < tlen; i++) {
                try {
                    toAdd[i] = new InternetAddress(toJarr.get(i).toString());
                } catch (AddressException ex) {
                }
            }
            String msg = new BasicTemplates().getSimpleEmailTemplate(email.get("message").toString(), attach, email.get("from").toString());
            try {
                Message message = new MimeMessage(this.session);
                message.setFrom(new InternetAddress("notifications@workveu.com", email.get("from").toString(), "utf-8"));
                message.setRecipients(Message.RecipientType.TO,
                        toAdd);
                message.setSubject(email.get("subject").toString());
                message.setContent(msg, "text/html");
                Transport transport = session.getTransport("smtps");
                transport.send(message);
                //Helper.printConsole(transport.isConnected() + " == " + transport.getURLName().toString());
                transport.close();
                return true;
            } catch (MessagingException | UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
          public void dailyActionManagerMail(String uId,String name,String report_To)
          {
               CommonModel cm = new CommonModel();
               cm.con = this.supplyMySql();
               GetForm form=new GetForm();
               form.app_code=app_code;
               String To=cm.getEmail(app_code,uId);
               Helper.printConsole(To);
               //JSONObject json=form.getMyteamAction(app_code,uId,name,reportTo);
               JSONParser parser = new JSONParser();
               JSONObject json=new JSONObject();
               try {
                json = (JSONObject) parser.parse(form.getActionManager(uId, name, report_To));
                } 
               catch (ParseException ex) {
                ex.printStackTrace();
                }
                String rows=json.get("rows").toString().replace("\"", "");
                String d=rows.replace("],", "</td><tr><td>");
                String d1=d.replace("[", "");
                String fd=d1.replace(",", "</td><td>");
                String d3=fd.replace("]", "");
                subject="Actions";
                message="<table border=1><tr><th>Name</th><th>Reports_to</th>"+
                "<th>Missed</th><th>Today's action</th><th>Scheduled</th></tr><tr><td>"+d3+"</td></tr></table>";
                SendEmail(To);
             //  System.out.println(To);
               
          }
    
}
