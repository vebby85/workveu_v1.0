/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.Properties;

/**
 *
 * @author DELL
 */
public class Config {
    private String username;
    private String password;
    private String host;
    private String port;
    private Properties props = new Properties();
    public void setUsername(String u)
    {
        this.username = u;
    }
    public void setPassword(String p)
    {
        this.username = p;
    }
    public void setPort(String p)
    {
        this.port = p;
    }
    public void setHost(String h)
    {
        this.host = h;
    }
    public String getHost()
    {
        return this.host;
    }
    public String getUsername()
    {
        return this.username;
    }
    public String getPassword()
    {
        return this.password;
    }
    public String getPort()
    {
        return this.port;
    }
    public Properties getEmailProps()
    {      
       
        props.put("mail.smtp.auth", "localhost"); 
        props.put("mail.smtp.starttls.enable", "true"); 
        props.put("mail.smtp.host", this.host); 
        props.put("mail.smtp.port", this.port);          
        return this.props;
    }
}
