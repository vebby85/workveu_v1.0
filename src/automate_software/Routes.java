/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automate_software;

import connections.DbConnection;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.gzip.GzipHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import services.AnalyticsService;
import services.Api_links_service;
import services.AutocompleteResourceService;
import services.CommonInterfaceService;
import services.DashboardDataService;
import services.DataListsService;
import services.EmailService;
import services.FCMNotificationService;
import services.GetFormService;
import services.GetMetaFormService;
import services.InternalGetService;
import services.LogsService;
import services.ModuleFormService;
import services.NotifictionService;
import services.PostFormService;
import services.PostMetaFormService;
import services.ReminderService;
import services.ReportsService;
import services.ServerStatsService;
import services.UserService;
import services.WelcomeService;

/**
 *
 * @author viru
 */
public class Routes {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        QueuedThreadPool threads = new QueuedThreadPool(500,50);
        threads.setIdleTimeout(10000); 
        
        Server server = new Server(threads);
        HttpConfiguration http = new HttpConfiguration();
        http.setIdleTimeout(15000);
        
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(9000);
        connector.setIdleTimeout(15000);

        ServletContextHandler context = new ServletContextHandler();
       
        //context.setContextPath("/");
        FilterHolder cors = new FilterHolder(CrossOriginFilter.class);
        cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,POST,HEAD");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Access-Control-Allow-Origin,X-Requested-With,Content-Type,Accept,Origin,Cache-Control");
        cors.setInitParameter(CrossOriginFilter.CHAIN_PREFLIGHT_PARAM, "false");
        cors.setInitParameter("allowCredentials", "false");
        
        //context.addFilter(cors, "/*", EnumSet.of(DispatcherType.REQUEST));
        GzipHandler gzip = new GzipHandler();
        gzip.setIncludedMethods("GET");
        gzip.setMinGzipSize(50);
        gzip.setIncludedMimeTypes("text/html","application/json");
        server.setHandler(context);
        context.addServlet(WelcomeService.class, "/");
        context.addServlet(WelcomeService.class, "/welcome");
        context.addServlet(ModuleFormService.class, "/module/forms");
        context.addServlet(UserService.class, "/users");
        context.addServlet(PostFormService.class, "/post/form");
        context.addServlet(PostMetaFormService.class, "/post/form/meta");
        context.addServlet(GetMetaFormService.class, "/get/form/meta");
        context.addServlet(GetFormService.class, "/get/form");
        context.addServlet(AutocompleteResourceService.class, "/get/autocomplte");
        context.addServlet(DashboardDataService.class, "/get/dashoard/widgets");
        context.addServlet(ReportsService.class, "/get/reports");
        context.addServlet(InternalGetService.class, "/get/internal/data");
        context.addServlet(CommonInterfaceService.class, "/common/data/setget");
        context.addServlet(ReminderService.class, "/reminders");
        context.addServlet(FCMNotificationService.class, "/send/fcm/notification");
        context.addServlet(EmailService.class, "/send/email");
        context.addServlet(LogsService.class, "/app/user/logs");
        context.addServlet(NotifictionService.class, "/app/user/notifications");
        context.addServlet(AnalyticsService.class, "/get/analytics");
        context.addServlet(DataListsService.class, "/get/data/lists");
        context.addServlet(ServerStatsService.class, "/server/stats");
   //   context.addServlet(PostImportService.class, "/post/import/global_data");
        context.addServlet(Api_links_service.class,"/get/api_links");
        DbConnection.setMongoSettings();
        gzip.setHandler(context);
        server.setHandler(gzip);
        server.addConnector(connector);
        server.start();
        server.join();
    }
    
}
